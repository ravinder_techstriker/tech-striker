package com.daivikapp.social.models;

import java.util.List;

public class Feeds {

    public String emp_id;
    public String name;
    public String email;
    public String phone;
    public String profile_pic;
    public String designation;
    public String user_type;
    public String date;
    public String time;
    public String leave_description;
    public String leave_title;
    public String leave_from;
    public String leave_to;
    public String leave_type;
    public String leave_type1;
    public String type;

    public String announce_img;
    public String announcement;
    public String announcement_title;
    public String announcement_date;
    public String announcement_time;
    public String feed_id;
    public String leave_status;

    public String getLeave_status() {
        return leave_status;
    }

    public void setLeave_status(String leave_status) {
        this.leave_status = leave_status;
    }

    public String getFeed_id() {
        return feed_id;
    }

    public void setFeed_id(String feed_id) {
        this.feed_id = feed_id;
    }

    private List<User> tagedUsersList = null;

    private List<Comments> leave_comment = null;

    private List<Option> announcement_like = null;

    private List<Comments> announcement_comment = null;


    public List<Comments> getLeave_comment() {
        return leave_comment;
    }

    public void setLeave_comment(List<Comments> leave_comment) {
        this.leave_comment = leave_comment;
    }

    public List<User> getTagedUsersList() {
        return tagedUsersList;
    }

    public void setTagedUsersList(List<User> tagedUsersList) {
        this.tagedUsersList = tagedUsersList;
    }
    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }



    public String getLeave_type1() {
        return leave_type1;
    }

    public void setLeave_type1(String leave_type1) {
        this.leave_type1 = leave_type1;
    }

    public String getEmp_id() {
        return emp_id;
    }

    public void setEmp_id(String emp_id) {
        this.emp_id = emp_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getProfile_pic() {
        return profile_pic;
    }

    public void setProfile_pic(String profile_pic) {
        this.profile_pic = profile_pic;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getUser_type() {
        return user_type;
    }

    public void setUser_type(String user_type) {
        this.user_type = user_type;
    }


    public String getLeave_description() {
        return leave_description;
    }

    public void setLeave_description(String leave_description) {
        this.leave_description = leave_description;
    }

    public String getLeave_title() {
        return leave_title;
    }

    public void setLeave_title(String leave_title) {
        this.leave_title = leave_title;
    }


    public String getLeave_from() {
        return leave_from;
    }

    public void setLeave_from(String leave_from) {
        this.leave_from = leave_from;
    }

    public String getLeave_to() {
        return leave_to;
    }

    public void setLeave_to(String leave_to) {
        this.leave_to = leave_to;
    }


    public String getLeave_type() {
        return leave_type;
    }

    public void setLeave_type(String leave_type) {
        this.leave_type = leave_type;
    }


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }


    public List<Option> getAnnouncement_like() {
        return announcement_like;
    }

    public void setAnnouncement_like(List<Option> announcement_like) {
        this.announcement_like = announcement_like;
    }

    public List<Comments> getAnnouncement_comment() {
        return announcement_comment;
    }

    public void setAnnouncement_comment(List<Comments> announcement_comment) {
        this.announcement_comment = announcement_comment;
    }


    public String getAnnouncement_time() {
        return announcement_time;
    }

    public void setAnnouncement_time(String announcement_time) {
        this.announcement_time = announcement_time;
    }

    public String getAnnouncement_date() {
        return announcement_date;
    }

    public void setAnnouncement_date(String announcement_date) {
        this.announcement_date = announcement_date;
    }



    public String getAnnounce_img() {
        return announce_img;
    }

    public void setAnnounce_img(String announce_img) {
        this.announce_img = announce_img;
    }

    public String getAnnouncement() {
        return announcement;
    }

    public void setAnnouncement(String announcement) {
        this.announcement = announcement;
    }

    public String getAnnouncement_title() {
        return announcement_title;
    }

    public void setAnnouncement_title(String announcement_title) {
        this.announcement_title = announcement_title;
    }






}

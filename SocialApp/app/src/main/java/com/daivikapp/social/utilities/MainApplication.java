package com.daivikapp.social.utilities;


import android.app.Activity;
import android.app.ProgressDialog;
import android.arch.lifecycle.LifecycleRegistry;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.support.design.widget.Snackbar;
import android.support.multidex.MultiDexApplication;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.TextView;

import com.facebook.FacebookSdk;
import com.facebook.drawee.backends.pipeline.Fresco;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.StandardExceptionParser;
import com.google.android.gms.analytics.Tracker;
import com.rygelouv.audiosensei.player.AudioSenseiListObserver;
import com.daivikapp.social.Interface.ApiCalls;
import com.daivikapp.social.R;
import com.daivikapp.social.utilities.Config;

/**
 * Created by gys on 02-03-2017.
 */

public class MainApplication extends MultiDexApplication {

    static ProgressDialog progress;
    private static ApiCalls apiCalls;
    public static Retrofit retrofit;
    public static final String TAG = MainApplication.class
            .getSimpleName();

    private static MainApplication mInstance;

    @Override
    public void onCreate() {
        super.onCreate();

        mInstance = this;
        FontsOverride.setDefaultFont(this, "SERIF", "fonts/noto_sans_regular.ttf");
        FontsOverride.setDefaultFont(this, "MONOSPACE", "fonts/noto_sans_bold.ttf");


        Fresco.initialize(this);
        FacebookSdk.sdkInitialize( getApplicationContext() );
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().connectTimeout(240, TimeUnit.SECONDS).readTimeout(240, TimeUnit.SECONDS).addInterceptor(interceptor).build();

        retrofit = new Retrofit.Builder()
                .baseUrl(Config.BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        apiCalls = retrofit.create(ApiCalls.class);


        AnalyticsTrackers.initialize(this);
        AnalyticsTrackers.getInstance().get(AnalyticsTrackers.Target.APP);

    }

    public static synchronized MainApplication getInstance() {
        return mInstance;
    }


    public static ApiCalls getApiService(){

        return apiCalls;
    }

    public static   boolean  isNetworkAvailable(Context context) {
        final ConnectivityManager connectivityManager = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE));
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }





    public static void showSnackBar(Activity ac, String message, View lay) {
        Snackbar snackbar = Snackbar
                .make(lay, message, Snackbar.LENGTH_LONG)
                .setAction("OK", onSnackBarClickListener());

        snackbar.setActionTextColor(ac.getResources().getColor(R.color.colorPrimary));
        View snackbarView = snackbar.getView();
        snackbarView.setBackgroundColor(Color.WHITE);
        TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
        //  textView.setTextColor(getResources().getColor(R.color.buttonbackground));
        textView.setTextColor(ContextCompat.getColor(ac, R.color.colorAccent));
        snackbar.show();
    }

    private static View.OnClickListener onSnackBarClickListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Toast.makeText(LoginActivity.this, "You clicked SnackBar Button", Toast.LENGTH_SHORT).show();

            }
        };
    }


    public synchronized Tracker getGoogleAnalyticsTracker() {
        AnalyticsTrackers analyticsTrackers = AnalyticsTrackers.getInstance();
        return analyticsTrackers.get(AnalyticsTrackers.Target.APP);
    }

    /***
     * Tracking screen view
     *
     * @param screenName screen name to be displayed on GA dashboard
     */
    public void trackScreenView(String screenName, String id) {
        Tracker t = getGoogleAnalyticsTracker();

        // Set screen name.
        t.setScreenName(screenName);
        t.set("&uid", id);
        // Send a screen view.
        t.send(new HitBuilders.ScreenViewBuilder().build());

        GoogleAnalytics.getInstance(this).dispatchLocalHits();
    }

    /***
     * Tracking exception
     *
     * @param e exception to be tracked
     */
    public void trackException(Exception e) {
        if (e != null) {
            Tracker t = getGoogleAnalyticsTracker();

            t.send(new HitBuilders.ExceptionBuilder()
                    .setDescription(
                            new StandardExceptionParser(this, null)
                                    .getDescription(Thread.currentThread().getName(), e))
                    .setFatal(false)
                    .build()
            );
        }
    }

    /***
     * Tracking event
     *
     * @param category event category
     * @param action   action of the event
     * @param label    label
     */
    public void trackEvent(String category, String action, String label, String id) {
        Tracker t = getGoogleAnalyticsTracker();
        t.set("&uid", id);
        // Build and send an Event.
        t.send(new HitBuilders.EventBuilder().setCategory(category).setAction(action).setLabel(label).build());
    }


}


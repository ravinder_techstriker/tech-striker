package com.daivikapp.social.utilities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.content.ContextCompat;

import com.daivikapp.social.R;


/**
 * Created by AndroidDev on 4/19/2017.
 */

public class ProgDialog {

    ProgressDialog progress;
   public  void progDialog(Context ac) {

       try {
           if(progress == null) {
               progress = new ProgressDialog(ac);
               progress.getWindow().setBackgroundDrawable(new ColorDrawable(Color.BLUE));
               progress.setMessage(ac.getString(R.string.progress_bar_text));
               progress.setInverseBackgroundForced(true);
               progress.getWindow().setBackgroundDrawable(new ColorDrawable(ContextCompat.getColor(ac, R.color.colorWhite)));
               progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
               progress.setIndeterminate(true);
           }
           progress.show();
       } catch (Exception e) {
//           //System.out.println("appppppppppp crash "+e);
       }
   }

    public void hideProg(){
    progress.hide();
}

    public void destroy() {
       if(progress != null)
           progress.dismiss();
    }
}

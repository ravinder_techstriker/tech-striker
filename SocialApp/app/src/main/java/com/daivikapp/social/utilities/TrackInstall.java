package com.daivikapp.social.utilities;

import android.content.Context;
import android.os.AsyncTask;
import android.os.RemoteException;

import com.android.installreferrer.api.InstallReferrerClient;
import com.android.installreferrer.api.InstallReferrerStateListener;
import com.android.installreferrer.api.ReferrerDetails;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

public class TrackInstall {
    private static String INSTALL_REF = "INSTALLREF";
    private static String INSTALL_POSTED = "INSTALLPOSTED";

    private static String INSTALL_PATH = "install";
    private static String INSTALL_LOGIN_PATH = "installlogin";

    private static InstallReferrerClient mReferrerClient;
    private static String mRef;

    public static class InstallTask extends AsyncTask {
        Context mContext;
        int mTaskId;
        String mUserId;
        TinyDB mTinyDb;

        public InstallTask(Context pCon, int taskId, String uid, TinyDB tdb) {
            mContext = pCon;
            mTaskId = taskId;
            mUserId = uid;
            mTinyDb = tdb;
        }

        @Override
        protected Object doInBackground(Object[] objects) {
            if (mTaskId == 1)
                handleFirstInstall();
            else if (mTaskId == 2)
                sendLoginInfo();
            return null;
        }

        protected void handleFirstInstall() {

            mRef = mTinyDb.getString(INSTALL_REF);

            if ((mRef == null) || (mRef.length() <= 1)) {
                mReferrerClient = InstallReferrerClient.newBuilder(mContext).build();
                mReferrerClient.startConnection(new InstallReferrerStateListener() {
                    @Override
                    public void onInstallReferrerSetupFinished(int responseCode) {
                        switch (responseCode) {
                            case InstallReferrerClient.InstallReferrerResponse.OK:
                                // Connection established
                                ReferrerDetails response = null;
                                try {
                                    response = mReferrerClient.getInstallReferrer();
                                    mRef = response.getInstallReferrer();
                                    if((mRef == null) || (mRef.length() <= 1)) {
                                        mRef = "utm_source=no_referrer";
                                    }
                                    //sent to server
                                    mTinyDb.putString(INSTALL_REF, mRef);
                                    //close
                                    mReferrerClient.endConnection();

                                    AsyncTask.execute(new Runnable() {
                                        @Override
                                        public void run() {
                                            sendToServer(mContext, mTinyDb);
                                        }
                                    });

                                } catch (RemoteException e) {
                                    e.printStackTrace();
                                    return;
                                }
                                break;
                            case InstallReferrerClient.InstallReferrerResponse.FEATURE_NOT_SUPPORTED:
                                // API not available on the current Play Store app
                                mRef = "utm_source=ref_not_supported";
                                //sent to server
                                mTinyDb.putString(INSTALL_REF, mRef);
                                //close
                                mReferrerClient.endConnection();

                                AsyncTask.execute(new Runnable() {
                                    @Override
                                    public void run() {
                                        sendToServer(mContext, mTinyDb);
                                    }
                                });
                                return;
                            case InstallReferrerClient.InstallReferrerResponse.SERVICE_UNAVAILABLE:
                                // Connection could not be established
                                return;
                        }
                    }

                    @Override
                    public void onInstallReferrerServiceDisconnected() {
                        // Try to restart the connection on the next request to
                        // Google Play by calling the startConnection() method.
                    }
                });
            } else if (mRef != null) {
                sendToServer(mContext, mTinyDb);
            }
        }

        protected void sendLoginInfo() {
            HttpURLConnection urlConnection = null;
            try {
                StringBuilder strb = new StringBuilder(Config.BASE_URL + INSTALL_LOGIN_PATH + "?");
                strb.append("uuid=" + DeviceUuidFactory.getDeviceUuid(mContext).toString() + "&user=" + mUserId);
                URL url = new URL(strb.toString());
                urlConnection = (HttpURLConnection) url.openConnection();
                InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                BufferedInputStream reader = new BufferedInputStream(in);
                byte[] buffer = new byte[10];
                int bytesRead = -1;

                bytesRead = reader.read(buffer, 0, 10);
                if (bytesRead >= 2) {
                    if ((buffer[0] == 'o') && buffer[1] == 'k') {
                        mTinyDb.putBoolean(INSTALL_LOGIN_PATH, true);
                    }
                }
                reader.close();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (urlConnection != null)
                    urlConnection.disconnect();
            }
        }


        private void sendToServer(Context pCon, TinyDB tinyDb) {
            HttpURLConnection urlConnection = null;
            try {
                StringBuilder strb = new StringBuilder(Config.BASE_URL + INSTALL_PATH + "?");
                String refEncoded = URLEncoder.encode(mRef, "UTF-8");
                strb.append("uuid=" + DeviceUuidFactory.getDeviceUuid(pCon).toString() + "&ref=" + refEncoded);
                URL url = new URL(strb.toString());
                urlConnection = (HttpURLConnection) url.openConnection();
                InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                BufferedInputStream reader = new BufferedInputStream(in);
                byte[] buffer = new byte[10];
                int bytesRead = -1;

                bytesRead = reader.read(buffer,0,10);
                if (bytesRead >= 2) {
                    if ((buffer[0] == 'o') && buffer[1] == 'k') {
                        tinyDb.putBoolean(INSTALL_POSTED, true);
                    }
                }
                reader.close();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (urlConnection != null)
                    urlConnection.disconnect();
            }
        }
    }

    public static void handleInstallReferrer(Context pContext, TinyDB tinyDB) {

        if(tinyDB.getBoolean(INSTALL_POSTED))
            return;

        AsyncTask task = new InstallTask(pContext,1,null,tinyDB);
        task.execute((Object) null);
    }

    public static void handleLogin(TinyDB tinyDb) {
        tinyDb.putBoolean(INSTALL_LOGIN_PATH,false);
    }

    public static void handleInstallLogin(Context pContext, TinyDB tinyDb) {

        if(tinyDb.getBoolean(INSTALL_LOGIN_PATH))
            return;

        String userid = tinyDb.getString("ID");

        AsyncTask task = new InstallTask(pContext,2,userid, tinyDb);
        task.execute((Object) null);
    }

    public static String getWhatsAppUrl(String contentid, String userid ) {

        String extrastr = "&referrer=utm_source%3Dwhatsapp%26utm_user%3D" +userid +"%26utm_content%3D"+contentid;

        return extrastr;
    }
}


package com.daivikapp.social.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Channel {

    @SerializedName("n")
    @Expose
    private String n;
    @SerializedName("i")
    @Expose
    private String i;

    public String getN() {
        return n;
    }

    public void setN(String n) {
        this.n = n;
    }

    public String getI() {
        return i;
    }

    public void setI(String i) {
        this.i = i;
    }

}

package com.daivikapp.social.utilities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.AppCompatButton;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;


import org.json.JSONException;

import com.daivikapp.social.R;

/**
 * Created by ANDROID PC on 12/20/2017.
 */

public class LogoutPopup {
    private static final LogoutPopup ourInstance = new LogoutPopup();
    private static AppCompatButton buttonConfirm, buttonCancel;
    private static Context ac;

    public static LogoutPopup getInstance() {
        return ourInstance;
    }

    private LogoutPopup() {
    }

    @SuppressLint("ResourceAsColor")
    public static void logout_Popup(final Activity ac) throws JSONException {

        //Creating a LayoutInflater object for the dialog box
        LayoutInflater li = LayoutInflater.from( ac );
        //Creating a view to get the dialog box
        View confirmDialog = li.inflate( R.layout.dialog_logout, null );

        //Initizliaing confirm button fo dialog box and edittext of dialog box
        buttonConfirm = (AppCompatButton) confirmDialog.findViewById( R.id.buttonConfirm );
        buttonCancel = (AppCompatButton) confirmDialog.findViewById( R.id.buttonCancel );

        TextView titlepop = (TextView) confirmDialog.findViewById( R.id.titlepop );
        titlepop.setText( R.string.do_you_want_to_logout);
        buttonCancel.setText( R.string.no);
        buttonConfirm.setText( R.string.yes );
        //Creating an alertdialog builder
        AlertDialog.Builder alert = new AlertDialog.Builder( ac );


        //Adding our dialog box to the view of alert dialog
        alert.setView( confirmDialog );

        //Creating an alert dialog
        final AlertDialog alertDialog = alert.create();
        alertDialog.getWindow().setBackgroundDrawable( new ColorDrawable( Color.TRANSPARENT ) );
        //Displaying the alert dialog
        alertDialog.show();

        //On the click of the confirm button from alert dialog
        buttonCancel.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();

            }
        } );
        buttonConfirm.setOnClickListener( new View.OnClickListener() {


            @Override
            public void onClick(View v) {
                //Hiding the alert dialog
                TinyDB tinyDb = new TinyDB( ac );
                tinyDb.putBoolean("isLogin", false);


            }


        } );
    }
}

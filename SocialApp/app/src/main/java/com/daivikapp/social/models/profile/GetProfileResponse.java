
package com.daivikapp.social.models.profile;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetProfileResponse {

    @SerializedName("rc")
    @Expose
    private int rc;
    @SerializedName("user_content")
    @Expose
    private UserContent userContent;

    public int getRc() {
        return rc;
    }

    public void setRc(int rc) {
        this.rc = rc;
    }

    public UserContent getUserContent() {
        return userContent;
    }

    public void setUserContent(UserContent userContent) {
        this.userContent = userContent;
    }

}

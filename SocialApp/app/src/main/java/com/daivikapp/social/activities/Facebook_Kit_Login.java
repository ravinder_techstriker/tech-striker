package com.daivikapp.social.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.View;

import com.facebook.CallbackManager;
import com.facebook.accountkit.AccessToken;
import com.facebook.accountkit.AccountKitLoginResult;
import com.facebook.accountkit.ui.AccountKitActivity;
import com.facebook.accountkit.ui.AccountKitConfiguration;
import com.facebook.accountkit.ui.LoginType;
import com.facebook.login.LoginManager;

import com.daivikapp.social.R;
import com.daivikapp.social.utilities.NetworkChecking;
import com.daivikapp.social.utilities.ProgDialog;
import com.daivikapp.social.utilities.TinyDB;

import com.facebook.accountkit.AccountKit;
import com.facebook.accountkit.Account;
import com.facebook.accountkit.PhoneNumber;
import com.facebook.accountkit.AccountKitCallback;
import com.facebook.accountkit.AccountKitError;



public class Facebook_Kit_Login extends AppCompatActivity {

    private CallbackManager callbackManager;
    private AccessToken accessToken;

    NetworkChecking networkChecking;
    ProgDialog prog;
    Activity ac;
    private String TAG = "Social App Facebook_Kit_Login Tag";

    TinyDB tinyDb;

    CardView login_btn;
    String accountKitId, phoneNumberString, email;

    public static int APP_REQUEST_CODE = 99;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ac = this;

        tinyDb = new TinyDB( ac );
        LoginManager.getInstance().logOut();

        networkChecking = new NetworkChecking();
        prog = new ProgDialog();
        callbackManager = CallbackManager.Factory.create();

        login_btn = (CardView) findViewById(R.id.login_btn);

        login_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                phoneLogin(v);
            }
        });

    }

    public void phoneLogin(final View view) {
        final Intent intent = new Intent(ac, AccountKitActivity.class);
        AccountKitConfiguration.AccountKitConfigurationBuilder configurationBuilder =
                new AccountKitConfiguration.AccountKitConfigurationBuilder(
                        LoginType.PHONE,
                        AccountKitActivity.ResponseType.TOKEN); // or .ResponseType.TOKEN
        // ... perform additional configuration ...
        intent.putExtra(
                AccountKitActivity.ACCOUNT_KIT_ACTIVITY_CONFIGURATION,
                configurationBuilder.build());
        startActivityForResult(intent, APP_REQUEST_CODE);
    }


    @Override
    protected void onActivityResult(
            final int requestCode,
            final int resultCode,
            final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == APP_REQUEST_CODE) {
            // confirm that this response matches your request
            AccountKitLoginResult loginResult = data.getParcelableExtra(AccountKitLoginResult.RESULT_KEY);


            String toastMessage;
            if (loginResult.getError() != null) {
                toastMessage = loginResult.getError().getErrorType().getMessage();
                // showErrorActivity(loginResult.getError());
            } else if (loginResult.wasCancelled()) {
                toastMessage = "Facebook_Kit_Login Cancelled";
            } else {


                if (loginResult.getAccessToken() != null) {
                    toastMessage = "Success:" + loginResult.getAccessToken().getAccountId();
                } else {
                    toastMessage = String.format(
                            "Success:%s...",
                            loginResult.getAuthorizationCode().substring(0, 10));
                }



                AccountKit.getCurrentAccount(new AccountKitCallback<Account>() {
                    @Override
                    public void onSuccess(final Account account) {
                        // Get Account Kit ID
                         accountKitId = account.getId();



                        // Get phone number
                        PhoneNumber phoneNumber = account.getPhoneNumber();
                         phoneNumberString = "0";
                        if (phoneNumber != null) {
                             phoneNumberString = phoneNumber.toString();
                        }

                        // Get email
                        email = account.getEmail();

//                        //System.out.println("Account detailll    "+accountKitId+"  "+phoneNumberString+"  "+email);

                        // Success! Start next activity...
                        Intent mainIntent = new Intent( getApplicationContext(), SignUp.class );
                        mainIntent.putExtra("FB_ID", accountKitId);
                        mainIntent.putExtra("PHONE", phoneNumberString);
                        mainIntent.putExtra("EMAIL", email);

                        startActivity( mainIntent );
                        finish();
                        overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);

                        // Surface the result to your user in an appropriate way.
//                        Toast.makeText(
//                                ac,
//                                accountKitId,
//                                Toast.LENGTH_LONG)
//                                .show();
                    }

                    @Override
                    public void onError(final AccountKitError error) {
                        // Handle Error
//                        //System.out.println("exceptionnnnnnnnnn     "+error);
                    }
                });








            }


        }
    }









}


package com.daivikapp.social.utilities;

import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;


import org.json.JSONException;

import java.util.ArrayList;

import com.daivikapp.social.R;
import com.daivikapp.social.adapter.DrawerItemCustomAdapter;
import com.daivikapp.social.fragment.AllFeeds;
import com.daivikapp.social.fragment.UserProfile;
import com.daivikapp.social.models.DataModel_;

/**
 * Created by Android on 4/20/2017.
 */

public class LeftMenu {

    AppCompatActivity ac;
    ArrayList<DataModel_> dataModels=new ArrayList<>();
    public  static  DrawerLayout drawer;
    TinyDB tinyDb;
    String user_type;


   public void init(AppCompatActivity activity, final DrawerLayout drawerLayout, Toolbar toolbar, ListView mDrawerList, ImageView menu_nav){
        ac=activity;

       tinyDb = new TinyDB( ac );


           drawer=drawerLayout;
           DataModel_ data=new DataModel_();
           data.setName(activity.getString(R.string.home));
           data.setImage(R.drawable.menu_home01);
           dataModels.add(data);

           data=new DataModel_();
           data.setName(activity.getString(R.string.edit_profile));
          data.setImage(R.drawable.menu_edit_profile01);
           dataModels.add(data);


       data=new DataModel_();
       data.setName(activity.getString(R.string.videos));
       data.setImage(R.drawable.menu_video01);
       dataModels.add(data);

       data=new DataModel_();
       data.setName(activity.getString(R.string.rate));
       data.setImage(R.drawable.menu_rating01);
       dataModels.add(data);


           data=new DataModel_();
           data.setName(activity.getString(R.string.log_out));
       data.setImage(R.drawable.menu_logout01);
           dataModels.add(data);










        DrawerItemCustomAdapter adapter = new DrawerItemCustomAdapter(activity, R.layout.list_view_item_row, dataModels);
        mDrawerList.setAdapter(adapter);
        mDrawerList.setOnItemClickListener(new Home_ActivityDrawerItemClickListener());
//      drawerLayout.setDrawerListener(menu_nav);

       menu_nav.setOnClickListener(new View.OnClickListener() {
           @Override
                   public void onClick(View view) {
               if(drawerLayout.isDrawerOpen(Gravity.LEFT)){
                   drawerLayout.closeDrawer(Gravity.LEFT);
               }else{
                   drawerLayout.openDrawer(Gravity.LEFT);
               }
           }
       });


    }

    private class Home_ActivityDrawerItemClickListener implements AdapterView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            selectItem(position);
        }
    }

    private void selectItem(int position) {

        android.support.v4.app.Fragment fragment = null;
        android.app.FragmentManager notifications ;
        android.app.FragmentTransaction fragmentTransaction2;



        switch (position) {
            case 0:
                tinyDb.putString("TYPE", "POPULAR");
                notifications = ac.getFragmentManager();
                fragmentTransaction2 = notifications.beginTransaction();
                fragmentTransaction2.setCustomAnimations(R.animator.slide_up, 0, 0, R.animator.slide_down);
                AllFeeds center = new AllFeeds();
                fragmentTransaction2.replace(R.id.fragment_container, android.app.Fragment.instantiate(ac, center.getClass().getName()));
                fragmentTransaction2.addToBackStack(null);
                fragmentTransaction2.commit();

                drawer.closeDrawer(Gravity.LEFT);
                break;
            case 1:

                notifications = ac.getFragmentManager();
                fragmentTransaction2 = notifications.beginTransaction();
                fragmentTransaction2.setCustomAnimations(R.animator.slide_up, 0, 0, R.animator.slide_down);
                UserProfile center1 = new UserProfile();
                fragmentTransaction2.replace(R.id.fragment_container, android.app.Fragment.instantiate(ac, center1.getClass().getName()));
                fragmentTransaction2.addToBackStack(null);
                fragmentTransaction2.commit();
                drawer.closeDrawer(Gravity.LEFT);


                break;

            case 2:

                tinyDb.putString("TYPE", "VID");
                notifications = ac.getFragmentManager();
                fragmentTransaction2 = notifications.beginTransaction();
                fragmentTransaction2.setCustomAnimations(R.animator.slide_up, 0, 0, R.animator.slide_down);
                AllFeeds center2 = new AllFeeds();
                fragmentTransaction2.replace(R.id.fragment_container, android.app.Fragment.instantiate(ac, center2.getClass().getName()));
                fragmentTransaction2.addToBackStack(null);
                fragmentTransaction2.commit();
                drawer.closeDrawer(Gravity.LEFT);

                break; case 3:

                RateUs.rate_us(ac);
                drawer.closeDrawer(Gravity.LEFT);

                break;

                case 4:
                try {
                    LogoutPopup.logout_Popup(ac);
                } catch (JSONException e) {
//                    e.printStackTrace();
                }
                break;


            default:
                break;

        }


        if (fragment != null) {
            FragmentManager fragmentManager = ac.getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.fragment_container, fragment).commit();



        } else {
            Log.e("MainActivity", "Error in creating fragment");
        }
    }

}

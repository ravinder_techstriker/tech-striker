package com.daivikapp.social.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Paint;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.widget.CircularProgressDrawable;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.davidecirillo.multichoicerecyclerview.MultiChoiceAdapter;
import com.facebook.drawee.view.SimpleDraweeView;
import com.github.lzyzsd.circleprogress.CircleProgress;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.krishna.fileloader.FileLoader;
import com.krishna.fileloader.listener.MultiFileDownloadListener;
import com.krishna.fileloader.request.MultiFileLoadRequest;
import com.pierfrancescosoffritti.androidyoutubeplayer.player.YouTubePlayer;
import com.pierfrancescosoffritti.androidyoutubeplayer.player.YouTubePlayerView;
import com.pierfrancescosoffritti.androidyoutubeplayer.player.listeners.AbstractYouTubePlayerListener;
import com.pierfrancescosoffritti.androidyoutubeplayer.player.listeners.YouTubePlayerInitListener;
import com.plattysoft.leonids.ParticleSystem;
import com.rygelouv.audiosensei.player.AudioSenseiPlayerView;
import com.daivikapp.social.R;
import com.daivikapp.social.models.Comment;
import com.daivikapp.social.models.Contentlist;
import com.daivikapp.social.models.GetCommentResponce;
import com.daivikapp.social.models.Like;
import com.daivikapp.social.models.Option;
import com.daivikapp.social.utilities.MainApplication;
import com.daivikapp.social.utilities.NetworkChecking;
import com.daivikapp.social.utilities.ProgDialog;
import com.daivikapp.social.utilities.SnakeBaar;
import com.daivikapp.social.utilities.TinyDB;
import com.daivikapp.social.utilities.WrapContentDraweeView;

import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import cn.jzvd.JZVideoPlayer;
import cn.jzvd.JZVideoPlayerStandard;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.daivikapp.social.fragment.AllFeeds.contentlistMain;
import static com.daivikapp.social.utilities.MainApplication.getInstance;
import static com.daivikapp.social.utilities.WrapContentDraweeView.mGifList;

public class ContentDetail  extends Activity implements View.OnClickListener {

    Activity ac;
    List<String> pos_item_tList = new ArrayList();
    RelativeLayout  main_lay;
    TextView name, date, heading, description;
    TextView share, like, comment;
    LinearLayout comment_lay, like_lay, share_lay;
    SimpleDraweeView profile_image;
    int clicked_post_position;
    RecyclerView recycle_post, recycle_comment;
    All_Media_Adapter all_post_adapter;
    LinearLayoutManager layoutManager;
    CommentAdapter commentAdapter;
    LikeAdapter likeAdapter;
    Dialog show1;
    String contentId;
    public static  RelativeLayout reg_toolbar;
    NetworkChecking networkChecking;
    ProgDialog prog;
    SnakeBaar snakeBaar;
    List<Comment> all_commentList = new ArrayList();
    List<Option> all_likesList = new ArrayList();
    TypedArray icons;
    ArrayList<String> pathsList;
    BottomSheetBehavior sheetBehavior;
    CardView layoutBottomSheet;
    RelativeLayout bottom_sheet_lay;
    Button  send;
    ImageView cancel;
    SimpleDraweeView user_image;
    EditText comment_edt;
    RelativeLayout donut_progress_layout;
    CircleProgress donut_progress;
    String responceCode;
    TinyDB tinyDb;
    LinearLayoutManager  layoutManager1;
    RecyclerView recycle_rections, recycle_tags;

    Contentlist contentlist;
    ImageView back;

    AudioSenseiPlayerView audio_player;

    MyAdapter myAdapter;

    RecyclerView.RecycledViewPool mLikesPool;
    RecyclerView.RecycledViewPool mTagsPool;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.fragment_con_detail );

        ac = this;
        tinyDb = new TinyDB( ac );
        mGifList.clear();

        mLikesPool = new RecyclerView.RecycledViewPool();
        mTagsPool = new RecyclerView.RecycledViewPool();

        // Tracking the screen view
        getInstance().trackScreenView("ContentDetail", tinyDb.getString("ID"));

        networkChecking = new NetworkChecking();
        prog = new ProgDialog();
        prog.progDialog(ac);
        snakeBaar = new SnakeBaar();

        name = (TextView)findViewById(R.id.name);
        date = (TextView)findViewById(R.id.date);

        back = (ImageView) findViewById(R.id.back);
        profile_image = (SimpleDraweeView) findViewById(R.id.profile_image);
        reg_toolbar = (RelativeLayout) findViewById( R.id.reg_toolbar );


        audio_player = (AudioSenseiPlayerView) findViewById( R.id.audio_player );

        heading = (TextView)findViewById(R.id.heading);
        description = (TextView)findViewById(R.id.description);

        share = (TextView)findViewById(R.id.share);
        like = (TextView)findViewById(R.id.like);
        comment = (TextView)findViewById(R.id.comment);

        recycle_post = (RecyclerView) findViewById(R.id.recycle_post);
        recycle_rections = (RecyclerView) findViewById(R.id.recycle_rections);
        recycle_tags = (RecyclerView) findViewById(R.id.recycle_tags);

        comment_edt = (EditText) findViewById(R.id.comment_edt);
        send = (Button) findViewById(R.id.send);
        cancel = (ImageView) findViewById(R.id.cancel);
        cancel.setOnClickListener(this);

        user_image = (SimpleDraweeView) findViewById(R.id.user_image);
        main_lay = (RelativeLayout) findViewById(R.id.main_lay);

        layoutBottomSheet = (CardView)findViewById(R.id.bottom_sheet);
        bottom_sheet_lay = (RelativeLayout)findViewById(R.id.bottom_sheet_lay);

        recycle_comment = (RecyclerView) findViewById(R.id.recycle_comment);
        donut_progress_layout = (RelativeLayout) findViewById(R.id.donut_progress_layout);
        donut_progress = (CircleProgress) findViewById(R.id.donut_progress);

        sheetBehavior = BottomSheetBehavior.from(layoutBottomSheet);

        sheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                switch (newState) {
                    case BottomSheetBehavior.STATE_HIDDEN:
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED: {
//                        btnBottomSheet.setText("Close Sheet");
                    }
                    break;
                    case BottomSheetBehavior.STATE_COLLAPSED: {
//                        btnBottomSheet.setText("Expand Sheet");
                    }
                    break;
                    case BottomSheetBehavior.STATE_DRAGGING:
                        break;
                    case BottomSheetBehavior.STATE_SETTLING:
                        break;
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                getFragmentManager().popBackStack();
              onBackPressed();

//                toolBarVisibility();

            }
        });

        comment_lay = (LinearLayout) findViewById(R.id.comment_lay);
        like_lay = (LinearLayout) findViewById(R.id.like_lay);
        share_lay = (LinearLayout) findViewById(R.id.share_lay);

       like_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // Tracking Event
                MainApplication.getInstance().trackEvent("Content Detail", "Like", "Like Clicked", tinyDb.getString("ID"));

                // custom dialog
                show1 = new Dialog(ac, R.style.AlertDialogCustom);
                show1.setContentView(R.layout.dialog_like);
                show1.setTitle("Select Reaction");

                RecyclerView recycle_options = (RecyclerView) show1.findViewById(R.id.recycle_options);

                contentId = contentlist.getId();

                likeAdapter = new LikeAdapter(ac);
                recycle_options.setLayoutManager(new GridLayoutManager(ac, 3));
                recycle_options.setAdapter(likeAdapter);
                show1.show();

            }
        } );

        comment_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // Tracking Event
                MainApplication.getInstance().trackEvent("Content Detail", "Comment", "Comment Viewed", tinyDb.getString("ID"));


                contentId = contentlist.getId();
                getComments(contentId, "0", "500000");

                if (sheetBehavior.getState() != BottomSheetBehavior.STATE_EXPANDED) {
                    sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                } else {
                    sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                }
                LinearLayoutManager layoutManager = new LinearLayoutManager(ac, LinearLayoutManager.VERTICAL, false);
                commentAdapter = new CommentAdapter(ac);
                recycle_comment.setLayoutManager(layoutManager);

            }
        } );

        share_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // Tracking Event
                MainApplication.getInstance().trackEvent("Content Detail", "Share", "Post Shared", tinyDb.getString("ID"));

                final List<MultiFileLoadRequest> uris = new ArrayList<>() ;

                if (contentlist.getType().equals("vid")){
                    donut_progress_layout.setVisibility( View.VISIBLE );
                    donut_progress.setProgress( 0 );

                    for (int i = 0; i < contentlist.getVideos().size(); i++) {
                        MultiFileLoadRequest multiFileLoadRequest = new MultiFileLoadRequest(contentlist.getVideos().get(i), Environment.DIRECTORY_DOWNLOADS, FileLoader.DIR_EXTERNAL_PUBLIC, true);
                        uris.add(multiFileLoadRequest);
                    }

                    whatsApp(uris);
                }else if (contentlist.getType().equals("img")){
                    donut_progress_layout.setVisibility( View.VISIBLE );
                    donut_progress.setProgress( 0 );
                    for (int i = 0; i < contentlist.getImages().size(); i++) {
                        MultiFileLoadRequest multiFileLoadRequest = new MultiFileLoadRequest(contentlist.getImages().get(i), Environment.DIRECTORY_DOWNLOADS, FileLoader.DIR_EXTERNAL_PUBLIC, true);
                        uris.add(multiFileLoadRequest);
                    }

                    whatsApp(uris);
                }else  if (contentlist.getType().equals("uvid")) {


                    Intent i = new Intent(Intent.ACTION_SEND);
                    i.setType("text/plain");
                    i.setPackage("com.whatsapp");

                    i.putExtra(Intent.EXTRA_TEXT, contentlist.getTags()+"\n"+contentlist.getTitle()+"\n"+"\nhttps://www.youtube.com/watch?v="+contentlist.getVideoimages().get(0).getV()+"\n\n"+getString(R.string.share_link)+" https://play.google.com/store/apps/details?id=com.panagola.app.om");
                    startActivityForResult(i , 4);
                }else{
                    Intent i = new Intent(Intent.ACTION_SEND);
                    i.setType("text/plain");
                    i.setPackage("com.whatsapp");
                    i.putExtra(Intent.EXTRA_TEXT, contentlist.getTags()+"\n"+contentlist.getTitle()+"\n"+"\n\n"+getString(R.string.share_link)+" https://play.google.com/store/apps/details?id=com.panagola.app.om"); i.putExtra(Intent.EXTRA_TEXT, contentlist.getTitle()+"\n"+contentlist.getDesc());
                    startActivityForResult(i , 4);

                }

            }
        } );

        send.setOnClickListener(this);



        new LongOperation(ac).execute();

    }

//    @Override
//    public void onWindowFocusChanged(boolean hasFocus) {
//
//        new LongOperation().execute();
//
//        super.onWindowFocusChanged(hasFocus);
//    }

    @Override
    public void onBackPressed() {
        JZVideoPlayer.releaseAllVideos();

        try {
            audio_player.stop();
            audio_player.setVisibility(View.GONE);
        } catch (Exception e) {
//            e.printStackTrace();
        }

        if(prog != null)
            prog.destroy();

    finish();
    super.onBackPressed();
    }


    @Override
    public void onClick(View v) {

        if (v == cancel) {
            if (sheetBehavior.getState() != BottomSheetBehavior.STATE_EXPANDED) {
                sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            } else {
                sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            }
        } else if (v == send){

            // Tracking Event
            MainApplication.getInstance().trackEvent("Content Detail", "Comment", "Comment Added", tinyDb.getString("ID"));
            if (!comment_edt.getText().toString().isEmpty()) {

                Date today = new Date();
                SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss a");
                String dateToStr = format.format(today);
//                //System.out.println(dateToStr);

                String[] array = dateToStr.split(" ");
                postCommentToServer(comment_edt.getText().toString(),contentId, clicked_post_position);

                comment_edt.setText("");

                if (sheetBehavior.getState() != BottomSheetBehavior.STATE_EXPANDED) {
                    sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                } else {
                    sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                }

                InputMethodManager imm = (InputMethodManager) ac.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

                new ParticleSystem(ac, 20, R.drawable.comment01, 5000)
                        .setSpeedRange(0.1f, 0.25f)
                        .setRotationSpeedRange(10, 20)
                        .setInitialRotationRange(0, 360)
                        .oneShot(comment, 10);

            }else {
                Toast.makeText(ac, "Please add comment", Toast.LENGTH_SHORT).show();
            }
        }

    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {


        // Check which request we're responding to
        if (requestCode == 4) {
            contentId = contentlist.getId();
            postShareToServer(contentId);

        }

    }




    public class All_Media_Adapter extends RecyclerView.Adapter<All_Media_Adapter.MyViewHolder> {



        int VIEW_TYPE1 = 0;
        Context con;
        /**/


        All_Media_Adapter(Context c) {
            con = c;
        }

        @Override
        public int getItemViewType(int position) {
            // depends on your problem

                return VIEW_TYPE1;

        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


            View view;
            MyViewHolder vh ;

                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_detail, parent, false);
                vh = new MyViewHolder( view );
                return vh;



        }

        @Override
        public void onBindViewHolder(final MyViewHolder holder, final int position) {


            MyViewHolder viewHolder = (MyViewHolder) holder;


            if (contentlist.getType().equals("uvid") ) {
                holder.image1.setVisibility(View.GONE);
                holder.video_view1.setVisibility(View.GONE);
                holder.youtube_player_view.setVisibility(View.VISIBLE);



                holder.youtube_player_view.initialize(new YouTubePlayerInitListener() {
                    @Override
                    public void onInitSuccess(@NonNull final YouTubePlayer initializedYouTubePlayer) {
                        initializedYouTubePlayer.addListener(new AbstractYouTubePlayerListener() {
                            @Override
                            public void onReady() {
                                String videoId = contentlist.getVideoimages().get(0).getV();
//                                String videoId = "H-4be6Rwr_M";
                                initializedYouTubePlayer.cueVideo(videoId, 0);
                            }
                        });
                    }
                }, true);



            }else if (contentlist.getType().equals("vid")){

                holder.image1.setVisibility(View.GONE);
                holder.youtube_player_view.setVisibility(View.GONE);

                try {

                    holder.video_view1.setUp(pos_item_tList.get(position),
                            JZVideoPlayerStandard.SCREEN_WINDOW_NORMAL,
                            "");
                    Glide.with(ac)
                            .load(contentlist.getVideoimages().get(0).getI())
                            .into(holder.video_view1.thumbImageView);



                } catch (Exception e) {
//                    e.printStackTrace();
                }



            }else if (contentlist.getType().equals("img")) {

                holder.video_view1.setVisibility(View.GONE);
                holder.youtube_player_view.setVisibility(View.GONE);


                CircularProgressDrawable circularProgressDrawable = new CircularProgressDrawable(ac);
                circularProgressDrawable.setStrokeWidth(5f);
                circularProgressDrawable.setCenterRadius(30f);
                circularProgressDrawable.start();


                holder.image1.setImageURI(pos_item_tList.get(position),ac);

                String uri = pos_item_tList.get(position);

                String extension = uri.substring(uri.lastIndexOf("."));


                if (extension.equals(".gif")) {
                    holder.play_gif.setVisibility(View.VISIBLE);

                    holder.image1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (holder.image1.onClick(position)){
                                holder.play_gif.setVisibility(View.GONE);
                            }else{
                                holder.play_gif.setVisibility(View.VISIBLE);
                            }
                        }
                    });
                }


            }

        }


        @Override
        public int getItemCount() {

            return pos_item_tList.size();
        }




        public class MyViewHolder extends RecyclerView.ViewHolder {
            WrapContentDraweeView image1;
            YouTubePlayerView youtube_player_view;

            ImageView play_gif;

            JZVideoPlayerStandard video_view1;


            public MyViewHolder(View itemView) {
                super( itemView );
                // get the reference of item view's



                video_view1 = (JZVideoPlayerStandard) itemView.findViewById(R.id.video_view);

                image1 = (WrapContentDraweeView) itemView.findViewById(R.id.image1);
                youtube_player_view = (YouTubePlayerView) itemView.findViewById(R.id.youtube_player_view);
                play_gif = (ImageView) itemView.findViewById(R.id.play_gif);

            }
        }
    }



    public class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.MyViewHolder> {


        public CommentAdapter(Context c) {}

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            // infalte the item Layout
            View v = LayoutInflater.from( parent.getContext() ).inflate( R.layout.row_comment, parent, false );
            // set the view's size, margins, paddings and layout parameters
            MyViewHolder vh = new MyViewHolder( v ); // pass the view to View Holder
            return vh;
        }

        @Override
        public void onBindViewHolder(final MyViewHolder holder, final int position) {


            MyViewHolder viewHolder = (MyViewHolder) holder;

            try {
                holder.name.setText( all_commentList.get( position ).getUser().getName());
            } catch (Exception e) {
//                e.printStackTrace();
            }

            try {
                holder.comment.setText( all_commentList.get( position ).getComment() );
            } catch (Exception e) {
//                e.printStackTrace();
            }

            try {
                holder.date.setText( all_commentList.get( position ).getDisplayTime());
            } catch (Exception e) {
//                e.printStackTrace();
            }

            try {
                holder.profile_image.setImageURI(Uri.parse(all_commentList.get(position).getUser().getUimage()));
            } catch (Exception e) {
//                e.printStackTrace();
            }

            ((MyViewHolder) holder).itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    tinyDb.putString("ClIK_USER_ID", all_commentList.get( position ).getUser().getId());

                    Intent mainIntent = new Intent(getApplicationContext(), UserDetailAc.class);
                    startActivity(mainIntent);
                    finish();
                    overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);

                }
            });

        }

        @Override
        public int getItemCount() {
            return all_commentList.size();
        }


        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView name, comment, date;
            SimpleDraweeView profile_image;

            public MyViewHolder(View itemView) {
                super( itemView );
                // get the reference of item view's
                name = (TextView) itemView.findViewById( R.id.name );
                comment = (TextView) itemView.findViewById( R.id.comment );
                date = (TextView) itemView.findViewById( R.id.date );
                profile_image = (SimpleDraweeView) itemView.findViewById( R.id.profile_image );

            }
        }
    }


    public class LikeAdapter extends RecyclerView.Adapter<LikeAdapter.MyViewHolder> {



        public LikeAdapter(Context c) {}

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            // infalte the item Layout
            View v = LayoutInflater.from( parent.getContext() ).inflate( R.layout.row_reaction, parent, false );
            // set the view's size, margins, paddings and layout parameters
            MyViewHolder vh = new MyViewHolder( v ); // pass the view to View Holder
            return vh;
        }

        @Override
        public void onBindViewHolder(final MyViewHolder holder, final int position) {


            MyViewHolder viewHolder = (MyViewHolder) holder;

            try {

                holder.image.setImageResource(icons.getResourceId(position, 0));
                holder.name.setText(all_likesList.get(position).getName());

            } catch (Exception e) {
//                e.printStackTrace();
            }

            ((MyViewHolder) holder).itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    show1.dismiss();



                    postLikeToServer(String.valueOf(position), contentId, clicked_post_position);
                    int numLike = contentlist.getNumlikes();
                    numLike = numLike+1;
                    contentlist.setNumlikes(numLike);

                    Like like1 = new Like();
                    like1.setK(position);
                    like1.setId("0");

                    contentlist.getLikes().add(like1);

                    try {
                        myAdapter.notifyDataSetChanged();
                    } catch (Exception e) {}

                    new ParticleSystem(ac, 20, icons.getResourceId(position, 0), 5000)
                            .setSpeedRange(0.1f, 0.25f)
                            .setRotationSpeedRange(0, 0)
                            .setInitialRotationRange(0, 0)
                            .oneShot(like, 10);




                }
            });

        }

        @Override
        public int getItemCount() {
            return all_likesList.size();
        }


        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView name;
            SimpleDraweeView image;

            public MyViewHolder(View itemView) {
                super( itemView );
                // get the reference of item view's
                name = (TextView) itemView.findViewById( R.id.name );;
                image = (SimpleDraweeView) itemView.findViewById( R.id.image );

            }
        }
    }

    private void getComments(String content_id, String offset, String num_reslt) {

        if (networkChecking.isNetworkAvailable( ac )) {
            prog.progDialog( ac );


            all_commentList = new ArrayList<>();
            RequestBody lang_part = RequestBody.create(MediaType.parse("multipart/form-data"), tinyDb.getString("LANG"));
            RequestBody offset_part = RequestBody.create(MediaType.parse("multipart/form-data"), offset);
            RequestBody content_id_part = RequestBody.create(MediaType.parse("multipart/form-data"), content_id);
            RequestBody numreslt_part = RequestBody.create(MediaType.parse("multipart/form-data"), num_reslt);



            MainApplication.getApiService().getComment("content/comments/get/"+tinyDb.getString("ID"),content_id_part, offset_part, numreslt_part, lang_part).enqueue(new Callback<JsonElement>() {
                @Override
                public void onResponse(Call<JsonElement> call, Response<JsonElement > response) {
                    prog.hideProg();
                    if (response.isSuccessful()) {

                        try {

                            try {
                                JSONObject jsonObj = new JSONObject(String.valueOf(response.body()));
                                responceCode =  jsonObj.getString("rc");

                            }catch (Exception e1){}

                            if (responceCode.equals("1")) {
                                Gson gson = new Gson();
                                GetCommentResponce responcee = null;


//                                //System.out.println("responceeeeeeeeeeeeee   " + response.body());
                                responcee = gson.fromJson(String.valueOf(response.body()), GetCommentResponce.class);

                                all_commentList = responcee.getContent().getComments();
                                recycle_comment.setAdapter(commentAdapter);

                            }else{
                                snakeBaar.showSnackBar( ac, "No comment found.", main_lay );
                            }


                        }catch (Exception e1){}

                    }
                }

                @Override
                public void onFailure(Call<JsonElement > call, Throwable t) {
                    prog.hideProg();
                    t.printStackTrace();
                }
            });








        } else {
            snakeBaar.showSnackBar( ac, getString(R.string.no_internet), main_lay );
        }

    }


    private void postCommentToServer(final String comment_str, String content_id, final int clicked_post_position) {



        if (networkChecking.isNetworkAvailable( ac )) {
            prog.progDialog( ac );


            RequestBody user_id_part = RequestBody.create(MediaType.parse("multipart/form-data"), tinyDb.getString("ID"));
            RequestBody password_part = RequestBody.create(MediaType.parse("multipart/form-data"), tinyDb.getString("PASSWORD"));
            RequestBody content_id_part = RequestBody.create(MediaType.parse("multipart/form-data"), content_id);
            RequestBody comment_part = RequestBody.create(MediaType.parse("multipart/form-data"), comment_str);



            MainApplication.getApiService().addComment("content/comment/add/"+tinyDb.getString("ID"),content_id_part, user_id_part, password_part, comment_part).enqueue(new Callback<JsonElement>() {
                @Override
                public void onResponse(Call<JsonElement> call, Response<JsonElement > response) {
                    prog.hideProg();
                    if (response.isSuccessful()) {

                        try {
                            JSONObject jsonObj = new JSONObject(String.valueOf(response.body()));
                            String responceCode =  jsonObj.getString("rc");
                            if (responceCode.equals("1")){
                                snakeBaar.showSnackBar( ac, getString(R.string.comment_added), main_lay );
                                int num_of_comment = contentlist.getNumcomm();
                                num_of_comment = num_of_comment+1;
                                contentlist.setNumcomm(num_of_comment);

                                comment.setText(contentlist.getNumcomm()+" "+ac.getResources().getString(R.string.comment));
                            }

                        }catch (Exception e1){}

                    }
                }

                @Override
                public void onFailure(Call<JsonElement > call, Throwable t) {
                    prog.hideProg();
                    t.printStackTrace();
                }
            });

        } else {
            snakeBaar.showSnackBar( ac, getString(R.string.no_internet), main_lay );
        }
    }



    private void postLikeToServer(final String likePostion, String content_id, final int clicked_post_position) {



        if (networkChecking.isNetworkAvailable( ac )) {
//            prog.progDialog( ac );


            RequestBody user_id_part = RequestBody.create(MediaType.parse("multipart/form-data"), tinyDb.getString("ID"));
            RequestBody password_part = RequestBody.create(MediaType.parse("multipart/form-data"), tinyDb.getString("PASSWORD"));
            RequestBody content_id_part = RequestBody.create(MediaType.parse("multipart/form-data"), content_id);
            RequestBody comment_part = RequestBody.create(MediaType.parse("multipart/form-data"), likePostion);



            MainApplication.getApiService().addLike("content/like/"+tinyDb.getString("ID"),content_id_part, user_id_part, password_part, comment_part).enqueue(new Callback<JsonElement>() {
                @Override
                public void onResponse(Call<JsonElement> call, Response<JsonElement > response) {
//                    prog.hideProg();
                    if (response.isSuccessful()) {

                        try {
                            JSONObject jsonObj = new JSONObject(String.valueOf(response.body()));
                            String responceCode =  jsonObj.getString("rc");
                            if (responceCode.equals("1")){
                                snakeBaar.showSnackBar( ac, getString(R.string.reaction_posted), main_lay );
                                int num_of_comment = contentlist.getNumlikes();
                                num_of_comment = num_of_comment+1;
                                contentlist.setNumshare(num_of_comment);

                                like.setText(contentlist.getNumlikes()+" "+ac.getResources().getString(R.string.like));

                            }

                        }catch (Exception e1){}

                    }
                }

                @Override
                public void onFailure(Call<JsonElement > call, Throwable t) {
                    prog.hideProg();
                    t.printStackTrace();
                }
            });

        } else {
            snakeBaar.showSnackBar( ac, getString(R.string.no_internet), main_lay );
        }
    }



    private void postShareToServer( String content_id) {



        if (networkChecking.isNetworkAvailable( ac )) {
//            prog.progDialog( ac );


            RequestBody user_id_part = RequestBody.create(MediaType.parse("multipart/form-data"), tinyDb.getString("ID"));
            RequestBody password_part = RequestBody.create(MediaType.parse("multipart/form-data"), tinyDb.getString("PASSWORD"));
            RequestBody content_id_part = RequestBody.create(MediaType.parse("multipart/form-data"), content_id);



            MainApplication.getApiService().addShare("content/share/"+tinyDb.getString("ID"),content_id_part, user_id_part, password_part).enqueue(new Callback<JsonElement>() {
                @Override
                public void onResponse(Call<JsonElement> call, Response<JsonElement > response) {
//                    prog.hideProg();
                    if (response.isSuccessful()) {

                        try {
                            JSONObject jsonObj = new JSONObject(String.valueOf(response.body()));
                            String responceCode =  jsonObj.getString("rc");
                            if (responceCode.equals("1")){
                                snakeBaar.showSnackBar( ac, getString(R.string.share_posted), main_lay );
                                int num_of_comment = contentlist.getNumshare();
                                num_of_comment = num_of_comment+1;
                                contentlist.setNumshare(num_of_comment);
                                share.setText(contentlist.getNumshare()+" "+ac.getResources().getString(R.string.share));

                            }

                        }catch (Exception e1){}

                    }
                }

                @Override
                public void onFailure(Call<JsonElement> call, Throwable t) {
                    prog.hideProg();
                    t.printStackTrace();
                }
            });

        } else {
            snakeBaar.showSnackBar( ac, getString(R.string.no_internet), main_lay );
        }
    }

    public  void whatsApp(final List<MultiFileLoadRequest> uris){
//        //System.out.println("URI list  "+uris);

        pathsList = new ArrayList<>();

        FileLoader.multiFileDownload(ac)
                .fromDirectory(Environment.DIRECTORY_PICTURES, FileLoader.DIR_EXTERNAL_PUBLIC)
                .progressListener(new MultiFileDownloadListener() {
                    @Override
                    public void onProgress(File downloadedFile, int progress, int totalFiles) {

                        donut_progress.setMax(totalFiles);
                        donut_progress.setProgress(progress);
//                        //System.out.println("Path listt-  "+downloadedFile.getAbsolutePath());
                        pathsList.add(downloadedFile.getAbsolutePath());

//                        //System.out.println(uris.size()+"   Path list size  "+pathsList.size());
                        if (pathsList.size() == uris.size()){

                            donut_progress_layout.setVisibility( View.GONE );
                            donut_progress.setProgress( 0 );

                            ArrayList<Uri> uri_lis = new ArrayList<>();
                            for (int i = 0; i < pathsList.size(); i++) {
                                uri_lis.add(Uri.parse(pathsList.get(i)));
                            }



                            if (contentlist.getType().equals("aud")) {
//                                videoshare.setType("audio/*");

                                Intent share = new Intent(Intent.ACTION_SEND);
                                share.putExtra(Intent.EXTRA_STREAM, uri_lis.get(0));
                                share.setType("audio/*");
                                share.setPackage("com.whatsapp");
                                share.putExtra(Intent.EXTRA_TEXT, contentlist.getTags()+"\n"+contentlist.getTitle()+"\n"+"\n\n"+getString(R.string.share_link)+" https://play.google.com/store/apps/details?id=com.panagola.app.om");
                                share.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                                startActivityForResult(share, 4);
                            }else{
                                Intent videoshare = new Intent(Intent.ACTION_SEND);
                                videoshare.setType("*/*");
                                videoshare.setPackage("com.whatsapp");
                                videoshare.putExtra(Intent.EXTRA_TEXT, contentlist.getTags()+"\n"+contentlist.getTitle()+"\n"+"\n\n"+getString(R.string.share_link)+" https://play.google.com/store/apps/details?id=com.panagola.app.om");
                                videoshare.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                                videoshare.putParcelableArrayListExtra(Intent.EXTRA_STREAM,uri_lis);

                                startActivityForResult(videoshare, 4);
                            }


//                            Intent videoshare = new Intent(Intent.ACTION_SEND);
//                            if (contentlist.getType().equals("aud")) {
//                                videoshare.setType("audio/*");
//                            }else{
//                                videoshare.setType("*/*");
//                            }
//                            videoshare.setPackage("com.whatsapp");
////                            videoshare.putExtra(Intent.EXTRA_SUBJECT, all_postList.get(position).getTitle());
////                            videoshare.putExtra(Intent.EXTRA_TEXT, all_postList.get(position).getDesc());
//                            videoshare.putExtra(Intent.EXTRA_TEXT, "Tags:-"+contentlist.getTags()+"\n"+contentlist.getTitle()+"\n"+contentlist.getDesc()+"\n\n"+getString(R.string.share_link)+" https://play.google.com/store/apps/details?id=com.panagola.app.om");videoshare.putParcelableArrayListExtra(Intent.EXTRA_STREAM,uri_lis);
//
//                            startActivityForResult(videoshare, 4);
                        }

                    }

                    @Override
                    public void onError(Exception e, int progress) {
                        super.onError(e, progress);
                    }
                }).loadMultiple(true, uris);
    }



    public class MyAdapterTags extends MultiChoiceAdapter<MyAdapterTags.MyViewHolder> {

        Context mContext;
        List<String> mList;

        public MyAdapterTags(List<String> stringList, Context context) {
            this.mList = stringList;
            this.mContext = context;
        }

        public void updateData(List<String> data) {
            this.mList = data;
            this.notifyDataSetChanged();
        }

        @NonNull
        @Override
        public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view;
            MyViewHolder vh;

            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_tag, parent, false);

            vh = new MyViewHolder(view);
            return vh;
        }

        @Override
        public void onBindViewHolder(final MyViewHolder holder, final int position) {
            super.onBindViewHolder(holder, position);

            holder.textView.setText(mList.get(position));

            holder.main_card.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    tinyDb.putBoolean("Tag_Selected", true);
                    tinyDb.putString("Selected_Tag", holder.textView.getText().toString());

                    Intent mainIntent = new Intent(getApplicationContext(), MainActivity.class);
                    mainIntent.putExtra("Screen", "AllFeeds");
                    startActivity(mainIntent);
                    overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);


                }
            });


        }

        @Override
        public int getItemCount() {
            if(mList != null)
                return mList.size();
            return 0;
        }


        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView textView;
            RelativeLayout main_card;
            RelativeLayout bg_lay;


            public MyViewHolder(View itemView) {
                super(itemView);

                textView = (TextView) itemView.findViewById(R.id.textView);
                main_card = (RelativeLayout) itemView.findViewById(R.id.main_card);
                bg_lay = (RelativeLayout) itemView.findViewById(R.id.bg_lay);

//                textView.setTextColor(getResources().getColor(R.color.colorTags));
                textView.setPaintFlags(textView.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

            }
        }

    }

    public class MyAdapter extends MultiChoiceAdapter<MyAdapter.MyViewHolder> {

        Context mContext;
        List<Like> mList;

        public MyAdapter(List<Like> stringList, Context context) {
            this.mList = stringList;
            this.mContext = context;
        }

        public void updateData(List<Like> data) {
            this.mList = data;
            this.notifyDataSetChanged();
        }

        @NonNull
        @Override
        public MyAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view;
            MyAdapter.MyViewHolder vh;

            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_likes, parent, false);

            vh = new MyAdapter.MyViewHolder(view);
            return vh;
        }

        @Override
        public void onBindViewHolder(MyAdapter.MyViewHolder holder, int position) {
            super.onBindViewHolder(holder, position);

            holder.imageView.setImageResource(icons.getResourceId(mList.get(position).getK(), 0));

        }

        @Override
        public int getItemCount() {
            if(mList != null)
                return mList.size();
            return 0;
        }


        public class MyViewHolder extends RecyclerView.ViewHolder {
            ImageView imageView;


            public MyViewHolder(View itemView) {
                super(itemView);

                imageView = (ImageView) itemView.findViewById(R.id.imageView);

            }
        }


    }





     class LongOperation extends AsyncTask<Void, Void, Long> {

         List<String> options;
         Activity ac;

        LongOperation(Activity acc){
          ac = acc;
        }
         @Override
         protected void onPreExecute() {
//             //System.out.println("content detail 22222");

         }

         protected void onPostExecute(Long result) {

//             //System.out.println("content detail 33333");
             prog.hideProg();
         }
         @Override
         protected Long doInBackground(Void... voids) {
             contentlist = contentlistMain;

             all_likesList = new ArrayList<>();

             options  = Arrays.asList(getResources().getStringArray(R.array.reaction_name_list));

             Resources r = ac.getResources();
             icons = r.obtainTypedArray(R.array.reaction_images_list);

             runOnUiThread(new Runnable() {
                 public void run() {
                     user_image.setImageURI( Uri.parse(tinyDb.getString("IMAGE")) );
                     layoutManager1 = new LinearLayoutManager(ac, LinearLayoutManager.HORIZONTAL, false);
                     recycle_rections.setLayoutManager(layoutManager1);
                     recycle_rections.setRecycledViewPool(mLikesPool);
                     myAdapter = new MyAdapter(contentlist.getLikes(), ac);
                     recycle_rections.setAdapter(myAdapter);

                     layoutManager1 = new LinearLayoutManager(ac, LinearLayoutManager.HORIZONTAL, false);
                     recycle_tags.setLayoutManager(layoutManager1);
                     recycle_tags.setRecycledViewPool(mTagsPool);
                     MyAdapterTags myAdapter2 = new MyAdapterTags(contentlist.getTags(), ac);
                     recycle_tags.setAdapter(myAdapter2);




                     for (int i = 0; i < options.size(); i++) {
                         Option option = new Option();

                         option.setName(options.get(i));
                         all_likesList.add(option);
                     }

                     try {
                         name.setText(contentlist.getUser().getName());
                     } catch (Exception e) {}

                     try {
                         profile_image.setImageURI(Uri.parse(contentlist.getUser().getUimage()));
                     } catch (Exception e) {}

                     try {
                         heading.setText(contentlist.getTitle());
                         description.setText(contentlist.getDesc());
                         date.setText(contentlist.getDisplayTime());
                         comment.setText(contentlist.getNumcomm()+" "+ac.getResources().getString(R.string.comment));
                         like.setText(contentlist.getNumlikes()+" "+ac.getResources().getString(R.string.like));
                         share.setText(contentlist.getNumshare()+" "+ac.getResources().getString(R.string.share));
                     } catch (Exception e) {
                     }


                     try {

                         if (contentlist.getType().equals("vid")){
                             pos_item_tList = contentlist.getVideos();
                             audio_player.setVisibility(View.GONE);

                         }else if (contentlist.getType().equals("img")){
                             pos_item_tList = contentlist.getImages();
                             audio_player.setVisibility(View.GONE);
                         }else if (contentlist.getType().equals("aud")){

                             audio_player.setAudioTarget(contentlist.getAudios().get(0));

                             audio_player.setVisibility(View.VISIBLE);
                             recycle_post.setVisibility(View.GONE);
                         }else if (contentlist.getType().equals("uvid")){
                             pos_item_tList.add(contentlist.getVideoimages().get(0).getV());
                             audio_player.setVisibility(View.GONE);
                             recycle_post.setVisibility(View.VISIBLE);
                         }else{

                             audio_player.setVisibility(View.GONE);
                             recycle_post.setVisibility(View.GONE);

                         }
                     } catch (Exception e) {  }

                     layoutManager = new LinearLayoutManager( ac, LinearLayoutManager.VERTICAL, false );

                     all_post_adapter = new All_Media_Adapter( ac );
                     recycle_post.setHasFixedSize( true );
                     recycle_post.setItemViewCacheSize( 10 );
                     recycle_post.setDrawingCacheEnabled( true );
                     recycle_post.setLayoutManager( layoutManager );
                     recycle_post.setAdapter(all_post_adapter);
                     recycle_post.setNestedScrollingEnabled(false);


                 }
             });


                        return null;
         }



        @Override
        protected void onProgressUpdate(Void... values) {}
    }
}

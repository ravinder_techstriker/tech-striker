package com.daivikapp.social.Interface;


import com.google.gson.JsonElement;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Url;
import com.daivikapp.social.models.ContentRequest;
import com.daivikapp.social.models.ContentResponce;
import com.daivikapp.social.models.commented_by_user.PostsCommentedByUserResponce;

/**
 * Created by gys on 02-03-2017.
 */

public interface ApiCalls {


//    @GET("getprofessionforfilterlist")
//    Call<GetProfessions> getProfactionForArtist(@Header("Content-Type") String content_type);
//
//

 @Multipart
 @POST()
 Call<JsonElement> getTags(
         @Url String url,
         @Part("lang") RequestBody lang);

 @Multipart
 @POST()
 Call<JsonElement> getChannels(
         @Url String url,
         @Part("lang") RequestBody lang);

 @Multipart
 @POST("user/add")
 Call<JsonElement> addUser(@Part MultipartBody.Part image,
                           @Part("name") RequestBody name,
                           @Part("mobile") RequestBody mobile,
                           @Part("lang") RequestBody lang,
                           @Part("ustaus") RequestBody ustatus,
                           @Part("uplace") RequestBody uplace,
                           @Part("fbdob") RequestBody fdob,
                           @Part("fbgener") RequestBody fgender,
                           @Part("fbname") RequestBody fname,
                           @Part("fbphone") RequestBody fphone,
                           @Part("fbid") RequestBody fbid,
                           @Part("fbemail") RequestBody femail,
                           @Part("gcmregid") RequestBody gcmregid);

 @Multipart
 @POST()
 Call<JsonElement> updateUser(
                          @Url String url,
                           @Part MultipartBody.Part image,
                           @Part("_id") RequestBody id,
                           @Part("pwd") RequestBody pwd,
                           @Part("mobile") RequestBody mobile,
                           @Part("name") RequestBody name,
                           @Part("lang") RequestBody lang,
                           @Part("ustatus") RequestBody ustatus,
                           @Part("uplace") RequestBody uplace,
                           @Part("fbdob") RequestBody fdob,
                           @Part("udob") RequestBody udob,
                           @Part("ugender") RequestBody fgender,
                           @Part("fbname") RequestBody fname,
                           @Part("fbphone") RequestBody fphone,
                           @Part("uemail") RequestBody uemail,
                           @Part("fbemail") RequestBody femail);



 @Multipart
 @POST()
 Call<JsonElement> uploadFiles(
         @Url String url,
         @Part("user._id") RequestBody id,
         @Part("user.pwd") RequestBody password,
         @Part("content.title") RequestBody title,
         @Part("content.desc") RequestBody desc,
         @Part("content.lang") RequestBody lang,
         @Part("content.type") RequestBody type,
         @Part("content.style") RequestBody style,
         @Part("content.tags[]") List<RequestBody>  tags,
         @Part("content.loc") RequestBody loc,
         @Part List<MultipartBody.Part> files);


 @Multipart
 @POST()
 Call<JsonElement> uploadStatus(
         @Url String url,
         @Part("user._id") RequestBody id,
         @Part("user.pwd") RequestBody password,
         @Part("content.title") RequestBody title,
         @Part("content.desc") RequestBody desc,
         @Part("content.lang") RequestBody lang,
         @Part("content.type") RequestBody type,
         @Part("content.style") RequestBody style,
         @Part("content.tags[]") List<RequestBody>  tags,
         @Part("content.loc") RequestBody loc);


//    @Part("content.tags") RequestBody tags,

//    @FieldMap Map<String, String> tags,


 @Multipart
 @POST()
 Call<ContentResponce> getContent(
         @Url String url,
         @Part("type[]") ArrayList<RequestBody> typePart,
         @Part("channel[]") ArrayList<RequestBody> chanalListPart,
         @Part("lang") RequestBody lang,
         @Part("offset") RequestBody offset,
         @Part("numresults") RequestBody num_res,
         @Part("tags[]") ArrayList<RequestBody> tagListPart,
         @Part("user._id") RequestBody id);



 @Multipart
 @POST()
 Call<JsonElement> addComment(
         @Url String url,
         @Part("content._id") RequestBody contentId,
         @Part("comment.user._id") RequestBody userId,
         @Part("comment.user.pwd") RequestBody userPswrd,
         @Part("comment.comment") RequestBody comment);


 @Multipart
 @POST()
 Call<JsonElement> addLike(
         @Url String url,
         @Part("content._id") RequestBody contentId,
         @Part("user._id") RequestBody userId,
         @Part("user.pwd") RequestBody userPswrd,
         @Part("like.k") RequestBody like);


 @Multipart
 @POST()
 Call<JsonElement> addShare(
         @Url String url,
         @Part("content._id") RequestBody contentId,
         @Part("user._id") RequestBody userId,
         @Part("user.pwd") RequestBody userPswrd);

 @Multipart
 @POST()
 Call<JsonElement> addFollow(
         @Url String url,
         @Part("leaderid") RequestBody contentId,
         @Part("user._id") RequestBody userId,
         @Part("follow") RequestBody follow,
         @Part("user.pwd") RequestBody userPswrd);




 @Multipart
 @POST()
 Call<JsonElement> getComment(
         @Url String url,
         @Part("content._id") RequestBody contentId,
         @Part("offset") RequestBody offset,
         @Part("numresults") RequestBody numResult,
         @Part("lang") RequestBody lang);

//    @GET()
//    Call<JsonElement> getUserProfile(@Url String url);


 @Multipart
 @POST()
 Call<JsonElement> getUserProfile(
         @Url String url,
         @Part("id") RequestBody id,
         @Part("user._id") RequestBody userId);


 @Multipart
 @POST()
 Call<ContentResponce> getLikedByUser(
         @Url String url,
         @Part("lang") RequestBody lang,
         @Part("offset") RequestBody offset,
         @Part("numresults") RequestBody num_res,
         @Part("id") RequestBody id);


 @Multipart
 @POST()
 Call<PostsCommentedByUserResponce> getContentCommentedByUser(
         @Url String url,
         @Part("lang") RequestBody lang,
         @Part("offset") RequestBody offset,
         @Part("numresults") RequestBody num_res,
         @Part("id") RequestBody id);


 @Multipart
 @POST()
 Call<JsonElement> getFollowerList(
         @Url String url,
         @Part("id") RequestBody id,
         @Part("offset") RequestBody offset,
         @Part("numresults") RequestBody num_res,
         @Part("userid") RequestBody userId);


 @Multipart
 @POST()
 Call<JsonElement> getFollowingList(
         @Url String url,
         @Part("id") RequestBody id,
         @Part("offset") RequestBody offset,
         @Part("numresults") RequestBody num_res,
         @Part("userid") RequestBody userId);

 // POST GCM/FCM Regustration Id


 @Multipart
 @POST()
 Call<JsonElement> saveRegId(
         @Url String url,
         @Part("user._id") RequestBody userId,
         @Part("user.pwd") RequestBody userPswrd,
         @Part("user.gcmregid") RequestBody gcmRegId);
}

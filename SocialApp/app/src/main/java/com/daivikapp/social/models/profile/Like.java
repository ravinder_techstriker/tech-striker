
package com.daivikapp.social.models.profile;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Like {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("k")
    @Expose
    private int k;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getK() {
        return k;
    }

    public void setK(int k) {
        this.k = k;
    }

}

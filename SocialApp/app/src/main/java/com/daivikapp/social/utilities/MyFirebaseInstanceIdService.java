package com.daivikapp.social.utilities;

import android.content.Context;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.google.gson.JsonElement;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyFirebaseInstanceIdService extends FirebaseInstanceIdService {



    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d("MyFirebaseService", "Refreshed token: " + refreshedToken);

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        sendRegistrationToServer(refreshedToken);
    }

    /**
     * Persist token to third-party servers.
     *
     * Modify this method to associate the user's FCM InstanceID token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
    private void sendRegistrationToServer(String token) {
        // TODO: Implement this method to send token to your app server.
        TinyDB pTinyDb;

        Context context = getApplicationContext();

        pTinyDb = new TinyDB(context);

        Log.d("MyFirebaseService", "Tiny DB : " + pTinyDb.toString());

        String user_id = pTinyDb.getString("ID");

        // Check if user details are available
        if (user_id != null && !user_id.isEmpty()) {

            RequestBody user_id_part = RequestBody.create(MediaType.parse("multipart/form-data"), user_id);
            RequestBody password_part = RequestBody.create(MediaType.parse("multipart/form-data"), pTinyDb.getString("PASSWORD"));
            RequestBody gcmregid_part = RequestBody.create(MediaType.parse("multipart/form-data"), token);

            Log.d("MyFirebaseService", "User _id :" + pTinyDb.getString("ID"));
            Log.d("MyFirebaseService", "password :" + pTinyDb.getString("PASSWORD"));

            MainApplication.getApiService().saveRegId("user/update-reg-id", user_id_part, password_part, gcmregid_part).enqueue(new Callback<JsonElement>() {
                @Override
                public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                    Log.d("MyFirebaseService", "onresponse error : " + response.body().toString());
                }

                @Override
                public void onFailure(Call<JsonElement> call, Throwable t) {
                    Log.e("MyfirebaseService", t.getMessage());
                }
            });
        }
        else {
            // User details aren't available - could be the app is installed the first time
            // Store Token in TinyDB to be sent later
            Log.d("MyFirebaseService", "No User details present...saving token");
            pTinyDb.putString("gcmregid", token);
        }
    }

}

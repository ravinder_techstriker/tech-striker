
package com.daivikapp.social.models;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FollowerListResponce {

    @SerializedName("rc")
    @Expose
    private int rc;
    @SerializedName("flist")
    @Expose
    private List<Flist> flist = null;

    public int getRc() {
        return rc;
    }

    public void setRc(int rc) {
        this.rc = rc;
    }

    public List<Flist> getFlist() {
        return flist;
    }

    public void setFlist(List<Flist> flist) {
        this.flist = flist;
    }

}

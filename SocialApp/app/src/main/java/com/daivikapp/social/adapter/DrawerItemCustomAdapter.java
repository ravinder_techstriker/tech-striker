package com.daivikapp.social.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import java.util.ArrayList;

import com.daivikapp.social.R;
import com.daivikapp.social.models.DataModel_;

/**
 * Created by anupamchugh on 10/12/15.
 */
public class DrawerItemCustomAdapter extends ArrayAdapter<DataModel_> {

    Context mContext;
    int layoutResourceId;
    ArrayList<DataModel_> data = new ArrayList<>();
    private Typeface content;

    public DrawerItemCustomAdapter(Context mContext, int layoutResourceId, ArrayList<DataModel_> data) {

        super(mContext, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.mContext = mContext;
        this.data = data;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View listItem = convertView;
        LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
        listItem = inflater.inflate(layoutResourceId, parent, false);

        ImageView imageViewIcon = (ImageView) listItem.findViewById(R.id.imageViewIcon);
        TextView textViewName = (TextView) listItem.findViewById(R.id.textViewName);

        textViewName.setTypeface(content);

        DataModel_ folder = data.get(position);


        imageViewIcon.setImageResource(folder.getImage());
        textViewName.setText(folder.getName());



        return listItem;
    }
}


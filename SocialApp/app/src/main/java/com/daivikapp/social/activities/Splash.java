package com.daivikapp.social.activities;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.daivikapp.social.R;
import com.daivikapp.social.utilities.TinyDB;
import com.daivikapp.social.utilities.TrackInstall;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Locale;


import static com.daivikapp.social.utilities.MainApplication.getInstance;


public class Splash extends Activity {


    private static int counter;
    private int progressStatus = 0;
    private Handler handler = new Handler();
    Activity ac;
    protected boolean _active = true;
    protected int _splashTime = 1;/*500*/

    private LinearLayout view;
    TinyDB tinyDb;
    private Animation animationSlideInLeft;
    private static final String TAG = "SplashActivity";
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );


//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
//            Window w = getWindow(); // in Activity's onCreate() for instance
//            w.setFlags( WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS );
//        }

        setContentView(R.layout.activity_splash );
        ac = this;

//        getKeyHash();
        // Tracking the screen view
        getInstance().trackScreenView("Splash Screen", "");

        tinyDb = new TinyDB( ac );
        tinyDb.putString("TYPE", "POPULAR");
        Configuration config = getBaseContext().getResources().getConfiguration();
        String lang = tinyDb.getString("LANG");

        Locale locale = new Locale(lang);
        Locale.setDefault(locale);
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());

        //Handling install referrer
        TrackInstall.handleInstallReferrer(ac, tinyDb);

        view = (LinearLayout) findViewById(R.id.llroot );
        animationSlideInLeft = AnimationUtils.loadAnimation( this, android.R.anim.slide_in_left );
        animationSlideInLeft.setDuration( 10 );

        animationSlideInLeft.setAnimationListener( new Animation.AnimationListener() {
            @Override
            public void onAnimationEnd(Animation animation) {
                // if you need to do something
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationStart(Animation animation) {

                overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
            }
        } );
        view.startAnimation( animationSlideInLeft );
        //  Typeface custom_font_texts = Typeface.createFromAsset(getAssets(),"fonts/montserrat_bold.otf");
        // TextView textView= (TextView) findViewById(R.id.splashtxt);
        //  textView.setTypeface(custom_font_texts);


        Thread splashTread = new Thread() {
            @Override
            public void run() {
                try {

                    int waited = 0;
                    while (_active && (waited < _splashTime)) {
                        sleep( 50 );

                        if (_active) {
                            waited += 50;
                        }
                    }
                } catch (Exception e) {

                } finally {
                }
            }

            ;
        };
        splashTread.start();
        progMethod();

    }

    public void progMethod() {

        counter = 0;

          progressBar = (ProgressBar) findViewById(R.id.pbProcessing);
         progressBar.setMax(200);
        new Thread( new Runnable() {

            public void run() {
                while (progressStatus < 50) {
                    progressStatus = doSomeWork();

                    handler.post( new Runnable() {
                        public void run() {
                              progressBar.setProgress(progressStatus);


                        }
                    } );
                }

                handler.post( new Runnable() {

                    public void run() {

                        String lang = tinyDb.getString("LANG");

//                        //System.out.println(getResources().getString(R.string.app_name)+"  selected langgg   "+lang);

                        if (lang.isEmpty()) {
                            Intent mainIntent = new Intent(getApplicationContext(), LanguageChoose.class);
                            startActivity(mainIntent);
                            finish();
                            overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                        } else {




//                            //System.out.println(getResources().getString(R.string.app_name)+"  is Login   "+tinyDb.getBoolean("isLogin"));

//                            if (aBoolean) {
                            if (tinyDb.getBoolean("isLogin")) {

                                Intent mainIntent = new Intent(getApplicationContext(), MainActivity.class);
                                startActivity(mainIntent);
                                overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                                finish();

//                                overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);


                            } else {
//                               Intent mainIntent = new Intent(getApplicationContext(), Facebook_Kit_Login.class);
                               Intent mainIntent = new Intent(getApplicationContext(), Facebook_Kit_Login.class);
                                startActivity(mainIntent);
                                finish();
                                overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                            }
                              progressBar.setVisibility(View.VISIBLE);
                        }
                    }

                });
            }

            //---do some long lasting work here---
            private int doSomeWork() {
                try {
                    //---simulate doing some work---
                    Thread.sleep( 20 );
                } catch (InterruptedException e) {
//                    e.printStackTrace();
                }
                return ++counter;
            }
        }).start();

    }

    private void getKeyHash() {

        PackageInfo info;
        try {
            info = getPackageManager().getPackageInfo("com.daivikapp.social", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md;
                md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String something = new String(Base64.encode(md.digest(), 0));
                //String something = new String(Base64.encodeBytes(md.digest()));
                Log.e("hash key", something);
            }
        } catch (PackageManager.NameNotFoundException e1) {
//            Log.e("name not found", e1.toString());
        } catch (NoSuchAlgorithmException e) {
//            Log.e("no such an algorithm", e.toString());
        } catch (Exception e) {
//            Log.e("exception", e.toString());
        }
    }
}

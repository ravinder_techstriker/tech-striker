package com.daivikapp.social.utilities;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;

/**
 * Created by ANDROID PC on 8/10/2017.
 */

public class NetworkChecking extends Activity {

    private static final NetworkChecking ourInstance = new NetworkChecking( );

    public static NetworkChecking getInstance() {
        return ourInstance;
    }

    public NetworkChecking() {

    }

    public boolean isNetworkAvailable(Context context) {
        final ConnectivityManager connectivityManager = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE));
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }
}

package com.daivikapp.social.activities;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;

import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import com.daivikapp.social.R;
import com.daivikapp.social.utilities.Restart;
import com.daivikapp.social.utilities.TinyDB;

public class LanguageChoose extends AppCompatActivity {


    Activity ac;
    TinyDB tinyDb;

    CardView english_card, hindi_card;
    RadioButton radio_1, radio_2;
    Button move_ahead;
    TextView language;
    String[] languagee = {"English", "हिंदी"};
    int i = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        tinyDb = new TinyDB(getApplicationContext() );

        Configuration config = getBaseContext().getResources().getConfiguration();
        String lang = tinyDb.getString("LANG");

        if (lang.isEmpty()) {
            lang = "en";
            tinyDb.putString("LANG", "hi");
        }

        Locale locale = new Locale(lang);
        Locale.setDefault(locale);
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());

        setContentView(R.layout.activity_language_choose);


        ac = this;

        english_card = (CardView)findViewById(R.id.english_card);
        hindi_card = (CardView)findViewById(R.id.hindi_card);


        radio_1 = (RadioButton) findViewById(R.id.radio_1);
        radio_2 = (RadioButton) findViewById(R.id.radio_2);

        move_ahead = (Button) findViewById(R.id.move_ahead);
        language = (TextView) findViewById(R.id.language);


        //update current time view after every 1 seconds
        final Handler handler=new Handler();

        final Runnable updateTask=new Runnable() {
            @Override
            public void run() {
                if (i == 0) {
                    i = 1;
                    language.setText(languagee[i]);

                }else{
                    i = 0;
                    language.setText(languagee[i]);
                }
                handler.postDelayed(this,2000);
            }
        };

        handler.postDelayed(updateTask,2000);




     if (lang.equals("en")) {
         radio_1.setChecked(true);
         radio_2.setChecked(false);
     }else {
         radio_2.setChecked(true);
         radio_1.setChecked(false);
     }

        english_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                    radio_1.setChecked(true);
                    radio_2.setChecked(false);


                Configuration config = getBaseContext().getResources().getConfiguration();

                tinyDb.putString("LANG", "en");
                String lang = "en";

                Locale locale = new Locale(lang);
                Locale.setDefault(locale);
                config.locale = locale;
                getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());


                 new Restart().Restart(ac);


            }
        });


        hindi_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio_2.setChecked(true);
                radio_1.setChecked(false);


                Configuration config = getBaseContext().getResources().getConfiguration();

                tinyDb.putString("LANG", "hi");
                String lang = "hi";

                Locale locale = new Locale(lang);
                Locale.setDefault(locale);
                config.locale = locale;
                getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
                new Restart().Restart(ac);
            }
        });


        move_ahead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mainIntent = new Intent( getApplicationContext(), Facebook_Kit_Login.class );
                startActivity( mainIntent );
                finish();
                overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
            }
        });





    }


}

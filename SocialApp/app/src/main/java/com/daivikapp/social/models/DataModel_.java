package com.daivikapp.social.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ANDROID PC on 8/10/2017.
 */

public class DataModel_ {


    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("image")
    @Expose
    private int image;




    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }






    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}

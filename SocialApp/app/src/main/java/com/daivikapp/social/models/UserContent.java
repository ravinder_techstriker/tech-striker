
package com.daivikapp.social.models;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserContent {

    @SerializedName("user")
    @Expose
    private User user;
    @SerializedName("content")
    @Expose
    private List<Content> content = null;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<Content> getContent() {
        return content;
    }

    public void setContent(List<Content> content) {
        this.content = content;
    }

}

package com.daivikapp.social.activities;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

import com.daivikapp.social.R;
import com.daivikapp.social.utilities.NetworkChecking;
import com.daivikapp.social.utilities.ProgDialog;

public class Login extends AppCompatActivity {

    private CallbackManager callbackManager;
    private AccessToken accessToken;

    NetworkChecking networkChecking;
    ProgDialog prog;
    Activity ac;
    private String TAG = "Social App Facebook_Kit_Login Tag";

    CardView login_btn;

//    public static int APP_REQUEST_CODE = 99;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ac = this;






        networkChecking = new NetworkChecking();
        prog            = new ProgDialog();
        callbackManager = CallbackManager.Factory.create();


        login_btn = (CardView)findViewById(R.id.login_btn);


            login_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                facebooklogin();
            }
        });

    }










    void facebooklogin() {


        LoginManager.getInstance().logInWithReadPermissions( this, Arrays.asList( "public_profile", "email" ) );

        LoginManager.getInstance().registerCallback( callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                //   MainApplication.hideProg( ac );
                accessToken = AccessToken.getCurrentAccessToken();
                GraphRequest request = GraphRequest.newMeRequest(
                        accessToken,
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
                                // Insert your code here
                                try {
                                    //System.out.println( "gdcsgsgsgg   " + response.getJSONObject() );

                                    String social_login_type = "facebook";

                                    String image = "https://graph.facebook.com/" + response.getJSONObject().getString( "id" ) + "/picture?type=large";

                                    Log.d( TAG, "onCompleted: "+image+" Email"+ response.getJSONObject().getString( "email" ));
//
                                    //System.out.println( "Social App fb_Id" + response.getJSONObject().getString( "id" ) + "fbname" + response.getJSONObject().getString( "name" ) + "email" + response.getJSONObject().getString( "email" ) );
                                } catch (JSONException e) {
//                                    e.printStackTrace();
//                                    //System.out.println("Social App Exception  "+e);

                                }

                            }
                        } );

                Bundle parameters = new Bundle();
                parameters.putString( "fields", "id,name,email" );
                request.setParameters( parameters );
                request.executeAsync();

            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {

            }
        } );

    }

}

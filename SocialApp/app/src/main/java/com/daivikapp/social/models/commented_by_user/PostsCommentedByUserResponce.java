
package com.daivikapp.social.models.commented_by_user;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PostsCommentedByUserResponce {

    @SerializedName("rc")
    @Expose
    private int rc;
    @SerializedName("contentlist")
    @Expose
    private List<Contentlist> contentlist = null;

    public int getRc() {
        return rc;
    }

    public void setRc(int rc) {
        this.rc = rc;
    }

    public List<Contentlist> getContentlist() {
        return contentlist;
    }

    public void setContentlist(List<Contentlist> contentlist) {
        this.contentlist = contentlist;
    }

}


package com.daivikapp.social.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ContentRequest {

    @SerializedName("lang")
    @Expose
    private String lang;
    @SerializedName("offset")
    @Expose
    private String offset;
    @SerializedName("numresults")
    @Expose
    private String numresults;
    @SerializedName("tags")
    @Expose
    private String tags;
    @SerializedName("user._id")
    @Expose
    private String userId;

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public String getOffset() {
        return offset;
    }

    public void setOffset(String offset) {
        this.offset = offset;
    }

    public String getNumresults() {
        return numresults;
    }

    public void setNumresults(String numresults) {
        this.numresults = numresults;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

}

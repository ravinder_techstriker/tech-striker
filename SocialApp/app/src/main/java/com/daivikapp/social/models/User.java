
package com.daivikapp.social.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class User {

    @SerializedName("numfollowers")
    @Expose
    private int numfollowers;
    @SerializedName("numfollowing")
    @Expose
    private int numfollowing;
    @SerializedName("numcontent")
    @Expose
    private int numcontent;
    @SerializedName("numcomm")
    @Expose
    private int numcomm;
    @SerializedName("numlikes")
    @Expose
    private int numlikes;
    @SerializedName("numshare")
    @Expose
    private int numshare;
    @SerializedName("numviews")
    @Expose
    private int numviews;
    @SerializedName("utype")
    @Expose
    private String utype;
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("mobile")
    @Expose
    private String mobile;
    @SerializedName("lang")
    @Expose
    private String lang;
    @SerializedName("uplace")
    @Expose
    private String uplace;
    @SerializedName("fbdob")
    @Expose
    private String fbdob;
    @SerializedName("fbname")
    @Expose
    private String fbname;
    @SerializedName("fbphone")
    @Expose
    private String fbphone;
    @SerializedName("fbid")
    @Expose
    private String fbid;
    @SerializedName("fbemail")
    @Expose
    private String fbemail;
    @SerializedName("pwd")
    @Expose
    private String pwd;
    @SerializedName("__v")
    @Expose
    private int v;
    @SerializedName("uimage")
    @Expose
    private String uimage;
    @SerializedName("udob")
    @Expose
    private String udob;
    @SerializedName("ugender")
    @Expose
    private String ugender;
    @SerializedName("ustatus")
    @Expose
    private String ustatus;
    @SerializedName("cdate")
    @Expose
    private String cdate;
    @SerializedName("updatetime")
    @Expose
    private String updatetime;

    @SerializedName("follow")
    @Expose
    private boolean follow;


    public boolean isFollow() {
        return follow;
    }

    public void setFollow(boolean follow) {
        this.follow = follow;
    }

    public int getNumfollowers() {
        return numfollowers;
    }

    public void setNumfollowers(int numfollowers) {
        this.numfollowers = numfollowers;
    }

    public int getNumfollowing() {
        return numfollowing;
    }

    public void setNumfollowing(int numfollowing) {
        this.numfollowing = numfollowing;
    }

    public int getNumcontent() {
        return numcontent;
    }

    public void setNumcontent(int numcontent) {
        this.numcontent = numcontent;
    }

    public int getNumcomm() {
        return numcomm;
    }

    public void setNumcomm(int numcomm) {
        this.numcomm = numcomm;
    }

    public int getNumlikes() {
        return numlikes;
    }

    public void setNumlikes(int numlikes) {
        this.numlikes = numlikes;
    }

    public int getNumshare() {
        return numshare;
    }

    public void setNumshare(int numshare) {
        this.numshare = numshare;
    }

    public int getNumviews() {
        return numviews;
    }

    public void setNumviews(int numviews) {
        this.numviews = numviews;
    }

    public String getUtype() {
        return utype;
    }

    public void setUtype(String utype) {
        this.utype = utype;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public String getUplace() {
        return uplace;
    }

    public void setUplace(String uplace) {
        this.uplace = uplace;
    }

    public String getFbdob() {
        return fbdob;
    }

    public void setFbdob(String fbdob) {
        this.fbdob = fbdob;
    }

    public String getFbname() {
        return fbname;
    }

    public void setFbname(String fbname) {
        this.fbname = fbname;
    }

    public String getFbphone() {
        return fbphone;
    }

    public void setFbphone(String fbphone) {
        this.fbphone = fbphone;
    }

    public String getFbid() {
        return fbid;
    }

    public void setFbid(String fbid) {
        this.fbid = fbid;
    }

    public String getFbemail() {
        return fbemail;
    }

    public void setFbemail(String fbemail) {
        this.fbemail = fbemail;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public int getV() {
        return v;
    }

    public void setV(int v) {
        this.v = v;
    }

    public String getUimage() {
        return uimage;
    }

    public void setUimage(String uimage) {
        this.uimage = uimage;
    }

    public String getUdob() {
        return udob;
    }

    public void setUdob(String udob) {
        this.udob = udob;
    }

    public String getUgender() {
        return ugender;
    }

    public void setUgender(String ugender) {
        this.ugender = ugender;
    }

    public String getUstatus() {
        return ustatus;
    }

    public void setUstatus(String ustatus) {
        this.ustatus = ustatus;
    }

    public String getCdate() {
        return cdate;
    }

    public void setCdate(String cdate) {
        this.cdate = cdate;
    }

    public String getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(String updatetime) {
        this.updatetime = updatetime;
    }

}

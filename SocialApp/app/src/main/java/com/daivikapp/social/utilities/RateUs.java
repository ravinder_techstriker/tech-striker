package com.daivikapp.social.utilities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.support.v7.widget.AppCompatButton;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


import com.daivikapp.social.R;

/**
 * Created by ANDROID PC on 12/20/2017.
 */

public class RateUs {
    private static final RateUs ourInstance = new RateUs();
    private static Button buttonConfirm;
     static Context ac;

    static TinyDB tinyDb;
    public static RateUs getInstance() {
        return ourInstance;
    }

    private RateUs() {
    }

    @SuppressLint("ResourceAsColor")
    public static void rate_us(final Activity ac) {

        tinyDb = new TinyDB( ac );


        //Creating a LayoutInflater object for the dialog box
        LayoutInflater li = LayoutInflater.from( ac );
        //Creating a view to get the dialog box
        View confirmDialog = li.inflate( R.layout.dialog_rate_app, null );

        //Initizliaing confirm button fo dialog box and edittext of dialog box
        buttonConfirm = (Button) confirmDialog.findViewById( R.id.button );

        //Creating an alertdialog builder
        AlertDialog.Builder alert = new AlertDialog.Builder( ac );


        //Adding our dialog box to the view of alert dialog
        alert.setView( confirmDialog );

        //Creating an alert dialog
        final AlertDialog alertDialog = alert.create();
        alertDialog.getWindow().setBackgroundDrawable( new ColorDrawable( Color.TRANSPARENT ) );
        //Displaying the alert dialog
        alertDialog.show();


        buttonConfirm.setOnClickListener( new View.OnClickListener() {


            @Override
            public void onClick(View v) {

                try {
                    ac.startActivity(new Intent(Intent.ACTION_VIEW,
                            Uri.parse("https://play.google.com/store/apps/details?id=com.whatsapp")));

//                startActivity(new Intent(Intent.ACTION_VIEW,
//                        Uri.parse("market://details?id=" + ac.getPackageName())));

                } catch (android.content.ActivityNotFoundException e) {
                    ac.startActivity(new Intent(Intent.ACTION_VIEW,
                            Uri.parse("http://play.google.com/store/apps/details?id=" + ac.getPackageName())));

                }
            }

        } );
    }
}

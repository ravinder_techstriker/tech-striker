package com.daivikapp.social.utilities;

import android.content.Context;
import android.graphics.drawable.Animatable;
import android.net.Uri;
import android.util.AttributeSet;
import android.view.View;

import com.facebook.drawee.backends.pipeline.PipelineDraweeControllerBuilder;
import com.facebook.drawee.controller.BaseControllerListener;
import com.facebook.drawee.controller.ControllerListener;
import com.facebook.drawee.generic.GenericDraweeHierarchy;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.image.ImageInfo;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;

import java.util.ArrayList;
import java.util.List;

/**
 * Works when either height or width is set to wrap_content
 * The view is resized based on the image fetched
 */
public class WrapContentDraweeView extends SimpleDraweeView {

    public   Animatable mGif;
    private   View mGifButton;
    public  Boolean mRunning = false;
    public  static List<Animatable> mGifList = new ArrayList<>();


    public   boolean onClick(int pos){

        //System.out.println("dgsggsgs  "+mGifList.size());

        if(mRunning) {
            mGifList.get(pos).stop();
            mGif.stop();
            mRunning = false;
        }
        else {
            mGifList.get(pos).stop();
            mGif.start();
            mRunning = true;
        }
        return mRunning;
    }


    public   boolean onClick(){
        if(mRunning) {
            mGif.stop();
            mRunning = false;
        }
        else {
            mGif.start();
            mRunning = true;
        }
        return mRunning;
    }

    // we set a listener and update the view's aspect ratio depending on the loaded image
   public   ControllerListener mListener = new BaseControllerListener<ImageInfo>() {
        @Override
        public void onIntermediateImageSet(String id, ImageInfo imageInfo) {
            updateViewSize(imageInfo);
        }

        @Override
        public void onFinalImageSet(String id,  ImageInfo imageInfo, Animatable animatable) {
            updateViewSize(imageInfo);
            if(animatable != null) {
                mGif = animatable;
                mGifList.add(mGif);
                if(mGifButton != null) {
                    mGifButton.setVisibility(VISIBLE);
                    setClickable(true);
                }
            }
            else {
                mGif = null;
            }
        }


    };

    public WrapContentDraweeView(Context context, GenericDraweeHierarchy hierarchy) {
        super(context, hierarchy);
    }

    public WrapContentDraweeView(Context context) {
        super(context);
    }

    public WrapContentDraweeView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public WrapContentDraweeView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public WrapContentDraweeView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }


    public void setImageURI(String uri, Object callerContext,View gifImg) {
        setImageURI(uri, callerContext);
        mGifButton = gifImg;
    }

    @Override
    public void setImageURI(Uri uri, Object callerContext) {
        mGifButton = null;
        ImageRequest request = ImageRequestBuilder.newBuilderWithSource(uri)
                .setProgressiveRenderingEnabled(true)
                .build();
        DraweeController controller = ((PipelineDraweeControllerBuilder)getControllerBuilder())
                .setControllerListener(mListener)
                .setCallerContext(callerContext)
//                .setAutoPlayAnimations(true)
                .setImageRequest(request)
                .setOldController(getController())
                .build();
        /**
        DraweeController controller = ((PipelineDraweeControllerBuilder)getControllerBuilder())
                .setControllerListener(listener)
                .setCallerContext(callerContext)
                .setUri(uri)
                .setOldController(getController())
                .build();
         **/
        setController(controller);
    }

//     void attachGifButton(boolean show) {
//        but = new Button(getContext());
//        but.setVisibility(GONE);
//        but.setText("GIF");
//        ((ViewGroup)this.getParent()).addView(but,0);
//
//    }

    public   void updateViewSize(ImageInfo imageInfo) {
        if (imageInfo != null) {
            setAspectRatio((float) imageInfo.getWidth() / imageInfo.getHeight());
        }
    }
}

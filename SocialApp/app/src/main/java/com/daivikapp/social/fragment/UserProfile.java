package com.daivikapp.social.fragment;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.google.gson.JsonElement;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.theartofdev.edmodo.cropper.CropImage;
import com.daivikapp.social.R;
import com.daivikapp.social.activities.MainActivity;
import com.daivikapp.social.utilities.Base64;
import com.daivikapp.social.utilities.FilePath;
import com.daivikapp.social.utilities.MainApplication;
import com.daivikapp.social.utilities.NetworkChecking;
import com.daivikapp.social.utilities.ProgDialog;
import com.daivikapp.social.utilities.SnakeBaar;
import com.daivikapp.social.utilities.TinyDB;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static android.content.Context.MODE_PRIVATE;
import static com.daivikapp.social.activities.MainActivity.form_one_title;
import static com.daivikapp.social.activities.MainActivity.name;
import static com.daivikapp.social.utilities.MainApplication.getInstance;

public class UserProfile extends Fragment implements View.OnClickListener {
    Activity ac;
    public static final String TEXT_PLAIN = "text/plain";

    private EditText edtName, address, bio;
    private SimpleDraweeView profileImage;
    private ImageView editprofilepic;
    private TextView contactNumber, birthday;
    TextView pref_lang;
    private EditText contactEmail;
    RelativeLayout main_lay;
    private TextView contactBirthday;
//    private EditText  bio;
    private Button updateProfile;
    private int mYear, mDay, mMonth;
    Calendar c;
    ProgDialog prog = new ProgDialog();
    String userChoosenTask, responceCode;
    private int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    String profile_img, photo_value;
    Uri profile_Uri;
    private SharedPreferences pref1;
    String user_id;
    TinyDB tinyDb;

    Dialog show1;

    SnakeBaar snakeBaar = new SnakeBaar();
    NetworkChecking networkChecking;

    RadioButton male, female;
    String gender_str;


    int height, width;
    String Selected_lang;

//    ArrayList<LanguageSelect> langList = new ArrayList<>();

 String name_str,  email_str, mob_str, bio_str, place_str, dob_str, fb_ph_str;
    Uri imageURI = null;
    File final_file;
    private File mTmpFile;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate( R.layout.fragment_user_profile, container, false );
        ac = getActivity();

        tinyDb  = new TinyDB(ac);

        // Tracking the screen view
        getInstance().trackScreenView("User Profile", tinyDb.getString("ID"));




        form_one_title.setText(R.string.user_profile);

        DisplayMetrics displayMetrics = new DisplayMetrics();
        ac.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        height = displayMetrics.heightPixels;
        width = displayMetrics.widthPixels;

        findViews(root);

        return root;
    }



    private void findViews(View root) {
        pref1 = ac.getSharedPreferences( "LOGIN_INFO", MODE_PRIVATE );
        user_id = pref1.getString( "USERID", "" );



        networkChecking = new NetworkChecking();
//        prog = new ProgDialog();
        snakeBaar = new SnakeBaar();

        edtName = (EditText)root.findViewById( R.id.edt_name );
        address = (EditText)root.findViewById( R.id.address );
        bio = (EditText)root.findViewById( R.id.bio );

        profileImage = (SimpleDraweeView)root.findViewById( R.id.profile_image );
        editprofilepic = (ImageView)root.findViewById( R.id.editprofilepic );
        contactNumber = (TextView) root.findViewById( R.id.contact_number );
        contactEmail = (EditText)root.findViewById( R.id.contact_email );
        contactBirthday = (TextView) root.findViewById( R.id.contact_birthday );
//        bio = (EditText)root.findViewById( R.id.bio );
        updateProfile = (Button)root.findViewById( R.id.update_profile );

        main_lay = (RelativeLayout) root.findViewById( R.id.main_lay);
        pref_lang = (TextView) root.findViewById( R.id.pref_lang);

        male = (RadioButton) root.findViewById( R.id.male );
        female = (RadioButton) root.findViewById( R.id.female );

//        LanguageSelect languageSelect = new LanguageSelect();
//        languageSelect.setLable(getResources().getString(R.string.english));
//        languageSelect.setImage(R.drawable.english);
//        langList.add(languageSelect);
//
//
//        languageSelect = new LanguageSelect();
//        languageSelect.setLable(getResources().getString(R.string.hindi));
//        languageSelect.setImage(R.drawable.hindi);
//        langList.add(languageSelect);


        pref_lang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                show1 = new Dialog(ac, R.style.AlertDialogCustom);
                show1.setContentView(R.layout.activity_language_choose);



                CardView english_card = (CardView) show1.findViewById(R.id.english_card);
                CardView hindi_card = (CardView) show1.findViewById(R.id.hindi_card);
                Button move_ahead = (Button) show1.findViewById(R.id.move_ahead);
                RadioButton radio_2 = (RadioButton) show1.findViewById(R.id.radio_2);
                RadioButton radio_1 = (RadioButton) show1.findViewById(R.id.radio_1);


                if (tinyDb.getString("LANG").equals("en")) {
                    radio_1.setChecked(true);
                    radio_2.setChecked(false);
                }else {
                    radio_1.setChecked(false);
                    radio_2.setChecked(true);
                }


                english_card.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // Tracking Event
                        MainApplication.getInstance().trackEvent("Profile", "English", "Language selected to English", tinyDb.getString("ID"));
                        Selected_lang = "en";
                            pref_lang.setText(ac.getResources().getString(R.string.english));
                        show1.dismiss();

                    }
                });

                move_ahead.setVisibility(View.GONE);

                hindi_card.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // Tracking Event
                        MainApplication.getInstance().trackEvent("Profile", ac.getResources().getString(R.string.hindi), "Language selected to "+ac.getResources().getString(R.string.hindi), tinyDb.getString("ID"));
                        Selected_lang = "hi";
                            pref_lang.setText(ac.getResources().getString(R.string.hindi));
                        show1.dismiss();

                    }
                });

                show1.show();

            }
        });





        updateProfile.setOnClickListener( this );
        contactBirthday.setOnClickListener( this );
        editprofilepic.setOnClickListener( this );




        getProfile();

    }




    @Override
    public void onClick(View v) {
        if ( v == updateProfile ) {
            // Handle clicks for updateProfile
            name_str = edtName.getText().toString();

            if (!name_str.isEmpty()) {


                if (female.isChecked()) {
                    gender_str = "f";
                } else {
                    gender_str = "m";
                }

                place_str = address.getText().toString();
                bio_str = bio.getText().toString();
                dob_str = contactBirthday.getText().toString();

              email_str = contactEmail.getText().toString();
              mob_str = contactNumber.getText().toString();
              fb_ph_str= contactNumber.getText().toString();

                updateProfileretro();

            }else{
                snakeBaar.showSnackBar( ac, getString(R.string.snake_name_msg), main_lay );
            }

        }else if (v == contactBirthday) {
            // Tracking Event
            MainApplication.getInstance().trackEvent("Profile", "Date of Birth", "Date of birth changed", tinyDb.getString("ID"));
            datepickerForDOB();


        } else if (v == editprofilepic) {

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {

            //Permission check and ask for required permission
            Dexter.withActivity(ac)
                    .withPermissions(
                            Manifest.permission.CAMERA,
                            Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE
                    ).withListener(new MultiplePermissionsListener() {
                @Override
                public void onPermissionsChecked(MultiplePermissionsReport report) {
                    // Tracking Event
                    MainApplication.getInstance().trackEvent("Profile", "Profile pic", "Profile pic changed", tinyDb.getString("ID"));

                    selectImage();

                }

                @Override
                public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                }

            }).check();

            }else{
                selectImage();
            }
        }
    }

    public void datepickerForDOB() {
        c = Calendar.getInstance();
        mYear = c.get( Calendar.YEAR );
        mMonth = c.get( Calendar.MONTH );
        mDay = c.get( Calendar.DAY_OF_MONTH );


        DatePickerDialog datePickerDialog = new DatePickerDialog( ac,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        String startTime = dayOfMonth + "-" + (monthOfYear + 1) + "-" + year;
                        // Get date from string
//                        //System.out.println( "hhhhhhhhhhhhhhhh   " + startTime );


                        try {

                            SimpleDateFormat format = new SimpleDateFormat("dd-mm-yyyy");
                            Date newDate = format.parse(startTime);

                            format = new SimpleDateFormat("yyyy-mm-dd");
                            String date = format.format(newDate);

                            contactBirthday.setText(date);
                        } catch (ParseException e) {
//                            e.printStackTrace();
                        }




                    }
                }, mYear, mMonth, mDay );
        datePickerDialog.show();
    }


//    public String getRealPathFromURI (Uri contentUri) {
//        String path = null;
//        String[] proj = { MediaStore.MediaColumns.DATA };
//        Cursor cursor = ac.getContentResolver().query(contentUri, proj, null, null, null);
//        if (cursor.moveToFirst()) {
//            int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
//            path = cursor.getString(column_index);
//        }
//        cursor.close();
//        return path;
//    }
    public void updateProfileretro() {


        if (networkChecking.isNetworkAvailable( ac )) {
            prog.progDialog( ac );

            // Tracking Event
            MainApplication.getInstance().trackEvent("Profile", "Update", "Profile Updated", tinyDb.getString("ID"));
            if (female.isChecked()){
                gender_str ="f";
                // Tracking Event
                MainApplication.getInstance().trackEvent("Profile", "Gender", "Female", tinyDb.getString("ID"));
            }else{
                // Tracking Event
                MainApplication.getInstance().trackEvent("Profile", "Gender", "Male", tinyDb.getString("ID"));
                gender_str = "m";
            }

            MultipartBody.Part imagenPerfil = null;


            if (final_file != null) {
                // create RequestBody instance from file
//                //System.out.println(final_file+"     kkkkkkkkkkkkkIIIIIIIIIII     ");
                RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), final_file);
                // MultipartBody.Part is used to send also the actual file name
                imagenPerfil = MultipartBody.Part.createFormData("uimage", final_file.getName(), requestFile);
            }


            RequestBody name_part = RequestBody.create(MediaType.parse("multipart/form-data"), name_str);
            RequestBody mob_part = RequestBody.create(MediaType.parse("multipart/form-data"), tinyDb.getString("PHONE"));
            RequestBody pswd_part = RequestBody.create(MediaType.parse("multipart/form-data"), tinyDb.getString("PASSWORD"));
            RequestBody id_part = RequestBody.create(MediaType.parse("multipart/form-data"), tinyDb.getString("ID"));
            RequestBody lang_part = RequestBody.create(MediaType.parse("multipart/form-data"), Selected_lang);
            RequestBody status_part = RequestBody.create(MediaType.parse("multipart/form-data"), bio_str);
            RequestBody place_part = RequestBody.create(MediaType.parse("multipart/form-data"), place_str);
            RequestBody dob_part = RequestBody.create(MediaType.parse("multipart/form-data"), dob_str);
            RequestBody gender_part = RequestBody.create(MediaType.parse("multipart/form-data"), gender_str);
            RequestBody email_part = RequestBody.create(MediaType.parse("multipart/form-data"), email_str);
            RequestBody fbemail_part = RequestBody.create(MediaType.parse("multipart/form-data"), "");
            RequestBody fbdob_part = RequestBody.create(MediaType.parse("multipart/form-data"), "");
            RequestBody fbname_part = RequestBody.create(MediaType.parse("multipart/form-data"), "");

            RequestBody fb_ph_part = null;
            try {
                fb_ph_part = RequestBody.create(MediaType.parse("multipart/form-data"), fb_ph_str);
            } catch (Exception e) {
                fb_ph_part = RequestBody.create(MediaType.parse("multipart/form-data"), "");
            }



            MainApplication.getApiService().updateUser("/api/user/update/"+tinyDb.getString("ID"), imagenPerfil, id_part, pswd_part, mob_part, name_part, lang_part, status_part, place_part, fbdob_part, dob_part, gender_part, fbname_part, fb_ph_part, email_part, fbemail_part).enqueue(new Callback<JsonElement >() {
                @Override
                public void onResponse(Call<JsonElement> call, Response<JsonElement > response) {
                    prog.hideProg();
                    if (response.isSuccessful()) {

                        try {
                            try {
                                JSONObject jsonObj = new JSONObject(String.valueOf(response.body()));
                                responceCode =  jsonObj.getString("rc");

                            }catch (Exception e1){}

                        } catch (Exception e) {
//                            e.printStackTrace();
                            }
                        if (responceCode.equals("1")) {
//                            profileImage.setImageURI(imageURI);
                            name.setText(name_str);

                            tinyDb.putString("NAME", name_str);
                            tinyDb.putString("EMAIL", email_str);
                            tinyDb.putString("ID", tinyDb.getString("ID"));
                            tinyDb.putString("PASSWORD", tinyDb.getString("PASSWORD"));
                            tinyDb.putString("PHONE", mob_str);
                            tinyDb.putString("LANG", Selected_lang);
                            tinyDb.putString("BIO", bio_str);
                            tinyDb.putString("PLACE", place_str);
                            tinyDb.putString("DOB", dob_str);
                            tinyDb.putString("GENDER", gender_str);

                            Configuration config = ac.getBaseContext().getResources().getConfiguration();
                            Locale locale = new Locale(Selected_lang);
                            Locale.setDefault(locale);
                            config.locale = locale;
                            ac.getBaseContext().getResources().updateConfiguration(config, ac.getBaseContext().getResources().getDisplayMetrics());


                            if (imageURI != null)
                                tinyDb.putString("IMAGE", String.valueOf(imageURI));

                            Intent mainIntent = new Intent(ac, MainActivity.class);
                            mainIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(mainIntent);
                            ac.overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);


                        } else {
                            snakeBaar.showSnackBar( ac, getString(R.string.snake_msg_profile), main_lay );

                        }
                    }
                }

                @Override
                public void onFailure(Call<JsonElement > call, Throwable t) {
                    prog.hideProg();
                    t.printStackTrace();
                }
            });



        } else {
            snakeBaar.showSnackBar( ac, getString(R.string.no_internet), main_lay );
        }
    }





    private void selectImage() {
        final CharSequence[] items = {getString(R.string.take_photo), getString(R.string.choose_frm_gallery),
                getString(R.string.cancel)};

        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder( ac );
        builder.setTitle( R.string.add_photo );
        builder.setItems( items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {


                if (items[item].equals(getString(R.string.take_photo))) {
                    userChoosenTask = "Take Photo";

                    cameraIntent();

                } else if (items[item].equals(getString(R.string.choose_frm_gallery))) {
                    userChoosenTask = "Choose from Library";
                    galleryIntent();

                } else if (items[item].equals(getString(R.string.cancel))) {
                    dialog.dismiss();
                }
            }
        } );
        builder.show();
    }

    private void galleryIntent() {
        try {
            Intent intent = new Intent( Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI );
            intent.setAction(Intent.ACTION_GET_CONTENT);
            intent.setType( "image/*" );
            startActivityForResult( Intent.createChooser( intent, getString(R.string.choose_pic) ), SELECT_FILE );
        } catch (Exception e) {
            // TODO: handle exception
        }
    }

    private File getTempFile(Context context){
        //it will return /sdcard/image.tmp
        final File path = new File( Environment.getExternalStorageDirectory(), context.getPackageName() );
        if(!path.exists()){
            path.mkdir();
        }
        return new File(path, "image.tmp");
    }

    private void cameraIntent() {
        Intent intent = new Intent( MediaStore.ACTION_IMAGE_CAPTURE );
        mTmpFile = getTempFile(ac.getApplicationContext());
        Uri uri = Uri.fromFile(mTmpFile);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, uri );
        startActivityForResult( intent, REQUEST_CAMERA );
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult( requestCode, resultCode, data );



            if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);

            cropedImage(result);
        }else  {
            try {

                if(data != null) {
                    Uri selectedImageUri = data.getData();
                    String realPath = FilePath.getPath(ac, selectedImageUri);

                    imageURI = Uri.fromFile(new File(realPath));
                    final_file = new File(realPath);
                }
                else {
                    imageURI = Uri.fromFile(mTmpFile);
                    final_file = mTmpFile;
                }

                CropImage.activity(imageURI).start(ac, this);
            } catch (Exception e) {}

        }
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {


        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress( Bitmap.CompressFormat.JPEG, 100, bytes );
        String path = MediaStore.Images.Media.insertImage( inContext.getContentResolver(), inImage, "Title", null );
        return Uri.parse( path );
    }


    @SuppressWarnings("deprecation")
    private void cropedImage(CropImage.ActivityResult data) {

        Bitmap bm = null;
        Uri tempUri = null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap( ac.getContentResolver(), data.getUri() );
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                bm.compress( Bitmap.CompressFormat.JPEG, 90, bytes );
                byte[] b = bytes.toByteArray();
                photo_value = Base64.encodeBytes( b );
                tempUri = getImageUri( ac, bm );
            } catch (Exception e) {
//                e.printStackTrace();
//                //System.out.println("exceptionnnnnnnnnnnn   "+e);
            }
        }

            profile_img = photo_value;
            profileImage.setImageURI( tempUri );
            profile_Uri = tempUri;
        String  realPath = FilePath.getPath(ac, profile_Uri);
        imageURI = Uri.fromFile(new File(realPath));
        final_file = new File(realPath);

    }


    private void getProfile() {
        edtName.setText(tinyDb.getString("NAME"));
        contactNumber.setText(tinyDb.getString("PHONE"));
        contactEmail.setText(tinyDb.getString("EMAIL"));
        address.setText(tinyDb.getString("PLACE"));
        contactBirthday.setText(tinyDb.getString("DOB"));
        bio.setText(tinyDb.getString("BIO"));
        profileImage.setImageURI(tinyDb.getString("IMAGE"));

        if (tinyDb.getString("GENDER").equals("m")){
            male.setChecked(true);
            female.setChecked(false);
        }else{
            female.setChecked(true);
            male.setChecked(false);
        }

        if (tinyDb.getString("LANG").equals("en")) {
            pref_lang.setText(ac.getResources().getString(R.string.english));
            Selected_lang = "en";
        }else {
            pref_lang.setText(ac.getResources().getString(R.string.hindi));
            Selected_lang = "hi";
        }


    }


    @Override
    public void onDetach() {
        if(prog != null)
            prog.destroy();
        super.onDetach();
    }

}

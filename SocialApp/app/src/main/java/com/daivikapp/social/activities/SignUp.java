package com.daivikapp.social.activities;


import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.daivikapp.social.R;
import com.daivikapp.social.models.AddUserErrorResponce;
import com.daivikapp.social.models.AddUserResponce;
import com.daivikapp.social.utilities.MainApplication;
import com.daivikapp.social.utilities.NetworkChecking;
import com.daivikapp.social.utilities.ProgDialog;
import com.daivikapp.social.utilities.SnakeBaar;
import com.daivikapp.social.utilities.TinyDB;
import com.daivikapp.social.utilities.TrackInstall;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.daivikapp.social.utilities.MainApplication.getInstance;

//import com.hbb20.CountryCodePicker;

public class SignUp extends AppCompatActivity implements View.OnClickListener, LocationListener {

    Activity ac;
    SnakeBaar snakeBaar;
    ProgDialog prog;
    NetworkChecking networkChecking;

    private Button btnsubmitcontact;
    private RelativeLayout rr;
    AlertDialog.Builder alertDialogBuilder;
    private EditText name_edt;
    private String mob_str, name_str;
    String fb_id_str = "", fb_ph_str="", fb_em_str = "", place_str;


    SharedPreferences pref;
    SharedPreferences.Editor editor;
    private String country_code = "+91";


    TinyDB tinyDb;
    LocationManager mLocationManager;
    String responceCode;
    String error = "Error Check detail";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_signup);

        // Tracking the screen view
        getInstance().trackScreenView("SignUp", "");

        ac = this;
        tinyDb = new TinyDB(ac);


        fb_id_str = getIntent().getStringExtra("FB_ID");
        fb_ph_str = getIntent().getStringExtra("PHONE");
        fb_em_str = getIntent().getStringExtra("EMAIL");
        //System.out.println("rinku numberrrr    "+fb_ph_str);



        networkChecking = new NetworkChecking();
        prog = new ProgDialog();
        snakeBaar = new SnakeBaar();
        alertDialogBuilder = new AlertDialog.Builder(this);
        pref = getSharedPreferences("TechStriker_USER_INFO", MODE_PRIVATE);
        editor = pref.edit();


        findViews();


        //Permission check and ask for required permission
        Dexter.withActivity(ac)
                .withPermissions(
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.ACCESS_FINE_LOCATION
                ).withListener(new MultiplePermissionsListener() {
            @Override
            public void onPermissionsChecked(MultiplePermissionsReport report) {

                mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

                @SuppressLint("MissingPermission")
                Location location = mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

                if(location != null) {
                    // Do something with the recent location fix
                    //  otherwise wait for the update below
                    place_str =  getAddressFromLatLng(ac, location);
                    //System.out.println("Placeeeeeeeee   "+place_str);
                }
                else {

//                    mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
                }



            }

            @Override
            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
            }

        }).check();

    }
    private void findViews() {

        rr = (RelativeLayout) findViewById( R.id.contactushomedrawer_layout );
        name_edt = (EditText) findViewById( R.id.name_edt );

        btnsubmitcontact = (Button) findViewById( R.id.btnsubmitcontact );
        btnsubmitcontact.setOnClickListener( this );

    }
    @Override
    public void onClick(View v) {

        if (v == btnsubmitcontact) {

            name_str = name_edt.getText().toString().trim();


            if (name_str.equals( "" )) {
                snakeBaar.showSnackBar( ac, "Name can not be empty", rr );
            }

            else {

                editor.putString( "name", name_str );
                editor.apply();
                profileDataToServer();

            }
        }
    }

    private void profileDataToServer() {


        if (networkChecking.isNetworkAvailable( ac )) {
            prog.progDialog( ac );

            MultipartBody.Part imagenPerfil = null;


            RequestBody name_part = RequestBody.create(MediaType.parse("multipart/form-data"), name_str);
            RequestBody lang_part = RequestBody.create(MediaType.parse("multipart/form-data"), tinyDb.getString("LANG"));
            RequestBody status_part = RequestBody.create(MediaType.parse("multipart/form-data"), "");
            RequestBody place_part = RequestBody.create(MediaType.parse("multipart/form-data"), place_str);
            RequestBody dob_part = RequestBody.create(MediaType.parse("multipart/form-data"), "");
            RequestBody gender_part = RequestBody.create(MediaType.parse("multipart/form-data"), "m");
            RequestBody email_part = RequestBody.create(MediaType.parse("multipart/form-data"), "");
            RequestBody gcm_reg_id = RequestBody.create(MediaType.parse("multipart/form-data"), tinyDb.getString("gcmregid"));

            Log.d("Signup", "GCM Registration ID sent to server : " + tinyDb.getString("gcmregid"));

            RequestBody fb_ph_part = null;
            RequestBody mob_part;
            try {
                fb_ph_part = RequestBody.create(MediaType.parse("multipart/form-data"), fb_ph_str);
                mob_part = RequestBody.create(MediaType.parse("multipart/form-data"), fb_ph_str);
            } catch (Exception e) {
                fb_ph_part = RequestBody.create(MediaType.parse("multipart/form-data"), "");
                mob_part = RequestBody.create(MediaType.parse("multipart/form-data"), "");
            }

//            RequestBody fb_email_part = RequestBody.create(MediaType.parse("multipart/form-data"), fb_em_str);

            RequestBody fbid_part = null;
            try {
                fbid_part = RequestBody.create(MediaType.parse("multipart/form-data"), fb_id_str);
            } catch (Exception e) {
                fbid_part = RequestBody.create(MediaType.parse("multipart/form-data"), "");
            }


            MainApplication.getApiService().addUser(
                    imagenPerfil,
                    name_part,
                    mob_part,
                    lang_part,
                    status_part,
                    place_part,
                    dob_part,
                    gender_part,
                    name_part,
                    fb_ph_part,
                    fbid_part,
                    email_part,
                    gcm_reg_id).enqueue(new Callback<JsonElement >() {
                @Override
                public void onResponse(Call<JsonElement> call, Response<JsonElement > response) {
                    prog.hideProg();
                    if (response.isSuccessful()) {


                        Gson gson = new Gson();
                        AddUserResponce user = null;
                        AddUserErrorResponce userError = null;
                        try {


                            try {
                                JSONObject jsonObj = new JSONObject(String.valueOf(response.body()));
                               responceCode =  jsonObj.getString("rc");

                                try {
                                    error =  jsonObj.getString("error");
                                } catch (JSONException e) { }

                            }catch (Exception e1){}

                             user= gson.fromJson(String.valueOf(response.body()), AddUserResponce.class);

                            //System.out.println("responceeeeeeeeeeeeee   "+user.getUser().getName());
                        } catch (Exception e) {}



                        if (responceCode.equals("1")) {


                            tinyDb.putString("NAME", name_str);
                            tinyDb.putString("EMAIL", user.getUser().getFbemail());
                            tinyDb.putString("ID", user.getUser().getId());
                            tinyDb.putString("PHONE", fb_ph_str);
                            tinyDb.putString("LANG", user.getUser().getLang());
                            tinyDb.putString("BIO", user.getUser().getUstatus());
                            tinyDb.putString("PLACE", place_str);
                            tinyDb.putString("DOB", user.getUser().getFbdob());
                            tinyDb.putString("GENDER", user.getUser().getUgender());
                            tinyDb.putString("PASSWORD", String.valueOf(user.getUser().getPwd()));

                            snakeBaar.showSnackBar( ac, "Registered Successfully!", rr );

                            tinyDb.putBoolean("isLogin", true);

                            //Login
                            TrackInstall.handleLogin(tinyDb);

                            Intent intent = new Intent( getApplicationContext(), MainActivity.class );
                            intent.addFlags( Intent.FLAG_ACTIVITY_CLEAR_TOP );
                            startActivity( intent );
                            finish();
                            overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);


                        } else {
                            snakeBaar.showSnackBar( ac, error, rr );

                        }
                    }
                }

                @Override
                public void onFailure(Call<JsonElement > call, Throwable t) {
                    prog.hideProg();
                    t.printStackTrace();
                }
            });



        } else {
            snakeBaar.showSnackBar( ac, getString(R.string.no_internet), rr );
        }
    }






    @Override
    public void onBackPressed() {

        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);


    }











    public static String getAddressFromLatLng(Context context, Location latLng) {
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(context, Locale.getDefault());
        try {
            addresses = geocoder.getFromLocation(latLng.getLatitude(), latLng.getLongitude(), 1);
            return addresses.get(0).getAddressLine(0);
        } catch (Exception e) {
//            e.printStackTrace();
            return "";
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        if (location != null) {
            Log.v("Location Changed", location.getLatitude() + " and " + location.getLongitude());
            mLocationManager.removeUpdates(this);
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {}

    @Override
    public void onProviderEnabled(String provider) { }

    @Override
    public void onProviderDisabled(String provider) {}

    @Override
    public void onDestroy() {
        if(prog != null)
            prog.destroy();
        super.onDestroy();
    }

}

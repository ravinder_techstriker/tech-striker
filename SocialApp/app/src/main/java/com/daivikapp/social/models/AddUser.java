
package com.daivikapp.social.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AddUser {

    @SerializedName("mobile")
    @Expose
    private String mobile;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("lang")
    @Expose
    private String lang;
    @SerializedName("ustaus")
    @Expose
    private String ustaus;
    @SerializedName("uplace")
    @Expose
    private String uplace;
    @SerializedName("fbdob")
    @Expose
    private String fbdob;
    @SerializedName("fbgener")
    @Expose
    private String fbgener;
    @SerializedName("fbname")
    @Expose
    private String fbname;
    @SerializedName("fbphone")
    @Expose
    private String fbphone;
    @SerializedName("fbemail")
    @Expose
    private String fbemail;

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public String getUstaus() {
        return ustaus;
    }

    public void setUstaus(String ustaus) {
        this.ustaus = ustaus;
    }

    public String getUplace() {
        return uplace;
    }

    public void setUplace(String uplace) {
        this.uplace = uplace;
    }

    public String getFbdob() {
        return fbdob;
    }

    public void setFbdob(String fbdob) {
        this.fbdob = fbdob;
    }

    public String getFbgener() {
        return fbgener;
    }

    public void setFbgener(String fbgener) {
        this.fbgener = fbgener;
    }

    public String getFbname() {
        return fbname;
    }

    public void setFbname(String fbname) {
        this.fbname = fbname;
    }

    public String getFbphone() {
        return fbphone;
    }

    public void setFbphone(String fbphone) {
        this.fbphone = fbphone;
    }

    public String getFbemail() {
        return fbemail;
    }

    public void setFbemail(String fbemail) {
        this.fbemail = fbemail;
    }

}

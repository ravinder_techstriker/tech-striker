package com.daivikapp.social.fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.Fragment;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.database.Cursor;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.borjabravo.readmoretextview.ReadMoreTextView;
import com.bumptech.glide.Glide;
import com.davidecirillo.multichoicerecyclerview.MultiChoiceAdapter;
import com.erikagtierrez.multiple_media_picker.Gallery;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.backends.pipeline.PipelineDraweeController;
import com.facebook.drawee.generic.GenericDraweeHierarchy;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.common.ResizeOptions;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.getbase.floatingactionbutton.FloatingActionButton;
import com.getbase.floatingactionbutton.FloatingActionsMenu;
import com.github.lzyzsd.circleprogress.CircleProgress;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.krishna.fileloader.FileLoader;
import com.krishna.fileloader.listener.MultiFileDownloadListener;
import com.krishna.fileloader.request.MultiFileLoadRequest;
import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;
import com.pierfrancescosoffritti.androidyoutubeplayer.player.YouTubePlayer;
import com.pierfrancescosoffritti.androidyoutubeplayer.player.YouTubePlayerView;
import com.pierfrancescosoffritti.androidyoutubeplayer.player.listeners.AbstractYouTubePlayerListener;
import com.pierfrancescosoffritti.androidyoutubeplayer.player.listeners.YouTubePlayerInitListener;
import com.plattysoft.leonids.ParticleSystem;
import com.rygelouv.audiosensei.player.AudioSenseiPlayerView;
import com.daivikapp.social.R;
import com.daivikapp.social.activities.ContentDetail;
import com.daivikapp.social.activities.MainActivity;
import com.daivikapp.social.activities.SelectedImages;
import com.daivikapp.social.activities.UserDetailAc;
import com.daivikapp.social.models.Comment;
import com.daivikapp.social.models.ContentResponce;
import com.daivikapp.social.models.Contentlist;
import com.daivikapp.social.models.GetChanelsResponse;
import com.daivikapp.social.models.GetCommentResponce;
import com.daivikapp.social.models.Like;
import com.daivikapp.social.models.Option;
import com.daivikapp.social.utilities.EndlessRecyclerViewScrollListener;
import com.daivikapp.social.utilities.FilePath;
import com.daivikapp.social.utilities.MainApplication;
import com.daivikapp.social.utilities.NetworkChecking;
import com.daivikapp.social.utilities.ProgDialog;
import com.daivikapp.social.utilities.RateUs;
import com.daivikapp.social.utilities.SnakeBaar;
import com.daivikapp.social.utilities.TinyDB;
import com.daivikapp.social.utilities.WrapContentDraweeView;

import org.json.JSONObject;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import cn.jzvd.JZVideoPlayer;
import cn.jzvd.JZVideoPlayerStandard;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static android.view.View.GONE;
import static com.daivikapp.social.activities.MainActivity.form_one_title;
import static com.daivikapp.social.activities.MainActivity.lifecycle;
import static com.daivikapp.social.utilities.MainApplication.getInstance;


public class AllFeeds extends Fragment implements View.OnClickListener {
    Activity ac;

    FloatingActionButton action_video, action_audio, action_image, action_text;
    FloatingActionsMenu multiple_actions;


    RelativeLayout main_lay;


    RelativeLayout donut_progress_layout;
    CircleProgress donut_progress;
    RecyclerView recycle_comment;


    GetChanelsResponse getChanelsResponse = null;

    RecyclerView recycle_post, recycle_filter;


    FilterAdapter filterAdapter;


    All_Post_Adapter all_post_adapter;
    All_Videos_Adapter all_videos_adapter;


    LinearLayoutManager layoutManager, layoutManager1;

    List<Contentlist> all_postListt = new ArrayList();
    ArrayList<Boolean> checkList = new ArrayList();


    public static Contentlist contentlistMain;

    CommentAdapter commentAdapter;
    LikeAdapter likeAdapter;


    List<Comment> all_commentList = new ArrayList();
    List<Option> all_likesList = new ArrayList();
    TypedArray icons;


    TinyDB tinyDb;

    private File mTmpFile;


    BottomSheetBehavior sheetBehavior;
    CardView layoutBottomSheet;
    Button send;
    ImageView cancel;
    SimpleDraweeView user_image;
    EditText comment_edt;
    Dialog show1;
    View likeView;
    //    GifView centerViewBottom, centerViewTop;
    static final int OPEN_MEDIA_PICKER = 65;  // Request code


    NetworkChecking networkChecking;
    ProgDialog prog = new ProgDialog();

    SnakeBaar snakeBaar = new SnakeBaar();


    String contentId;
    String responceCode;
    int clicked_post_position;

    int numResult = 5;

    ArrayList<String> pathsList = new ArrayList<>();
    boolean checkloading = false;

    //    SwipeRefreshLayout  swipeContainer;
    SwipyRefreshLayout swipyrefreshlayout;
    int last_postion;

    // Store a member variable for the listener
    private EndlessRecyclerViewScrollListener scrollListener;
    private int SELECT_AUDIO = 45;

//    EmojiRainLayout mContainer;

    int channel_pos = 0;
    String selected_tag = "";

    //    public  static int gif_check = 0;
    int appOpen = 0;
    boolean youtube_check = false;

    private int REQUEST_CAMERA_IMAGE = 0;
    private int REQUEST_CAMERA_VIDEO = 1;
    // Save state
    Parcelable recyclerViewState;

    RecyclerView.RecycledViewPool mLikesPool;
    RecyclerView.RecycledViewPool mTagsPool;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fregment_allfeeds, container, false);
        ac = getActivity();

        mLikesPool = new RecyclerView.RecycledViewPool();
        mTagsPool = new RecyclerView.RecycledViewPool();

        tinyDb = new TinyDB(ac);

        // Tracking the screen view
        getInstance().trackScreenView("AllFeeds", tinyDb.getString("ID"));


        if (tinyDb.getString("TYPE").equals("VID")) {
            form_one_title.setText(R.string.videos);
        } else {
            form_one_title.setText(R.string.popular_posts);
        }


        try {
            appOpen = tinyDb.getInt("APP_OPEN");
        } catch (Exception e) {
            e.printStackTrace();
            appOpen = 0;
        }

//        //System.out.println("appOpen   " + appOpen);
        if (appOpen > 0) {

//            //System.out.println("appOpen   " + (appOpen % 5));
            if (appOpen % 5 == 0) {
                RateUs.rate_us(ac);

            }
        }
        appOpen = appOpen + 1;

        tinyDb.putInt("APP_OPEN", appOpen);


//        //System.out.println(tinyDb.getString("ID") + " Ures id password    " + tinyDb.getString("PASSWORD"));


        networkChecking = new NetworkChecking();
        prog = new ProgDialog();
        snakeBaar = new SnakeBaar();


        all_likesList = new ArrayList<>();


        List<String> options = Arrays.asList(getResources().getStringArray(R.array.reaction_name_list));

        Resources r = ac.getResources();
        icons = r.obtainTypedArray(R.array.reaction_images_list);


        for (int i = 0; i < options.size(); i++) {
            Option option = new Option();

            option.setName(options.get(i));
            all_likesList.add(option);


        }


        layoutBottomSheet = (CardView) root.findViewById(R.id.bottom_sheet);
        recycle_comment = (RecyclerView) root.findViewById(R.id.recycle_comment);
//        recycle_comment.addItemDecoration(new DividerItemDecoration(recycle_comment.getContext(), DividerItemDecoration.VERTICAL));
        recycle_filter = (RecyclerView) root.findViewById(R.id.recycle_filter);
        if (tinyDb.getString("TYPE").equals("VID")) {
            recycle_filter.setVisibility(GONE);
        } else {
            recycle_filter.setVisibility(View.VISIBLE);
        }


        Gson gson = new Gson();
        getChanelsResponse = gson.fromJson(tinyDb.getString("Channels"), GetChanelsResponse.class);
        try {
            for (int i = 0; i < getChanelsResponse.getChannels().size(); i++) {

                checkList.add(false);
            }


            checkList.set(0, true);
        } catch (Exception e) {
//            e.printStackTrace();
        }

        LinearLayoutManager layoutManager1 = new LinearLayoutManager(ac, LinearLayoutManager.HORIZONTAL, false);
        recycle_filter.setLayoutManager(layoutManager1);


        filterAdapter = new FilterAdapter(ac);
        recycle_filter.setAdapter(filterAdapter);


        comment_edt = (EditText) root.findViewById(R.id.comment_edt);
        send = (Button) root.findViewById(R.id.send);
        cancel = (ImageView) root.findViewById(R.id.cancel);
        user_image = (SimpleDraweeView) root.findViewById(R.id.user_image);


        user_image.setImageURI(Uri.parse(tinyDb.getString("IMAGE")));


//        centerViewBottom = (GifView) root.findViewById(R.id.centerViewBottom);
//        centerViewTop = (GifView) root.findViewById(R.id.centerViewTop);

        sheetBehavior = BottomSheetBehavior.from(layoutBottomSheet);

        sheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                switch (newState) {
                    case BottomSheetBehavior.STATE_HIDDEN:
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED: {
//                        btnBottomSheet.setText("Close Sheet");
                    }
                    break;
                    case BottomSheetBehavior.STATE_COLLAPSED: {
//                        btnBottomSheet.setText("Expand Sheet");
                    }
                    break;
                    case BottomSheetBehavior.STATE_DRAGGING:
                        break;
                    case BottomSheetBehavior.STATE_SETTLING:
                        break;
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });


        multiple_actions = (FloatingActionsMenu) root.findViewById(R.id.multiple_actions);

        main_lay = (RelativeLayout) root.findViewById(R.id.main_lay);


        donut_progress_layout = (RelativeLayout) root.findViewById(R.id.donut_progress_layout);
        donut_progress = (CircleProgress) root.findViewById(R.id.donut_progress);


        action_video = (FloatingActionButton) root.findViewById(R.id.action_video);
        action_audio = (FloatingActionButton) root.findViewById(R.id.action_audio);
        action_image = (FloatingActionButton) root.findViewById(R.id.action_image);
        action_text = (FloatingActionButton) root.findViewById(R.id.action_text);

        recycle_post = (RecyclerView) root.findViewById(R.id.recycle_post);
        recycle_post.setItemViewCacheSize(4);

        layoutManager = new LinearLayoutManager(ac, LinearLayoutManager.VERTICAL, false);


        recycle_post.setHasFixedSize(true);
        recycle_post.setItemViewCacheSize(1);
        recycle_post.setDrawingCacheEnabled(true);

        recycle_post.setLayoutManager(layoutManager);

        // Retain an instance so that you can call `resetState()` for fresh searches

        try {
            scrollListener = new EndlessRecyclerViewScrollListener(layoutManager) {

                @Override
                public void onScrolled(RecyclerView view, int dx, int dy) {
                    super.onScrolled(view, dx, dy);

                }

                @Override
                public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                    // Triggered only when new data needs to be appended to the list
                    // Add whatever code is needed to append new items to the bottom of the list
//                    //System.out.println(page + "   recycleview scrolleddddddd   " + totalItemsCount);

                    recyclerViewState = recycle_post.getLayoutManager().onSaveInstanceState();

                    last_postion = layoutManager.findLastCompletelyVisibleItemPosition();

                    ArrayList<RequestBody> chanalListPart = new ArrayList<>();
                    ArrayList<RequestBody> tagsListPart = new ArrayList<>();
                    ArrayList<RequestBody> typeListPart = new ArrayList<>();

                    if (!checkloading) {
                        checkloading = true;
//                        //System.out.println(page + "   andar ayaaaaaaaaaa     " + totalItemsCount);
                        RequestBody user_id_part = RequestBody.create(MediaType.parse("multipart/form-data"), tinyDb.getString("ID"));
                        RequestBody lang_part = RequestBody.create(MediaType.parse("multipart/form-data"), tinyDb.getString("LANG"));
                        RequestBody num_res_part = RequestBody.create(MediaType.parse("multipart/form-data"), String.valueOf(numResult));
                        RequestBody offset_part = RequestBody.create(MediaType.parse("multipart/form-data"), String.valueOf(all_postListt.size()));

                        RequestBody tag_part = RequestBody.create(MediaType.parse("multipart/form-data"), selected_tag);
                        RequestBody chanal_part = RequestBody.create(MediaType.parse("multipart/form-data"), getChanelsResponse.getChannels().get(channel_pos).getN());
                        chanalListPart.add(chanal_part);
                        tagsListPart.add(tag_part);
                        if (tinyDb.getString("TYPE").equals("VID")) {

//                            for (int i = 0; i < 2; i++) {
                            RequestBody type = RequestBody.create(MediaType.parse("multipart/form-data"), "vid");
                            typeListPart.add(type);
                            RequestBody type1 = RequestBody.create(MediaType.parse("multipart/form-data"), "uvid");
                            typeListPart.add(type1);
//                            }
                        }

//                        //System.out.println("chenalssss    " + chanalListPart);

                        if (networkChecking.isNetworkAvailable(ac)) {

                            MainApplication.getApiService().getContent("content/get/" + tinyDb.getString("ID"), typeListPart, chanalListPart, lang_part, offset_part, num_res_part, tagsListPart, user_id_part).enqueue(new Callback<ContentResponce>() {
                                @Override
                                public void onResponse(Call<ContentResponce> call, Response<ContentResponce> response) {

                                    if (response.isSuccessful()) {


                                        if (response.body().getRc() == 1) {
                                            checkloading = false;
                                            if (response.body().getContentlist().size() != 0) {
                                                for (int i = 0; i < response.body().getContentlist().size(); i++) {

                                                    all_postListt.add(response.body().getContentlist().get(i));
                                                }

                                                // Restore state
                                                recycle_post.getLayoutManager().onRestoreInstanceState(recyclerViewState);

                                                try {
//                                                    all_post_adapter.notifyItemRangeChanged(last_postion, all_post_adapter.getItemCount());
                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }

                                                scrollListener.resetState();


                                                swipyrefreshlayout.setRefreshing(false);

                                            } else {
                                                snakeBaar.showSnackBar(ac, getString(R.string.no_more_result), main_lay);
                                            }
                                        } else {
                                            snakeBaar.showSnackBar(ac, getString(R.string.no_content_found), main_lay);


                                        }
                                    }
                                }

                                @Override
                                public void onFailure(Call<ContentResponce> call, Throwable t) {
                                    t.printStackTrace();
                                }
                            });


                        } else {
                            snakeBaar.showSnackBar(ac, getString(R.string.no_internet), main_lay);
                        }

                    }


                }
            };
        } catch (Exception e) {
        }


        // Adds the scroll listener to RecyclerView
        recycle_post.addOnScrollListener(scrollListener);


        swipyrefreshlayout = (SwipyRefreshLayout) root.findViewById(R.id.swipyrefreshlayout);
        swipyrefreshlayout.setOnRefreshListener(new SwipyRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh(SwipyRefreshLayoutDirection direction) {
                Log.d("MainActivity", "Refresh triggered at "
                        + (direction == SwipyRefreshLayoutDirection.TOP ? "top" : "bottom"));

                if (direction == SwipyRefreshLayoutDirection.TOP) {

                    if (networkChecking.isNetworkAvailable(ac)) {

                        getContents(selected_tag, getChanelsResponse.getChannels().get(channel_pos).getN(), true, numResult, 0);
                    } else {
                        swipyrefreshlayout.setRefreshing(false);
                        snakeBaar.showSnackBar(ac, getString(R.string.no_internet), main_lay);
                    }
                } else if (direction == SwipyRefreshLayoutDirection.BOTTOM) {
                    swipyrefreshlayout.setRefreshing(false);


                }
            }
        });


        recycle_post.addOnScrollListener(new RecyclerView.OnScrollListener() {


            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                // Tracking Event
                youtube_check = false;

                JZVideoPlayer.goOnPlayOnPause();


            }
        });


        LinearLayoutManager layoutManager = new LinearLayoutManager(ac, LinearLayoutManager.VERTICAL, false);

        commentAdapter = new CommentAdapter(ac);
        recycle_comment.setLayoutManager(layoutManager);


        send.setOnClickListener(this);
        cancel.setOnClickListener(this);
        action_text.setOnClickListener(this);
        action_audio.setOnClickListener(this);
        action_video.setOnClickListener(this);
        action_image.setOnClickListener(this);


        if (tinyDb.getBoolean("Tag_Selected")) {
            tinyDb.putBoolean("Tag_Selected", false);
            selected_tag = tinyDb.getString("Selected_Tag");

            form_one_title.setText(selected_tag);


            for (int i = 0; i < checkList.size(); i++) {
                checkList.set(i, false);

            }
            filterAdapter.notifyDataSetChanged();

            getContents(selected_tag, "", false, numResult, 0);
        } else {

            try {
                getContents("", getChanelsResponse.getChannels().get(channel_pos).getN(), false, numResult, 0);
            } catch (Exception e) {
//            e.printStackTrace();
                getContents("", "", false, numResult, 0);
            }
        }

        return root;
    }


    @Override
    public void onDetach() {
        JZVideoPlayer.releaseAllVideos();

        for (int i = 0; i < all_postListt.size(); i++) {
            if (all_postListt.get(i).getType().equals("aud")) {
                try {
                    AudioSenseiPlayerView player = layoutManager.findViewByPosition(i).findViewById(R.id.audio_player);
                    player.stop();
                } catch (Exception e) {
                }
            }
        }

        if (prog != null)
            prog.destroy();

        super.onDetach();


    }


    @Override
    public void onClick(View view) {


        if (view == cancel) {
            if (sheetBehavior.getState() != BottomSheetBehavior.STATE_EXPANDED) {
                sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            } else {
                sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            }
        } else if (view == action_video) {
            // Tracking Event
            MainApplication.getInstance().trackEvent("Post", "Add Video", "Add Video button clicked", tinyDb.getString("ID"));

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {

                //Permission check and ask for required permission
                Dexter.withActivity(ac)
                        .withPermissions(
                                Manifest.permission.CAMERA,
                                Manifest.permission.READ_EXTERNAL_STORAGE,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE
                        ).withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {

                        multiple_actions.collapse();

                        selectVideo();


                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                    }

                }).check();

            } else {
                multiple_actions.collapse();

                selectVideo();
            }


        } else if (view == action_audio) {

            // Tracking Event
            MainApplication.getInstance().trackEvent("Post", "Add Audio", "Add Audio button clicked", tinyDb.getString("ID"));
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {

                //Permission check and ask for required permission
                Dexter.withActivity(ac)
                        .withPermissions(
                                Manifest.permission.CAMERA,
                                Manifest.permission.READ_EXTERNAL_STORAGE,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE
                        ).withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {

                        multiple_actions.collapse();
                        tinyDb.putString("MideaType", "Audio");
//                        Intent intent = new Intent();
//                        intent.setType( "audio/*" );
////                        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
//                        intent.setAction( Intent.ACTION_GET_CONTENT );
//                        startActivityForResult( Intent.createChooser( intent, "Select Audio " ), SELECT_AUDIO );

                        Intent intent_upload = new Intent();
                        intent_upload.setType("audio/*");
                        intent_upload.setAction(Intent.ACTION_GET_CONTENT);
                        startActivityForResult(intent_upload, SELECT_AUDIO);

                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                    }

                }).check();

            } else {
                multiple_actions.collapse();
                tinyDb.putString("MideaType", "Audio");
//                Intent intent = new Intent();
//                intent.setType( "audio/*" );
////                intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
//                intent.setAction( Intent.ACTION_GET_CONTENT );
//                startActivityForResult( Intent.createChooser( intent, "Select Audio " ), SELECT_AUDIO );

                Intent intent_upload = new Intent();
                intent_upload.setType("audio/*");
                intent_upload.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(intent_upload, SELECT_AUDIO);
            }


        } else if (view == action_image) {

            // Tracking Event
            MainApplication.getInstance().trackEvent("Post", "Add Images", "Add Images button clicked", tinyDb.getString("ID"));


            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {

                //Permission check and ask for required permission
                Dexter.withActivity(ac)
                        .withPermissions(
                                Manifest.permission.CAMERA,
                                Manifest.permission.READ_EXTERNAL_STORAGE,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE
                        ).withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {

                        multiple_actions.collapse();


                        selectImage();

                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                    }

                }).check();

            } else {
                multiple_actions.collapse();

                selectImage();
            }


        } else if (view == action_text) {

            // Tracking Event
            MainApplication.getInstance().trackEvent("Post", "Add Text", "Add Text button clicked", tinyDb.getString("ID"));

            multiple_actions.collapse();
            tinyDb.putString("MideaType", "Text");


            Intent intent = new Intent(ac, SelectedImages.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            ac.overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);

        } else if (view == send) {


            // Tracking Event
            MainApplication.getInstance().trackEvent("Post", "Add Comment", "Comment added ", tinyDb.getString("ID"));

            if (!comment_edt.getText().toString().isEmpty()) {

                Date today = new Date();
                SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss a");
                String dateToStr = format.format(today);
//                //System.out.println(dateToStr);


                postCommentToServer(comment_edt.getText().toString(), contentId, clicked_post_position);


                comment_edt.setText("");


                if (sheetBehavior.getState() != BottomSheetBehavior.STATE_EXPANDED) {
                    sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                } else {
                    sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                }

                InputMethodManager imm = (InputMethodManager) ac.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

                new ParticleSystem(ac, 20, R.drawable.comment01, 5000)
                        .setSpeedRange(0.1f, 0.25f)
                        .setRotationSpeedRange(10, 20)
                        .setInitialRotationRange(0, 360)
                        .oneShot(likeView, 10);


            } else {
                Toast.makeText(ac, "Please add comment", Toast.LENGTH_SHORT).show();
            }
        }

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {


//        //System.out.println(resultCode + "   resulttt   " + requestCode);
        if (requestCode == REQUEST_CAMERA_VIDEO) {
            if (resultCode == RESULT_OK) {
                try {
                    ArrayList<String> selectionResult = new ArrayList<>();
                    if (data != null) {
                        Uri selectedImageUri = data.getData();
                        String realPath;

                        realPath = FilePath.getPath(ac, selectedImageUri);

                        selectionResult.add(realPath);
                        tinyDb.putListString("Media_Path", selectionResult);
                    } else {

                        String realPath;

                        realPath = FilePath.getPath(ac, Uri.fromFile(mTmpFile));

                        selectionResult.add(realPath);
                        tinyDb.putListString("Media_Path", selectionResult);
                    }

                    Intent intent = new Intent(ac, SelectedImages.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    ac.overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);


                } catch (Exception e) {
                }
            }

        } else if (requestCode == REQUEST_CAMERA_IMAGE) {
            if (resultCode == RESULT_OK) {
                try {
                    ArrayList<String> selectionResult = new ArrayList<>();
                    if (data != null) {
                        Uri selectedImageUri = data.getData();
                        String realPath;

                        realPath = FilePath.getPath(ac, selectedImageUri);

                        selectionResult.add(realPath);
                        tinyDb.putListString("Media_Path", selectionResult);
                    } else {

                        String realPath;

                        realPath = FilePath.getPath(ac, Uri.fromFile(mTmpFile));

                        selectionResult.add(realPath);
                        tinyDb.putListString("Media_Path", selectionResult);
                    }

                    Intent intent = new Intent(ac, SelectedImages.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    ac.overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);


                } catch (Exception e) {
                }
            }

        } else if (requestCode == SELECT_AUDIO) {


            if (resultCode == RESULT_OK) {

                ArrayList<String> selectionResult = new ArrayList<>();

                String realPath = null;
                Uri selectedImageUri = data.getData();
//                //System.out.println(selectedImageUri.toString());
                realPath = FilePath.getPath(ac, selectedImageUri);

                selectionResult.add(realPath);
                tinyDb.putListString("Media_Path", selectionResult);


                Intent intent = new Intent(ac, SelectedImages.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                ac.overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);


            }

        } else if (requestCode == 4) {
            contentId = all_postListt.get(clicked_post_position).getId();
            postShareToServer(contentId, clicked_post_position);

        } else if (requestCode == OPEN_MEDIA_PICKER) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK && data != null) {
                ArrayList<String> selectionResult = data.getStringArrayListExtra("result");
                tinyDb.putListString("Media_Path", selectionResult);

                Intent intent = new Intent(ac, SelectedImages.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                ac.overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);


            }
        }

    }


    public class All_Post_Adapter extends RecyclerView.Adapter<All_Post_Adapter.MyViewHolder> {

        Context con;
        List<Contentlist> all_postList = new ArrayList<>();
        private final static int IMAGE_VIEW = 0;
        private final static int TEXT_VIEW = 1;
        private final static int VIDEO_VIEW = 2;
        private final static int YOUTUBE_VIEW = 3;
        private final static int MULTI_IMAGES_VIEW = 4;
        private final static int AUDIO_VIEW = 5;


        All_Post_Adapter(Context c, List<Contentlist> all_postListt) {
            con = c;
            all_postList = all_postListt;
        }


        @Override
        public long getItemId(int position) {
            return position;
        }


        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            int layoutRes = 0;
            switch (viewType) {
                case IMAGE_VIEW:
                    layoutRes = R.layout.row_feed_image_view;
                    break;
                case TEXT_VIEW:
                    layoutRes = R.layout.row_feed_text_view;
                    break;
                case VIDEO_VIEW:
                    layoutRes = R.layout.row_feed_video_view;
                    break;
                case YOUTUBE_VIEW:
                    layoutRes = R.layout.row_youtube_view;
                    break;
                case AUDIO_VIEW:
                    layoutRes = R.layout.row_feed_audio_view;
                    break;
            }

            View view = LayoutInflater.from(parent.getContext()).inflate(layoutRes, parent, false);
            return new MyViewHolder(view, viewType);
        }

        @Override
        public int getItemViewType(int position) {
            if (all_postList.get(position).getType().equals("uvid")) {
                return YOUTUBE_VIEW;
            } else if (all_postList.get(position).getType().equals("vid")) {
                return VIDEO_VIEW;
            } else if (all_postList.get(position).getType().equals("img")) {
                return IMAGE_VIEW;
            } else if (all_postList.get(position).getType().equals("txt")) {
                return TEXT_VIEW;
            } else if (all_postList.get(position).getType().equals("aud")) {
                return AUDIO_VIEW;
            } else {
                return TEXT_VIEW;
            }
        }


        @Override
        public void onBindViewHolder(final MyViewHolder holder, final int position) {

            MyViewHolder viewHolder = (MyViewHolder) holder;
            holder.mDataPosition = position;

            String userImage = all_postList.get(position).getUser().getUimage();
            if (userImage != null) {
                ImageRequest requestU = ImageRequestBuilder.newBuilderWithSource(Uri.parse(all_postList.get(position).getUser().getUimage()))
                        .setResizeOptions(new ResizeOptions(60, 60))
                        .build();
                PipelineDraweeController controllerU = (PipelineDraweeController) Fresco.newDraweeControllerBuilder()
                        .setOldController(holder.imageView.getController())
                        .setImageRequest(requestU)
                        .build();

                holder.imageView.setController(controllerU);
            } else {
                holder.imageView.setActualImageResource(R.drawable.usericon);
            }


            holder.name.setText(all_postList.get(position).getUser().getName());

            if (all_postList.get(position).getUser().isFollow()) {
                holder.add_follower.setVisibility(GONE);
            } else {
                holder.add_follower.setVisibility(View.VISIBLE);
            }

            try {
                holder.heading.setText(all_postList.get(position).getTitle());

                if (holder.description != null)
                    holder.description.setText(all_postList.get(position).getDesc());


                holder.date.setText(all_postList.get(position).getDisplayTime());
                holder.comment.setText(all_postList.get(position).getNumcomm() + " " + ac.getResources().getString(R.string.comment));
                holder.like.setText(all_postList.get(position).getNumlikes() + " " + ac.getResources().getString(R.string.like));
                holder.share.setText(all_postList.get(position).getNumshare() + " " + ac.getResources().getString(R.string.share));
            } catch (Exception e) {
            }

            ((MyAdapter) holder.recycle_rections.getAdapter()).updateData(all_postList.get(position).getLikes());
            ((MyAdapterTags) holder.recycle_tags.getAdapter()).updateData(all_postList.get(position).getTags());

            if (holder.mViewType == YOUTUBE_VIEW) {

                holder.youtube_player_view.getPlayerUIController().showFullscreenButton(false);
                holder.youtube_player_view.getPlayerUIController().showYouTubeButton(false);
                lifecycle.addObserver(holder.youtube_player_view);


                holder.youtube_player_view.initialize(new YouTubePlayerInitListener() {
                    @Override
                    public void onInitSuccess(@NonNull final YouTubePlayer initializedYouTubePlayer) {
                        initializedYouTubePlayer.addListener(new AbstractYouTubePlayerListener() {
                            @Override
                            public void onReady() {
                                holder.youTubePlayer = initializedYouTubePlayer;

                                try {
                                    holder.youTubePlayer.cueVideo(all_postList.get(holder.mDataPosition).getVideoimages().get(0).getV(), 0);
                                } catch (Exception e) {
                                }

                            }
                        });
                    }
                }, true);


//                       if (!youtube_check) {
//                          String videoId = all_postList.get(position).getVideoimages().get(0).getV();
//                          viewHolder.cueVideo(videoId);
//
//                      }

            } else if (holder.mViewType == AUDIO_VIEW) {

                holder.audio_player.setAudioTarget(all_postList.get(position).getAudios().get(0));
//                AudioSenseiListObserver.getInstance().registerLifecycle(lifecycle);

            } else if (holder.mViewType == IMAGE_VIEW) {

                if (all_postList.get(position).getImages().size() > 3) {

                    holder.image2_lay.setVisibility(View.VISIBLE);
                    holder.image3_lay.setVisibility(View.VISIBLE);
                    holder.plus_icon.setVisibility(View.VISIBLE);
                    holder.play_gif.setVisibility(View.GONE);
                    holder.image1.setClickable(false);

                    holder.image1.setImageURI(all_postList.get(position).getImages().get(0), ac);
                    holder.image2.setImageURI(all_postList.get(position).getImages().get(1), ac);
                    holder.image3.setImageURI(all_postList.get(position).getImages().get(2), ac);

                } else if (all_postList.get(position).getImages().size() == 3) {
                    holder.image2_lay.setVisibility(View.VISIBLE);
                    holder.image3_lay.setVisibility(View.VISIBLE);
                    holder.plus_icon.setVisibility(GONE);
                    holder.play_gif.setVisibility(View.GONE);
                    holder.image1.setClickable(false);

                    holder.image1.setImageURI(all_postList.get(position).getImages().get(0), ac);

                    holder.image2.setImageURI(all_postList.get(position).getImages().get(1), ac);

                    holder.image3.setImageURI(all_postList.get(position).getImages().get(2), ac);

                } else if (all_postList.get(position).getImages().size() == 2) {

                    holder.image2_lay.setVisibility(View.VISIBLE);
                    holder.image3_lay.setVisibility(GONE);
                    holder.plus_icon.setVisibility(GONE);
                    holder.play_gif.setVisibility(View.GONE);
                    holder.image1.setClickable(false);


                    holder.image1.setImageURI(all_postList.get(position).getImages().get(0), ac);

                    holder.image2.setImageURI(all_postList.get(position).getImages().get(1), ac);

                } else if (all_postList.get(position).getImages().size() == 1) {

                    holder.image2_lay.setVisibility(GONE);
                    holder.image3_lay.setVisibility(GONE);
                    holder.plus_icon.setVisibility(GONE);

                    holder.play_gif.setVisibility(View.GONE);


//                    String uri = all_postList.get(position).getImages().get(0);
//                    String extension = uri.substring(uri.lastIndexOf("."));

                    holder.image1.setClickable(false);
                    holder.image1.setImageURI(all_postList.get(position).getImages().get(0), ac, (View) holder.play_gif);

                    /**
                     if (extension.equals(".gif")) {
                     holder.play_gif.setVisibility(View.VISIBLE);
                     holder.image1.setClickable(true);
                     }
                     else {
                     holder.image1.setClickable(false);
                     holder.play_gif.setVisibility(View.GONE);
                     }
                     **/
                }

            } else if (holder.mViewType == VIDEO_VIEW) {

                if (all_postList.get(position).getVideos().size() > 3) {

                    holder.video2_lay.setVisibility(View.VISIBLE);
                    holder.video3_lay.setVisibility(View.VISIBLE);
                    holder.videoplus_icon.setVisibility(View.VISIBLE);

                    holder.video_view3.setUp(all_postList.get(position).getVideos().get(2),
                            JZVideoPlayerStandard.SCREEN_WINDOW_NORMAL,
                            "");
                    Glide.with(ac)
                            .load(all_postList.get(position).getVideoimages().get(2).getI())
                            .into(holder.video_view3.thumbImageView);

                    holder.video_view2.setUp(all_postList.get(position).getVideos().get(1),
                            JZVideoPlayerStandard.SCREEN_WINDOW_NORMAL,
                            "");
                    Glide.with(ac)
                            .load(all_postList.get(position).getVideoimages().get(1).getI())
                            .into(holder.video_view2.thumbImageView);

                    holder.video_view1.setUp(all_postList.get(position).getVideos().get(0),
                            JZVideoPlayerStandard.SCREEN_WINDOW_NORMAL,
                            "");
                    try {
                        Glide.with(ac)
                                .load(all_postList.get(position).getVideoimages().get(0).getI())
                                .into(holder.video_view1.thumbImageView);
                    } catch (Exception e) {
                    }

                    ((MyViewHolder) holder).videoplus_icon.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
//                            //System.out.println("content detail 0000");
                            // Tracking Event
                            MainApplication.getInstance().trackEvent("AllFeeds", "Post Clicked", "Post Clicked to view detail", tinyDb.getString("ID"));

                            clicked_post_position = position;

                            contentlistMain = all_postList.get(position);
                            tinyDb.putInt("CLICKED_POS", clicked_post_position);
//                            //System.out.println("content detail 1111");
                            Intent intent = new Intent(ac, ContentDetail.class);
                            intent.putExtra("Screen", "AllFeed");
                            startActivity(intent);
//                            ac.overridePendingTransition(R.animator.slide_up, R.animator.slide_down);
                        }
                    });


                } else if (all_postList.get(position).getVideos().size() == 3) {
                    holder.video2_lay.setVisibility(View.VISIBLE);
                    holder.video3_lay.setVisibility(View.VISIBLE);
                    holder.videoplus_icon.setVisibility(View.GONE);

                    holder.video_view3.setUp(all_postList.get(position).getVideos().get(2),
                            JZVideoPlayerStandard.SCREEN_WINDOW_NORMAL,
                            "");
                    Glide.with(ac)
                            .load(all_postList.get(position).getVideoimages().get(2).getI())
                            .into(holder.video_view3.thumbImageView);

                    holder.video_view2.setUp(all_postList.get(position).getVideos().get(1),
                            JZVideoPlayerStandard.SCREEN_WINDOW_NORMAL,
                            "");
                    Glide.with(ac)
                            .load(all_postList.get(position).getVideoimages().get(1).getI())
                            .into(holder.video_view2.thumbImageView);

                    holder.video_view1.setUp(all_postList.get(position).getVideos().get(0),
                            JZVideoPlayerStandard.SCREEN_WINDOW_NORMAL,
                            "");
                    try {
                        Glide.with(ac)
                                .load(all_postList.get(position).getVideoimages().get(0).getI())
                                .into(holder.video_view1.thumbImageView);
                    } catch (Exception e) {
//                        e.printStackTrace();
                    }

                } else if (all_postList.get(position).getVideos().size() == 2) {
                    holder.video2_lay.setVisibility(View.VISIBLE);
                    holder.video3_lay.setVisibility(GONE);
                    holder.videoplus_icon.setVisibility(View.GONE);

                    holder.video_view2.setUp(all_postList.get(position).getVideos().get(1),
                            JZVideoPlayerStandard.SCREEN_WINDOW_NORMAL,
                            "");
                    Glide.with(ac)
                            .load(all_postList.get(position).getVideoimages().get(1).getI())
                            .into(holder.video_view2.thumbImageView);

                    holder.video_view1.setUp(all_postList.get(position).getVideos().get(0),
                            JZVideoPlayerStandard.SCREEN_WINDOW_NORMAL,
                            "");
                    try {
                        Glide.with(ac)
                                .load(all_postList.get(position).getVideoimages().get(0).getI())
                                .into(holder.video_view1.thumbImageView);
                    } catch (Exception e) {
//                        e.printStackTrace();
                    }

                } else if (all_postList.get(position).getVideos().size() == 1) {

                    try {
                        holder.video2_lay.setVisibility(GONE);
                        holder.videoplus_icon.setVisibility(GONE);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                    try {
                        holder.video_view1.setUp(all_postList.get(position).getVideos().get(0),
                                JZVideoPlayerStandard.SCREEN_WINDOW_NORMAL,
                                "");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try {
                        Glide.with(ac)
                                .load(all_postList.get(position).getVideoimages().get(0).getI())
                                .into(holder.video_view1.thumbImageView);
                    } catch (Exception e) {
                    }
                }

            }
        }


        @Override
        public int getItemCount() {

            return all_postList.size();
        }


        public class MyViewHolder extends RecyclerView.ViewHolder {
            YouTubePlayerView youtube_player_view;
            YouTubePlayer youTubePlayer;


            ReadMoreTextView description;
            ImageButton expand_collapse;


            TextView name, heading, comment, like, share, date; // init the item view's
            SimpleDraweeView imageView;

            WrapContentDraweeView image1, image2, image3;
            ImageView play_gif;

            ImageView like_img, comment_img;
            RelativeLayout media_lay, top_lay;
            LinearLayout comment_lay, like_lay, share_lay;

            JZVideoPlayerStandard video_view1, video_view2, video_view3;
            RecyclerView recycle_rections, recycle_tags;

            Button add_follower;

            RelativeLayout plus_icon, videoplus_icon;
            RelativeLayout video3_lay, image3_lay;
            LinearLayout video2_lay, image2_lay;


            LinearLayout images_lay;
            RelativeLayout video_lay;

            AudioSenseiPlayerView audio_player;
//            ImageView button_stop;

            int mDataPosition;
            int mViewType;
            boolean mIsSingleGif;


            public MyViewHolder(View itemView, int viewType) {
                super(itemView);

                mIsSingleGif = false;

                mViewType = viewType;


                add_follower = (Button) itemView.findViewById(R.id.add_follower);


                youtube_player_view = (YouTubePlayerView) itemView.findViewById(R.id.youtube_player_view);

                plus_icon = (RelativeLayout) itemView.findViewById(R.id.plus_icon);
                videoplus_icon = (RelativeLayout) itemView.findViewById(R.id.videoplus_icon);

                like_img = (ImageView) itemView.findViewById(R.id.like_img);
                comment_img = (ImageView) itemView.findViewById(R.id.comment_img);

                audio_player = (AudioSenseiPlayerView) itemView.findViewById(R.id.audio_player);

                comment_lay = (LinearLayout) itemView.findViewById(R.id.comment_lay);
                share_lay = (LinearLayout) itemView.findViewById(R.id.share_lay);

//                //System.out.println("view  type   " + mViewType);
                like_lay = (LinearLayout) itemView.findViewById(R.id.like_lay);
                media_lay = (RelativeLayout) itemView.findViewById(R.id.media_lay);
                top_lay = (RelativeLayout) itemView.findViewById(R.id.top_lay);

                recycle_rections = (RecyclerView) itemView.findViewById(R.id.recycle_rections);
                recycle_tags = (RecyclerView) itemView.findViewById(R.id.recycle_tags);

                name = (TextView) itemView.findViewById(R.id.name);
                description = (ReadMoreTextView) itemView.findViewById(R.id.description);


                heading = (TextView) itemView.findViewById(R.id.heading);
                comment = (TextView) itemView.findViewById(R.id.comment);
                like = (TextView) itemView.findViewById(R.id.like);
                share = (TextView) itemView.findViewById(R.id.share);
                date = (TextView) itemView.findViewById(R.id.date);

                imageView = (SimpleDraweeView) itemView.findViewById(R.id.profile_image);


                layoutManager1 = new LinearLayoutManager(ac, LinearLayoutManager.HORIZONTAL, false);
                this.recycle_rections.setLayoutManager(layoutManager1);
                this.recycle_rections.setRecycledViewPool(mLikesPool);
                MyAdapter myAdapter = new MyAdapter(null, ac);
                this.recycle_rections.setAdapter(myAdapter);

                layoutManager1 = new LinearLayoutManager(ac, LinearLayoutManager.HORIZONTAL, false);
                this.recycle_tags.setLayoutManager(layoutManager1);
                this.recycle_tags.setRecycledViewPool(mTagsPool);
                MyAdapterTags myAdapter2 = new MyAdapterTags(null, ac);
                this.recycle_tags.setAdapter(myAdapter2);

//                if(this.mViewType == AUDIO_VIEW) {
//
//                    button_stop = (ImageView) itemView.findViewById(R.id.button_stop);
//                    this.button_stop.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                           audio_player.stop();
//                        }
//                    });
//
//
//                }else
                if (this.mViewType == VIDEO_VIEW) {

                    video_view1 = (JZVideoPlayerStandard) itemView.findViewById(R.id.video_view1);
                    video_view2 = (JZVideoPlayerStandard) itemView.findViewById(R.id.video_view2);
                    video_view3 = (JZVideoPlayerStandard) itemView.findViewById(R.id.video_view3);

                    video_lay = (RelativeLayout) itemView.findViewById(R.id.video_lay);
                    video3_lay = (RelativeLayout) itemView.findViewById(R.id.video3_lay);
                    video2_lay = (LinearLayout) itemView.findViewById(R.id.video2_lay);
                } else if (this.mViewType == IMAGE_VIEW) {
                    images_lay = (LinearLayout) itemView.findViewById(R.id.images_lay);
                    image3_lay = (RelativeLayout) itemView.findViewById(R.id.image3_lay);
                    image2_lay = (LinearLayout) itemView.findViewById(R.id.image2_lay);
                    image1 = (WrapContentDraweeView) itemView.findViewById(R.id.image1);
                    image2 = (WrapContentDraweeView) itemView.findViewById(R.id.image2);
                    image3 = (WrapContentDraweeView) itemView.findViewById(R.id.image3);
                    play_gif = (ImageView) itemView.findViewById(R.id.play_gif);

                    this.plus_icon.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
//                            //System.out.println("content detail 00000");
                            MainActivity.previousfragment = getFragmentManager().findFragmentById(R.id.fragment_container);
                            // Tracking Event
                            MainApplication.getInstance().trackEvent("AllFeeds", "Post Clicked", "Post Clicked to view detail", tinyDb.getString("ID"));

                            clicked_post_position = mDataPosition;

                            contentlistMain = all_postList.get(mDataPosition);
//                            //System.out.println("content detail 11111");
                            tinyDb.putInt("CLICKED_POS", clicked_post_position);
                            Intent intent = new Intent(ac, ContentDetail.class);
                            intent.putExtra("Screen", "AllFeed");
                            startActivity(intent);
//                            ac.overridePendingTransition(R.animator.slide_up, R.animator.slide_down);
                        }
                    });

                    image1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (image1.onClick()) {
                                play_gif.setVisibility(View.GONE);
                            } else {
                                play_gif.setVisibility(View.VISIBLE);
                            }
                        }
                    });
                }

//                this.description.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//
//                        //System.out.println("content detail 0000");
//                        MainActivity.previousfragment = getFragmentManager().findFragmentById(R.id.fragment_container);
//                        // Tracking Event
//                        MainApplication.getInstance().trackEvent("AllFeeds", "Post Clicked", "Post Clicked to view detail", tinyDb.getString("ID"));
//
//                        try {
//                            if(audio_player != null)
//                                audio_player.removeAllViews();
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
//                        clicked_post_position = mDataPosition;
//
//                        contentlistMain = all_postList.get(mDataPosition);
//                        tinyDb.putInt("CLICKED_POS", clicked_post_position);
//
//                        //System.out.println("content detail 1111");
//
//                        Intent intent = new Intent( ac, ContentDetail.class );
//                        intent.putExtra("Screen", "AllFeed");
//                        startActivity( intent );
//
//                    }
//                });


                this.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
//                        //System.out.println("content detail 0000");
                        MainActivity.previousfragment = getFragmentManager().findFragmentById(R.id.fragment_container);
                        // Tracking Event
                        MainApplication.getInstance().trackEvent("AllFeeds", "Post Clicked", "Post Clicked to view detail", tinyDb.getString("ID"));


                        clicked_post_position = mDataPosition;

                        contentlistMain = all_postList.get(mDataPosition);
                        tinyDb.putInt("CLICKED_POS", clicked_post_position);

//                        //System.out.println("content detail 1111");
                        Intent intent = new Intent(ac, ContentDetail.class);
                        intent.putExtra("Screen", "AllFeed");
                        startActivity(intent);
//                        ac.overridePendingTransition(R.animator.slide_up, R.animator.slide_down);

                    }
                });


                this.like_lay.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        youtube_check = true;

                        // Tracking Event
                        MainApplication.getInstance().trackEvent("AllFeeds", "Post Liked", "Post liked", tinyDb.getString("ID"));

                        clicked_post_position = mDataPosition;
                        likeView = like_img;
                        // custom dialog
                        show1 = new Dialog(con, R.style.AlertDialogCustom);
                        show1.setContentView(R.layout.dialog_like);
                        show1.setTitle(R.string.select_reaction);


                        RecyclerView recycle_options = (RecyclerView) show1.findViewById(R.id.recycle_options);


                        contentId = all_postList.get(mDataPosition).getId();

                        likeAdapter = new LikeAdapter(ac);
                        recycle_options.setLayoutManager(new GridLayoutManager(ac, 3));
                        recycle_options.setAdapter(likeAdapter);
                        show1.show();

                    }
                });

                this.comment_lay.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        youtube_check = true;
                        // Tracking Event
                        MainApplication.getInstance().trackEvent("AllFeeds", "Post Comment", "Post comment clicked", tinyDb.getString("ID"));


                        clicked_post_position = mDataPosition;
                        likeView = comment_img;
                        contentId = all_postList.get(mDataPosition).getId();
                        getComments(contentId, "0", "500000");


                        if (sheetBehavior.getState() != BottomSheetBehavior.STATE_EXPANDED) {
                            sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                        } else {
                            sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                        }
                        LinearLayoutManager layoutManager = new LinearLayoutManager(ac, LinearLayoutManager.VERTICAL, false);
                        commentAdapter = new CommentAdapter(ac);
                        recycle_comment.setLayoutManager(layoutManager);

                    }
                });

                this.share_lay.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        // Tracking Event
                        MainApplication.getInstance().trackEvent("AllFeeds", "Post Shared", "Post Shared", tinyDb.getString("ID"));


                        clicked_post_position = mDataPosition;

                        final List<MultiFileLoadRequest> uris = new ArrayList<>();

                        if (all_postList.get(mDataPosition).getType().equals("vid")) {
                            donut_progress_layout.setVisibility(View.VISIBLE);
                            donut_progress.setProgress(0);

                            for (int i = 0; i < all_postList.get(mDataPosition).getVideos().size(); i++) {
                                MultiFileLoadRequest multiFileLoadRequest = new MultiFileLoadRequest(all_postList.get(mDataPosition).getVideos().get(i), Environment.DIRECTORY_DOWNLOADS, FileLoader.DIR_EXTERNAL_PUBLIC, true);
                                uris.add(multiFileLoadRequest);
                            }

                            whatsApp(all_postList, uris, mDataPosition);
                        } else if (all_postList.get(mDataPosition).getType().equals("img")) {
                            donut_progress_layout.setVisibility(View.VISIBLE);
                            donut_progress.setProgress(0);
                            for (int i = 0; i < all_postList.get(mDataPosition).getImages().size(); i++) {
                                MultiFileLoadRequest multiFileLoadRequest = new MultiFileLoadRequest(all_postList.get(mDataPosition).getImages().get(i), Environment.DIRECTORY_DOWNLOADS, FileLoader.DIR_EXTERNAL_PUBLIC, true);
                                uris.add(multiFileLoadRequest);
                            }

                            whatsApp(all_postList, uris, mDataPosition);
                        } else if (all_postList.get(mDataPosition).getType().equals("aud")) {
                            donut_progress_layout.setVisibility(View.VISIBLE);
                            donut_progress.setProgress(0);
                            for (int i = 0; i < all_postList.get(mDataPosition).getAudios().size(); i++) {
                                MultiFileLoadRequest multiFileLoadRequest = new MultiFileLoadRequest(all_postList.get(mDataPosition).getAudios().get(i), Environment.DIRECTORY_DOWNLOADS, FileLoader.DIR_EXTERNAL_PUBLIC, true);
                                uris.add(multiFileLoadRequest);
                            }

                            whatsApp(all_postList, uris, mDataPosition);
                        } else if (all_postList.get(mDataPosition).getType().equals("uvid")) {


                            Intent i = new Intent(Intent.ACTION_SEND);
                            i.setType("text/plain");
                            i.setPackage("com.whatsapp");

                            i.putExtra(Intent.EXTRA_TEXT, all_postList.get(mDataPosition).getTags() + "\n" + all_postList.get(mDataPosition).getTitle() + "\nhttps://www.youtube.com/watch?v=" + all_postList.get(mDataPosition).getVideoimages().get(0).getV() + "\n\n" + getString(R.string.share_link) + " https://play.google.com/store/apps/details?id=com.panagola.app.om");
                            startActivityForResult(i, 4);
                        } else {

                            Intent i = new Intent(Intent.ACTION_SEND);
                            i.setType("text/plain");
                            i.setPackage("com.whatsapp");

                            i.putExtra(Intent.EXTRA_TEXT, all_postList.get(mDataPosition).getTags() + "\n" + all_postList.get(mDataPosition).getTitle() + "\n\n" + getString(R.string.share_link) + " https://play.google.com/store/apps/details?id=com.panagola.app.om");
                            startActivityForResult(i, 4);
                        }
                    }
                });

                this.top_lay.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        // Tracking Event
                        MainApplication.getInstance().trackEvent("AllFeeds", "Post User clicked", "Post User clicked to view detail", tinyDb.getString("ID"));
                        try {
                            audio_player.stop();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        tinyDb.putString("ClIK_USER_ID", all_postList.get(mDataPosition).getUser().getId());

                        Intent mainIntent = new Intent(ac, UserDetailAc.class);
                        startActivity(mainIntent);
                        ac.overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                    }
                });


                this.add_follower.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // Tracking Event
                        MainApplication.getInstance().trackEvent("AllFeeds", "User added to follow", "User added to follow", tinyDb.getString("ID"));
                        clicked_post_position = mDataPosition;
                        postFollow(all_postList.get(mDataPosition).getUser().getId(), "1");
                    }
                });

            }
        }
    }


    public class All_Videos_Adapter extends RecyclerView.Adapter<All_Videos_Adapter.MyViewHolder> {

        Context con;
        List<Contentlist> all_postList = new ArrayList<>();
        private final static int VIDEO_VIEW = 2;


        All_Videos_Adapter(Context c, List<Contentlist> all_postListt) {
            con = c;
            all_postList = all_postListt;
        }


        @Override
        public long getItemId(int position) {
            return position;
        }


        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            int layoutRes = 0;
            switch (viewType) {
                case VIDEO_VIEW:
                    layoutRes = R.layout.row_video_feeds;
                    break;


            }

            View view = LayoutInflater.from(parent.getContext()).inflate(layoutRes, parent, false);
            return new MyViewHolder(view, viewType);
        }

        @Override
        public int getItemViewType(int position) {

            return VIDEO_VIEW;

        }


        @Override
        public void onBindViewHolder(final MyViewHolder holder, final int position) {

            MyViewHolder viewHolder = (MyViewHolder) holder;
            holder.mDataPosition = position;

            holder.date.setText(all_postList.get(position).getDisplayTime());


            ((MyAdapter) holder.recycle_rections.getAdapter()).updateData(all_postList.get(position).getLikes());


            try {
                holder.heading.setText(all_postList.get(position).getTitle());

                if (holder.description != null)
                    holder.description.setText(all_postList.get(position).getDesc());


            } catch (Exception e) {
            }

            try {
                holder.postimage.setImageURI(Uri.parse(all_postList.get(position).getVideoimages().get(0).getI()));
            } catch (Exception e) {
                e.printStackTrace();
            }


            holder.click_lay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
//                    //System.out.println("content detail 00000");
                    MainActivity.previousfragment = getFragmentManager().findFragmentById(R.id.fragment_container);
                    // Tracking Event
                    MainApplication.getInstance().trackEvent("AllFeeds", "Post Clicked", "Post Clicked to view detail", tinyDb.getString("ID"));


                    clicked_post_position = position;

                    contentlistMain = all_postList.get(position);
                    tinyDb.putInt("CLICKED_POS", clicked_post_position);

//                    //System.out.println("content detail 1111");
                    Intent intent = new Intent(ac, ContentDetail.class);
                    intent.putExtra("Screen", "AllFeed");
                    startActivity(intent);
//                    ac.overridePendingTransition(R.animator.slide_up, R.animator.slide_down);
                }
            });
        }


        @Override
        public int getItemCount() {

            return all_postList.size();
        }


        public class MyViewHolder extends RecyclerView.ViewHolder {


            RelativeLayout click_lay;

            ReadMoreTextView description;

            TextView heading, date;


            SimpleDraweeView postimage;
            RecyclerView recycle_rections;


            int mDataPosition;
            int mViewType;


            public MyViewHolder(View itemView, int viewType) {
                super(itemView);


                mViewType = viewType;

                postimage = (SimpleDraweeView) itemView.findViewById(R.id.postimage);


                description = (ReadMoreTextView) itemView.findViewById(R.id.description);
                heading = (TextView) itemView.findViewById(R.id.heading);
                date = (TextView) itemView.findViewById(R.id.date);
                click_lay = (RelativeLayout) itemView.findViewById(R.id.click_lay);

                recycle_rections = (RecyclerView) itemView.findViewById(R.id.recycle_rections);


                layoutManager1 = new LinearLayoutManager(ac, LinearLayoutManager.HORIZONTAL, false);
                this.recycle_rections.setLayoutManager(layoutManager1);
                this.recycle_rections.setRecycledViewPool(mLikesPool);
                MyAdapter myAdapter = new MyAdapter(null, ac);
                this.recycle_rections.setAdapter(myAdapter);


            }
        }
    }


    public void whatsApp(final List<Contentlist> all_postList, final List<MultiFileLoadRequest> uris, final int position) {
//        //System.out.println("URI list  " + uris);

        pathsList = new ArrayList<>();

        FileLoader.multiFileDownload(ac)
                .fromDirectory(Environment.DIRECTORY_PICTURES, FileLoader.DIR_EXTERNAL_PUBLIC)
                .progressListener(new MultiFileDownloadListener() {
                    @Override
                    public void onProgress(File downloadedFile, int progress, int totalFiles) {

                        donut_progress.setMax(totalFiles);
                        donut_progress.setProgress(progress);
//                        //System.out.println("Path listt-  " + downloadedFile.getAbsolutePath());
                        pathsList.add(downloadedFile.getAbsolutePath());

//                        //System.out.println(uris.size() + "   Path list size  " + pathsList.size());
                        if (pathsList.size() == uris.size()) {

                            donut_progress_layout.setVisibility(GONE);
                            donut_progress.setProgress(0);

                            ArrayList<Uri> uri_lis = new ArrayList<>();
                            for (int i = 0; i < pathsList.size(); i++) {
                                uri_lis.add(Uri.parse(pathsList.get(i)));
                            }


                            if (all_postList.get(position).getType().equals("aud")) {
                                Intent share = new Intent(Intent.ACTION_SEND);
                                share.putExtra(Intent.EXTRA_STREAM, uri_lis.get(0));
                                share.setType("audio/*");
                                share.setPackage("com.whatsapp");
                                share.putExtra(Intent.EXTRA_TEXT, all_postList.get(position).getTags() + "\n" + all_postList.get(position).getTitle() + "\n" + all_postList.get(position).getDesc() + "\n\n" + getString(R.string.share_link) + " https://play.google.com/store/apps/details?id=com.panagola.app.om");
                                share.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                                startActivityForResult(share, 4);
                            } else {
                                Intent videoshare = new Intent(Intent.ACTION_SEND);
                                videoshare.setType("*/*");
                                videoshare.setPackage("com.whatsapp");
                                videoshare.putExtra(Intent.EXTRA_TEXT, all_postList.get(position).getTags() + "\n" + all_postList.get(position).getTitle() + "\n" + all_postList.get(position).getDesc() + "\n\n" + getString(R.string.share_link) + " https://play.google.com/store/apps/details?id=com.panagola.app.om");
                                videoshare.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                                videoshare.putParcelableArrayListExtra(Intent.EXTRA_STREAM, uri_lis);

                                startActivityForResult(videoshare, 4);
                            }

                        }

                    }

                    @Override
                    public void onError(Exception e, int progress) {
                        super.onError(e, progress);
                    }
                }).loadMultiple(true, uris);
    }


    public String getCountOfDays(String createdDateString, String expireDateString) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());

        Date createdConvertedDate = null, expireCovertedDate = null, todayWithZeroTime = null;
        try {
            createdConvertedDate = dateFormat.parse(createdDateString);
            expireCovertedDate = dateFormat.parse(expireDateString);

            Date today = new Date();

            todayWithZeroTime = dateFormat.parse(dateFormat.format(today));
        } catch (ParseException e) {
//            e.printStackTrace();
        }

        int cYear = 0, cMonth = 0, cDay = 0;

        if (createdConvertedDate.after(todayWithZeroTime)) {
            Calendar cCal = Calendar.getInstance();
            cCal.setTime(createdConvertedDate);
            cYear = cCal.get(Calendar.YEAR);
            cMonth = cCal.get(Calendar.MONTH);
            cDay = cCal.get(Calendar.DAY_OF_MONTH);

        } else {
            Calendar cCal = Calendar.getInstance();
            cCal.setTime(todayWithZeroTime);
            cYear = cCal.get(Calendar.YEAR);
            cMonth = cCal.get(Calendar.MONTH);
            cDay = cCal.get(Calendar.DAY_OF_MONTH);
        }


    /*Calendar todayCal = Calendar.getInstance();
    int todayYear = todayCal.get(Calendar.YEAR);
    int today = todayCal.get(Calendar.MONTH);
    int todayDay = todayCal.get(Calendar.DAY_OF_MONTH);
    */

        Calendar eCal = Calendar.getInstance();
        eCal.setTime(expireCovertedDate);

        int eYear = eCal.get(Calendar.YEAR);
        int eMonth = eCal.get(Calendar.MONTH);
        int eDay = eCal.get(Calendar.DAY_OF_MONTH);

        Calendar date1 = Calendar.getInstance();
        Calendar date2 = Calendar.getInstance();

        date1.clear();
        date1.set(cYear, cMonth, cDay);
        date2.clear();
        date2.set(eYear, eMonth, eDay);

        long diff = date2.getTimeInMillis() - date1.getTimeInMillis();

        float dayCount = (float) diff / (24 * 60 * 60 * 1000);

        dayCount = dayCount + 1;

        return ("" + (int) dayCount + " Days");
    }


    public class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.MyViewHolder> {


        public CommentAdapter(Context c) {
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            // infalte the item Layout
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_comment, parent, false);
            // set the view's size, margins, paddings and layout parameters
            MyViewHolder vh = new MyViewHolder(v); // pass the view to View Holder
            return vh;
        }

        @Override
        public void onBindViewHolder(final MyViewHolder holder, final int position) {


            MyViewHolder viewHolder = (MyViewHolder) holder;

            try {
                holder.name.setText(all_commentList.get(position).getUser().getName());
            } catch (Exception e) {
//                e.printStackTrace();
            }

            try {
                holder.comment.setText(all_commentList.get(position).getComment());
            } catch (Exception e) {
//                e.printStackTrace();
            }

            try {
                holder.date.setText(all_commentList.get(position).getDisplayTime());
            } catch (Exception e) {
//                e.printStackTrace();
            }

            try {
                holder.profile_image.setImageURI(Uri.parse(all_commentList.get(position).getUser().getUimage()));
            } catch (Exception e) {
//                e.printStackTrace();
            }

            ((MyViewHolder) holder).itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    tinyDb.putString("ClIK_USER_ID", all_commentList.get(position).getUser().getId());

                    Intent mainIntent = new Intent(ac, UserDetailAc.class);
                    startActivity(mainIntent);
                    ac.overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);

                }
            });

        }

        @Override
        public int getItemCount() {
            return all_commentList.size();
        }


        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView name, comment, date;
            SimpleDraweeView profile_image;

            public MyViewHolder(View itemView) {
                super(itemView);
                // get the reference of item view's
                name = (TextView) itemView.findViewById(R.id.name);
                comment = (TextView) itemView.findViewById(R.id.comment);
                date = (TextView) itemView.findViewById(R.id.date);
                profile_image = (SimpleDraweeView) itemView.findViewById(R.id.profile_image);

            }
        }
    }


    public class LikeAdapter extends RecyclerView.Adapter<LikeAdapter.MyViewHolder> {


        public LikeAdapter(Context c) {
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            // infalte the item Layout
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_reaction, parent, false);
            // set the view's size, margins, paddings and layout parameters
            MyViewHolder vh = new MyViewHolder(v); // pass the view to View Holder
            return vh;
        }

        @Override
        public void onBindViewHolder(final MyViewHolder holder, final int position) {


            MyViewHolder viewHolder = (MyViewHolder) holder;

            try {

                holder.image.setImageResource(icons.getResourceId(position, 0));
                holder.name.setText(all_likesList.get(position).getName());

            } catch (Exception e) {
//                e.printStackTrace();
            }

            ((MyViewHolder) holder).itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    show1.dismiss();


                    postLikeToServer(String.valueOf(position), contentId, clicked_post_position);
                    int numLike = all_postListt.get(clicked_post_position).getNumlikes();
                    numLike = numLike + 1;
                    all_postListt.get(clicked_post_position).setNumlikes(numLike);

                    Like like = new Like();
                    like.setK(position);
                    like.setId("0");

                    all_postListt.get(clicked_post_position).getLikes().add(like);

                    all_post_adapter.notifyItemChanged(clicked_post_position);


//                    if (position == 0 || position == 1) {
//                        centerViewBottom.setVisibility(View.VISIBLE);
//                        centerViewBottom.play();
//                        centerViewBottom.setGifResource(R.drawable.smoke);
//
//                        final Handler handler=new Handler();
//
//                        final Runnable updateTask=new Runnable() {
//                            @Override
//                            public void run() {
//
//                                centerViewBottom.setVisibility(View.GONE);
//                                centerViewBottom.pause();
//
//                                handler.postDelayed(this,5000);
//                            }
//                        };
//
//                        handler.postDelayed(updateTask,5000);
//
//
//                    } else if (position == 5) {
//                        centerViewBottom.setVisibility(View.VISIBLE);
//                        centerViewBottom.play();
//                        centerViewBottom.setGifResource(R.drawable.candle_);
//
//                        final Handler handler=new Handler();
//
//                        final Runnable updateTask=new Runnable() {
//                            @Override
//                            public void run() {
//
//                                centerViewBottom.setVisibility(View.GONE);
//                                centerViewBottom.pause();
//
//                                handler.postDelayed(this,5000);
//                            }
//                        };
//
//                        handler.postDelayed(updateTask,5000);
//
//
//                    }else if (position == 2) {
//                        centerViewTop.setVisibility(View.VISIBLE);
//                        centerViewTop.play();
//                        centerViewTop.setGifResource(R.drawable.temple_bell);
//
//                        final Handler handler=new Handler();
//
//                        final Runnable updateTask=new Runnable() {
//                            @Override
//                            public void run() {
//
//                                centerViewTop.setVisibility(View.GONE);
//                                centerViewTop.pause();
//
//                                handler.postDelayed(this,5000);
//                            }
//                        };
//
//                        handler.postDelayed(updateTask,5000);
//
//
//                        MediaPlayer mPlayer2;
//                        mPlayer2= MediaPlayer.create(ac, R.raw.bell_test);
//                        mPlayer2.start();
//
//
//                    }else if(position == 3 || position == 4){
//                        centerViewTop.setVisibility(View.VISIBLE);
//                        centerViewTop.play();
//                        centerViewTop.setGifResource(R.drawable.flawers_fall);
//
//                        final Handler handler=new Handler();
//
//                        final Runnable updateTask=new Runnable() {
//                            @Override
//                            public void run() {
//
//                                centerViewTop.setVisibility(View.GONE);
//                                centerViewTop.pause();
//
//                                handler.postDelayed(this,5000);
//                            }
//                        };
//
//                        handler.postDelayed(updateTask,5000);
//
//                    }else if( position == 10 || position == 11){
//                        centerViewTop.setVisibility(View.VISIBLE);
//                        centerViewTop.play();
//                        centerViewTop.setGifResource(R.drawable.rain);
//
//                        final Handler handler=new Handler();
//
//                        final Runnable updateTask=new Runnable() {
//                            @Override
//                            public void run() {
//
//                                centerViewTop.setVisibility(View.GONE);
//                                centerViewTop.pause();
//
//                                handler.postDelayed(this,5000);
//                            }
//                        };
//
//                        handler.postDelayed(updateTask,5000);
//
//                    }else {
                    new ParticleSystem(ac, 20, icons.getResourceId(position, 0), 5000)
                            .setSpeedRange(0.1f, 0.25f)
                            .setRotationSpeedRange(0, 0)
                            .setInitialRotationRange(0, 0)
                            .oneShot(likeView, 10);
//                    }


                }
            });

        }

        @Override
        public int getItemCount() {
            return all_likesList.size();
        }


        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView name;
            SimpleDraweeView image;

            public MyViewHolder(View itemView) {
                super(itemView);
                // get the reference of item view's
                name = (TextView) itemView.findViewById(R.id.name);
                ;
                image = (SimpleDraweeView) itemView.findViewById(R.id.image);

            }
        }
    }


    private void getContents(String sel_tag, String chanel, final boolean refresh, int numresltt, int offset) {

        ArrayList<RequestBody> chanalListPart = new ArrayList<>();
        ArrayList<RequestBody> tagsListPart = new ArrayList<>();

        RequestBody user_id_part = RequestBody.create(MediaType.parse("multipart/form-data"), tinyDb.getString("ID"));
        RequestBody chanel_part = RequestBody.create(MediaType.parse("multipart/form-data"), chanel);
        RequestBody lang_part = RequestBody.create(MediaType.parse("multipart/form-data"), tinyDb.getString("LANG"));
        RequestBody num_res_part = RequestBody.create(MediaType.parse("multipart/form-data"), String.valueOf(numresltt));
        RequestBody offset_part = RequestBody.create(MediaType.parse("multipart/form-data"), String.valueOf(offset));
//        RequestBody tag_part = RequestBody.create(MediaType.parse("multipart/form-data"), "");
        RequestBody chanal_part = RequestBody.create(MediaType.parse("multipart/form-data"), getChanelsResponse.getChannels().get(channel_pos).getN());
        RequestBody tag_part = RequestBody.create(MediaType.parse("multipart/form-data"), sel_tag);

        chanalListPart.add(chanal_part);
        tagsListPart.add(tag_part);

        ArrayList<RequestBody> typeListPart = new ArrayList<>();
        if (tinyDb.getString("TYPE").equals("VID")) {
            for (int i = 0; i < 2; i++) {
                RequestBody type = RequestBody.create(MediaType.parse("multipart/form-data"), "vid");
                typeListPart.add(type);
                RequestBody type1 = RequestBody.create(MediaType.parse("multipart/form-data"), "uvid");
                typeListPart.add(type1);
            }
        }
//        //System.out.println("chenalssss    " + chanalListPart);

        if (networkChecking.isNetworkAvailable(ac)) {
            if (!refresh) {
                prog.progDialog(ac);
            }
            MainApplication.getApiService().getContent("content/get/" + tinyDb.getString("ID"), typeListPart, chanalListPart, lang_part, offset_part, num_res_part, tagsListPart, user_id_part).enqueue(new Callback<ContentResponce>() {
                @Override
                public void onResponse(Call<ContentResponce> call, Response<ContentResponce> response) {
                    if (!refresh) {
                        prog.hideProg();
                    }
                    if (response.isSuccessful()) {

                        if (response.body().getRc() == 1) {

                            if (!refresh) {

                                if (response.body().getContentlist().size() != 0) {


                                    all_postListt = response.body().getContentlist();

                                    if (tinyDb.getString("TYPE").equals("VID")) {
                                        all_videos_adapter = new All_Videos_Adapter(ac, all_postListt);
                                        all_videos_adapter.setHasStableIds(true);
                                        recycle_post.setAdapter(all_videos_adapter);
                                    } else {
                                        all_post_adapter = new All_Post_Adapter(ac, all_postListt);
                                        all_post_adapter.setHasStableIds(true);
                                        recycle_post.setAdapter(all_post_adapter);
                                    }


                                } else {
                                    snakeBaar.showSnackBar(ac, getString(R.string.no_more_result), main_lay);
                                }

                            } else {


                                if (response.body().getContentlist().size() != 0) {

                                    all_postListt.clear();
                                    all_post_adapter.notifyDataSetChanged();


                                    for (int i = 0; i < response.body().getContentlist().size(); i++) {


                                        all_postListt.add(response.body().getContentlist().get(i));

                                    }

                                    if (tinyDb.getString("TYPE").equals("VID")) {
                                        all_videos_adapter = new All_Videos_Adapter(ac, all_postListt);
                                        all_videos_adapter.setHasStableIds(true);
                                        recycle_post.setAdapter(all_videos_adapter);
                                    } else {
                                        all_post_adapter = new All_Post_Adapter(ac, all_postListt);
                                        all_post_adapter.setHasStableIds(true);
                                        recycle_post.setAdapter(all_post_adapter);
                                    }

                                    checkloading = false;
                                    try {
                                        swipyrefreshlayout.setRefreshing(false);
                                    } catch (Exception e) {
                                    }

                                } else {
                                    snakeBaar.showSnackBar(ac, getString(R.string.no_more_result), main_lay);
                                }


                            }

                        } else {
                            snakeBaar.showSnackBar(ac, "No Content Found", main_lay);


                        }
                    }
                }

                @Override
                public void onFailure(Call<ContentResponce> call, Throwable t) {
                    if (!refresh) {
                        prog.hideProg();
                    }
                    t.printStackTrace();
                }
            });


        } else {
            snakeBaar.showSnackBar(ac, getString(R.string.no_internet), main_lay);
        }
    }


    private void getComments(String content_id, String offset, String num_reslt) {

        if (networkChecking.isNetworkAvailable(ac)) {
            prog.progDialog(ac);


            all_commentList = new ArrayList<>();
            RequestBody lang_part = RequestBody.create(MediaType.parse("multipart/form-data"), tinyDb.getString("LANG"));
            RequestBody offset_part = RequestBody.create(MediaType.parse("multipart/form-data"), offset);
            RequestBody content_id_part = RequestBody.create(MediaType.parse("multipart/form-data"), content_id);
            RequestBody numreslt_part = RequestBody.create(MediaType.parse("multipart/form-data"), num_reslt);


            MainApplication.getApiService().getComment("content/comments/get/" + tinyDb.getString("ID"), content_id_part, offset_part, numreslt_part, lang_part).enqueue(new Callback<JsonElement>() {
                @Override
                public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                    prog.hideProg();
                    if (response.isSuccessful()) {

                        try {

                            try {
                                JSONObject jsonObj = new JSONObject(String.valueOf(response.body()));
                                responceCode = jsonObj.getString("rc");

                            } catch (Exception e1) {
                            }

                            if (responceCode.equals("1")) {
                                Gson gson = new Gson();
                                GetCommentResponce responcee = null;


//                                //System.out.println("responceeeeeeeeeeeeee   " + response.body());
                                responcee = gson.fromJson(String.valueOf(response.body()), GetCommentResponce.class);

                                all_commentList = responcee.getContent().getComments();
                                recycle_comment.setAdapter(commentAdapter);

                            } else {
                                snakeBaar.showSnackBar(ac, "No comment found.", main_lay);
                            }


                        } catch (Exception e1) {
                        }

                    }
                }

                @Override
                public void onFailure(Call<JsonElement> call, Throwable t) {
                    prog.hideProg();
                    t.printStackTrace();
                }
            });


        } else {
            snakeBaar.showSnackBar(ac, getString(R.string.no_internet), main_lay);
        }

    }


    private void postCommentToServer(final String comment_str, String content_id, final int clicked_post_position) {


        if (networkChecking.isNetworkAvailable(ac)) {
            prog.progDialog(ac);


            RequestBody user_id_part = RequestBody.create(MediaType.parse("multipart/form-data"), tinyDb.getString("ID"));
            RequestBody password_part = RequestBody.create(MediaType.parse("multipart/form-data"), tinyDb.getString("PASSWORD"));
            RequestBody content_id_part = RequestBody.create(MediaType.parse("multipart/form-data"), content_id);
            RequestBody comment_part = RequestBody.create(MediaType.parse("multipart/form-data"), comment_str);


            MainApplication.getApiService().addComment("content/comment/add/" + tinyDb.getString("ID"), content_id_part, user_id_part, password_part, comment_part).enqueue(new Callback<JsonElement>() {
                @Override
                public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                    prog.hideProg();
                    if (response.isSuccessful()) {

                        try {
                            JSONObject jsonObj = new JSONObject(String.valueOf(response.body()));
                            String responceCode = jsonObj.getString("rc");
                            if (responceCode.equals("1")) {
                                snakeBaar.showSnackBar(ac, getString(R.string.comment_added), main_lay);
                                int num_of_comment = all_postListt.get(clicked_post_position).getNumcomm();
                                num_of_comment = num_of_comment + 1;
                                all_postListt.get(clicked_post_position).setNumcomm(num_of_comment);

                                all_post_adapter.notifyItemChanged(clicked_post_position);

//                                //System.out.println(clicked_post_position + "   dhfehfh  " + num_of_comment);
                                if (!youtube_check) {
                                    all_post_adapter.notifyItemChanged(clicked_post_position);
                                }
                            }

                        } catch (Exception e1) {
                        }

                    }
                }

                @Override
                public void onFailure(Call<JsonElement> call, Throwable t) {
                    prog.hideProg();
                    t.printStackTrace();
                }
            });

        } else {
            snakeBaar.showSnackBar(ac, getString(R.string.no_internet), main_lay);
        }
    }


    private void postLikeToServer(final String likePostion, String content_id, final int clicked_post_position) {


        if (networkChecking.isNetworkAvailable(ac)) {
//            prog.progDialog( ac );


            RequestBody user_id_part = RequestBody.create(MediaType.parse("multipart/form-data"), tinyDb.getString("ID"));
            RequestBody password_part = RequestBody.create(MediaType.parse("multipart/form-data"), tinyDb.getString("PASSWORD"));
            RequestBody content_id_part = RequestBody.create(MediaType.parse("multipart/form-data"), content_id);
            RequestBody comment_part = RequestBody.create(MediaType.parse("multipart/form-data"), likePostion);


            MainApplication.getApiService().addLike("content/like/" + tinyDb.getString("ID"), content_id_part, user_id_part, password_part, comment_part).enqueue(new Callback<JsonElement>() {
                @Override
                public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
//                    prog.hideProg();
                    if (response.isSuccessful()) {

                        try {
                            JSONObject jsonObj = new JSONObject(String.valueOf(response.body()));
                            String responceCode = jsonObj.getString("rc");
                            if (responceCode.equals("1")) {
                                snakeBaar.showSnackBar(ac, getString(R.string.reaction_posted), main_lay);
                                int num_of_comment = all_postListt.get(clicked_post_position).getNumshare();
                                num_of_comment = num_of_comment + 1;
                                all_postListt.get(clicked_post_position).setNumshare(num_of_comment);
                                all_post_adapter.notifyItemChanged(clicked_post_position);
                            }

                        } catch (Exception e1) {
                        }

                    }
                }

                @Override
                public void onFailure(Call<JsonElement> call, Throwable t) {
                    prog.hideProg();
                    t.printStackTrace();
                }
            });

        } else {
            snakeBaar.showSnackBar(ac, getString(R.string.no_internet), main_lay);
        }
    }


    private void postShareToServer(String content_id, final int clicked_post_position) {


        if (networkChecking.isNetworkAvailable(ac)) {
//            prog.progDialog( ac );


            RequestBody user_id_part = RequestBody.create(MediaType.parse("multipart/form-data"), tinyDb.getString("ID"));
            RequestBody password_part = RequestBody.create(MediaType.parse("multipart/form-data"), tinyDb.getString("PASSWORD"));
            RequestBody content_id_part = RequestBody.create(MediaType.parse("multipart/form-data"), content_id);


            MainApplication.getApiService().addShare("content/share/" + tinyDb.getString("ID"), content_id_part, user_id_part, password_part).enqueue(new Callback<JsonElement>() {
                @Override
                public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
//                    prog.hideProg();
                    if (response.isSuccessful()) {

                        try {
                            JSONObject jsonObj = new JSONObject(String.valueOf(response.body()));
                            String responceCode = jsonObj.getString("rc");
                            if (responceCode.equals("1")) {
                                snakeBaar.showSnackBar(ac, getString(R.string.share_posted), main_lay);
                                int num_of_comment = all_postListt.get(clicked_post_position).getNumshare();
                                num_of_comment = num_of_comment + 1;
                                all_postListt.get(clicked_post_position).setNumshare(num_of_comment);

                                all_post_adapter.notifyItemChanged(clicked_post_position);
                            }

                        } catch (Exception e1) {
                        }

                    }
                }

                @Override
                public void onFailure(Call<JsonElement> call, Throwable t) {
                    prog.hideProg();
                    t.printStackTrace();
                }
            });

        } else {
            snakeBaar.showSnackBar(ac, getString(R.string.no_internet), main_lay);
        }
    }


    public class MyAdapter extends MultiChoiceAdapter<MyAdapter.MyViewHolder> {

        Context mContext;
        List<Like> mList;

        public MyAdapter(List<Like> stringList, Context context) {
            this.mList = stringList;
            this.mContext = context;
        }

        public void updateData(List<Like> data) {
            this.mList = data;
            this.notifyDataSetChanged();
        }

        @NonNull
        @Override
        public MyAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view;
            MyAdapter.MyViewHolder vh;

            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_likes, parent, false);

            vh = new MyAdapter.MyViewHolder(view);
            return vh;
        }

        @Override
        public void onBindViewHolder(MyAdapter.MyViewHolder holder, int position) {
            super.onBindViewHolder(holder, position);

            holder.imageView.setImageResource(icons.getResourceId(mList.get(position).getK(), 0));

        }

        @Override
        public int getItemCount() {
            if (mList != null)
                return mList.size();
            return 0;
        }


        public class MyViewHolder extends RecyclerView.ViewHolder {
            ImageView imageView;


            public MyViewHolder(View itemView) {
                super(itemView);

                imageView = (ImageView) itemView.findViewById(R.id.imageView);

            }
        }


    }


    public class MyAdapterTags extends MultiChoiceAdapter<MyAdapterTags.MyViewHolder> {

        Context mContext;
        List<String> mList;

        public MyAdapterTags(List<String> stringList, Context context) {
            this.mList = stringList;
            this.mContext = context;
        }

        public void updateData(List<String> data) {
            this.mList = data;
            this.notifyDataSetChanged();
        }

        @NonNull
        @Override
        public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view;
            MyViewHolder vh;

            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_tag, parent, false);

            vh = new MyViewHolder(view);
            return vh;
        }

        @Override
        public void onBindViewHolder(final MyViewHolder holder, final int position) {
            super.onBindViewHolder(holder, position);

            holder.textView.setText(mList.get(position));

            holder.main_card.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    all_postListt.clear();
                    all_post_adapter.notifyDataSetChanged();
                    selected_tag = mList.get(position);

                    form_one_title.setText(selected_tag);

                    getContents(selected_tag, getChanelsResponse.getChannels().get(channel_pos).getN(), true, numResult, 0);


                    for (int i = 0; i < checkList.size(); i++) {
                        checkList.set(i, false);

                    }
                    filterAdapter.notifyDataSetChanged();

                }
            });


        }

        @Override
        public int getItemCount() {
            if (mList != null)
                return mList.size();
            return 0;
        }


        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView textView;
            RelativeLayout main_card;
            RelativeLayout bg_lay;


            public MyViewHolder(View itemView) {
                super(itemView);

                textView = (TextView) itemView.findViewById(R.id.textView);
                main_card = (RelativeLayout) itemView.findViewById(R.id.main_card);
                bg_lay = (RelativeLayout) itemView.findViewById(R.id.bg_lay);

//                textView.setTextColor(getResources().getColor(R.color.colorTags));
                textView.setPaintFlags(textView.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

            }
        }


    }


    public class FilterAdapter extends RecyclerView.Adapter<FilterAdapter.MyViewHolder> {


        Context con;


        FilterAdapter(Context c) {
            con = c;
        }


        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


            View view;
            MyViewHolder vh;

            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_filter, parent, false);

            vh = new MyViewHolder(view);
            return vh;


        }

        @Override
        public void onBindViewHolder(final MyViewHolder holder, final int position) {


            MyViewHolder viewHolder = (MyViewHolder) holder;


            if (checkList.get(position)) {
                holder.textView.setTextColor(getResources().getColor(R.color.colorPrimary));
                GenericDraweeHierarchy hierarchy = holder.profile_image.getHierarchy();  // get SettableDraweeHierarchy
                hierarchy.setOverlayImage(ContextCompat.getDrawable(ac, R.drawable.outline));

            } else {
                holder.textView.setTextColor(getResources().getColor(R.color.colorBlack));

                GenericDraweeHierarchy hierarchy = holder.profile_image.getHierarchy();  // get SettableDraweeHierarchy
                hierarchy.setOverlayImage(ContextCompat.getDrawable(ac, R.drawable.outline1));


            }
            holder.textView.setText(getChanelsResponse.getChannels().get(position).getN());


            String chnlImage = getChanelsResponse.getChannels().get(position).getI();
            if (chnlImage != null) {
                holder.profile_image.setImageURI(getChanelsResponse.getChannels().get(position).getI());

            } else {
                holder.profile_image.setActualImageResource(R.drawable.usericon);
            }


            ((MyViewHolder) holder).itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    // Tracking Event
                    MainApplication.getInstance().trackEvent("Post", "Filter Chenged", "Filter Changed to " + getChanelsResponse.getChannels().get(position), tinyDb.getString("ID"));

                    selected_tag = "";
                    form_one_title.setText(getChanelsResponse.getChannels().get(position).getN());
                    holder.textView.setTextColor(getResources().getColor(R.color.colorPrimary));
                    for (int i = 0; i < checkList.size(); i++) {
                        checkList.set(i, false);
                    }
                    checkList.set(position, true);

                    try {
                        all_postListt.clear();
                        all_post_adapter.notifyDataSetChanged();

                        channel_pos = position;

                        getContents("", getChanelsResponse.getChannels().get(channel_pos).getN(), false, numResult, 0);

                        filterAdapter.notifyDataSetChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                }
            });


        }

        @Override
        public int getItemCount() {

            return getChanelsResponse.getChannels().size();
        }


        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView textView;
            RelativeLayout main_lay;
            SimpleDraweeView profile_image;


            public MyViewHolder(View itemView) {
                super(itemView);

                main_lay = (RelativeLayout) itemView.findViewById(R.id.main_lay);
                textView = (TextView) itemView.findViewById(R.id.textView);
                profile_image = (SimpleDraweeView) itemView.findViewById(R.id.profile_image);


            }
        }


    }


    private void postFollow(String leaderId, String follow) {

        if (networkChecking.isNetworkAvailable(ac)) {
            prog.progDialog(ac);


            RequestBody user_id_part = RequestBody.create(MediaType.parse("multipart/form-data"), tinyDb.getString("ID"));
            RequestBody password_part = RequestBody.create(MediaType.parse("multipart/form-data"), tinyDb.getString("PASSWORD"));
            RequestBody leaderId_part = RequestBody.create(MediaType.parse("multipart/form-data"), leaderId);
            RequestBody follow_part = RequestBody.create(MediaType.parse("multipart/form-data"), follow);

            MainApplication.getApiService().addFollow("/api/user/follow/" + tinyDb.getString("ID"), leaderId_part, user_id_part, follow_part, password_part).enqueue(new Callback<JsonElement>() {
                @Override
                public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                    prog.hideProg();
                    if (response.isSuccessful()) {

                        try {
                            JSONObject jsonObj = new JSONObject(String.valueOf(response.body()));
                            String responceCode = jsonObj.getString("rc");
                            if (responceCode.equals("1")) {
                                snakeBaar.showSnackBar(ac, getString(R.string.add_follow), main_lay);
                                for (int i = 0; i < all_postListt.size(); i++) {
                                    if (all_postListt.get(clicked_post_position).getUser().getId().equals(all_postListt.get(i).getUser().getId())) {
                                        all_postListt.get(i).getUser().setFollow(true);
                                    }

                                }

                                all_post_adapter.notifyDataSetChanged();
                            }

                        } catch (Exception e1) {
                        }

                    }
                }

                @Override
                public void onFailure(Call<JsonElement> call, Throwable t) {
                    prog.hideProg();
                    t.printStackTrace();
                }
            });

        } else {
            snakeBaar.showSnackBar(ac, getString(R.string.no_internet), main_lay);
        }
    }

    public static String getPathForAudio(Context context, Uri uri) {
        String result = null;
        Cursor cursor = null;

        try {
            String[] proj = {MediaStore.Audio.Media.DATA};
            cursor = context.getContentResolver().query(uri, proj, null, null, null);
            if (cursor == null) {
                result = uri.getPath();
            } else {
                cursor.moveToFirst();
                int column_index = cursor.getColumnIndex(MediaStore.Audio.AudioColumns.DATA);
                result = cursor.getString(column_index);
                cursor.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return result;
    }

    public static String getRealFilePath(final Context context, final Uri uri) {
        if (null == uri) return null;
        final String scheme = uri.getScheme();
        String data = null;
        if (scheme == null)
            data = uri.getPath();
        else if (ContentResolver.SCHEME_FILE.equals(scheme)) {
            data = uri.getPath();
        } else if
                (ContentResolver.SCHEME_CONTENT.equals(scheme)) {

            Cursor cursor = context.getContentResolver().query(uri, new String[]{MediaStore.Audio.AudioColumns.DATA}, null, null, null);
            if (null != cursor) {
                if (cursor.moveToFirst()) {

                    int index = cursor.getColumnIndex(MediaStore.Audio.AudioColumns.DATA);
                    if (index > -1) {
                        data = cursor.getString(index);
                    }
                }
                cursor.close();
            }
        }
        return data;
    }


    public void tagClick(List<String> mList, int position) {
        all_postListt.clear();
        all_post_adapter.notifyDataSetChanged();
        selected_tag = mList.get(position);

        form_one_title.setText(selected_tag);

        getContents(selected_tag, getChanelsResponse.getChannels().get(channel_pos).getN(), true, numResult, 0);


        for (int i = 0; i < checkList.size(); i++) {
            checkList.set(i, false);

        }
        filterAdapter.notifyDataSetChanged();
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    private void selectVideo() {
        final CharSequence[] items = {getString(R.string.take_video), getString(R.string.choose_frm_gallery),
                getString(R.string.cancel)};

        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(ac);
        builder.setTitle(R.string.add_video);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {

                tinyDb.putString("MideaType", "Video");

                if (items[item].equals(getString(R.string.take_video))) {


                    videoIntent();

                } else if (items[item].equals(getString(R.string.choose_frm_gallery))) {


                    Intent intent = new Intent(ac, Gallery.class);
                    // Set the title
                    intent.putExtra("title", getString(R.string.select_video));
                    // Mode 1 for both images and videos selection, 2 for images only and 3 for videos!
                    intent.putExtra("mode", 3);
                    intent.putExtra("maxSelection", 1); // Optional
                    startActivityForResult(intent, OPEN_MEDIA_PICKER);

                } else if (items[item].equals(getString(R.string.cancel))) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void selectImage() {
        final CharSequence[] items = {getString(R.string.take_photo), getString(R.string.choose_frm_gallery),
                getString(R.string.cancel)};

        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(ac);
        builder.setTitle(R.string.add_photo);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {

                tinyDb.putString("MideaType", "Image");

                if (items[item].equals(getString(R.string.take_photo))) {


                    cameraIntent();

                } else if (items[item].equals(getString(R.string.choose_frm_gallery))) {


                    Intent intent = new Intent(ac, Gallery.class);
                    // Set the title
                    intent.putExtra("title", getString(R.string.select_images));
                    // Mode 1 for both images and videos selection, 2 for images only and 3 for videos!
                    intent.putExtra("mode", 2);
                    intent.putExtra("maxSelection", 8); // Optional
                    startActivityForResult(intent, OPEN_MEDIA_PICKER);

                } else if (items[item].equals(getString(R.string.cancel))) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    @SuppressLint("RestrictedApi")
    private void cameraIntent() {
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        mTmpFile = getTempFile(ac);
        Uri uri = Uri.fromFile(mTmpFile);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
        startActivityForResult(intent, REQUEST_CAMERA_IMAGE);
    }

    private File getTempFile(Context context) {
        //it will return /sdcard/image.tmp
        final File path = new File(Environment.getExternalStorageDirectory(), context.getPackageName());
        if (!path.exists()) {
            path.mkdir();
        }
        return new File(path, "image.tmp");
    }

    private void videoIntent() {

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        if (takeVideoIntent.resolveActivity(ac.getPackageManager()) != null) {
            mTmpFile = getTempFile(ac);
            Uri uri = Uri.fromFile(mTmpFile);
            takeVideoIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
            startActivityForResult(takeVideoIntent, REQUEST_CAMERA_VIDEO);
        }
    }


    @Override
    public void onPause() {


        super.onPause();
    }
}



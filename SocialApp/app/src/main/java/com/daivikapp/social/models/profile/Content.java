
package com.daivikapp.social.models.profile;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import com.daivikapp.social.models.Videoimage;

public class Content {

    @SerializedName("style")
    @Expose
    private int style;
    @SerializedName("images")
    @Expose
    private List<String> images = null;
    @SerializedName("videos")
    @Expose
    private List<String> videos = null;
    @SerializedName("audios")
    @Expose
    private List<String> audios = null;
    @SerializedName("gifs")
    @Expose
    private List<String> gifs = null;
    @SerializedName("numcomm")
    @Expose
    private int numcomm;
    @SerializedName("numlikes")
    @Expose
    private int numlikes;
    @SerializedName("numshare")
    @Expose
    private int numshare;
    @SerializedName("numviews")
    @Expose
    private int numviews;
    @SerializedName("tags")
    @Expose
    private List<String> tags = null;
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("desc")
    @Expose
    private String desc;
    @SerializedName("lang")
    @Expose
    private String lang;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("loc")
    @Expose
    private String loc;
    @SerializedName("videoimages")
    @Expose
    private List<Videoimage> videoimages = null;
    @SerializedName("postdate")
    @Expose
    private String postdate;
    @SerializedName("user")
    @Expose
    private String user;
    @SerializedName("score")
    @Expose
    private double score;
    @SerializedName("__v")
    @Expose
    private int v;
    @SerializedName("displayTime")
    @Expose
    private String displayTime;

    @SerializedName("likes")
    @Expose
    private List<Like> likes = null;

    public List<Like> getLikes() {
        return likes;
    }

    public void setLikes(List<Like> likes) {
        this.likes = likes;
    }

    public int getStyle() {
        return style;
    }

    public void setStyle(int style) {
        this.style = style;
    }

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }

    public List<String> getVideos() {
        return videos;
    }

    public void setVideos(List<String> videos) {
        this.videos = videos;
    }

    public List<String> getAudios() {
        return audios;
    }

    public void setAudios(List<String> audios) {
        this.audios = audios;
    }

    public List<String> getGifs() {
        return gifs;
    }

    public void setGifs(List<String> gifs) {
        this.gifs = gifs;
    }

    public int getNumcomm() {
        return numcomm;
    }

    public void setNumcomm(int numcomm) {
        this.numcomm = numcomm;
    }

    public int getNumlikes() {
        return numlikes;
    }

    public void setNumlikes(int numlikes) {
        this.numlikes = numlikes;
    }

    public int getNumshare() {
        return numshare;
    }

    public void setNumshare(int numshare) {
        this.numshare = numshare;
    }

    public int getNumviews() {
        return numviews;
    }

    public void setNumviews(int numviews) {
        this.numviews = numviews;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLoc() {
        return loc;
    }

    public void setLoc(String loc) {
        this.loc = loc;
    }

    public List<Videoimage> getVideoimages() {
        return videoimages;
    }

    public void setVideoimages(List<Videoimage> videoimages) {
        this.videoimages = videoimages;
    }

    public String getPostdate() {
        return postdate;
    }

    public void setPostdate(String postdate) {
        this.postdate = postdate;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }

    public int getV() {
        return v;
    }

    public void setV(int v) {
        this.v = v;
    }

    public String getDisplayTime() {
        return displayTime;
    }

    public void setDisplayTime(String displayTime) {
        this.displayTime = displayTime;
    }

}


package com.daivikapp.social.models;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FollowingListResponce {

    @SerializedName("rc")
    @Expose
    private int rc;
    @SerializedName("llist")
    @Expose
    private List<Llist> llist = null;

    public int getRc() {
        return rc;
    }

    public void setRc(int rc) {
        this.rc = rc;
    }

    public List<Llist> getLlist() {
        return llist;
    }

    public void setLlist(List<Llist> llist) {
        this.llist = llist;
    }

}

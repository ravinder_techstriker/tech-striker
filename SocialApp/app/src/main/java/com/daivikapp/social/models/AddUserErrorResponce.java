
package com.daivikapp.social.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AddUserErrorResponce {

    @SerializedName("rc")
    @Expose
    private int rc;
    @SerializedName("error")
    @Expose
    private String error;

    public int getRc() {
        return rc;
    }

    public void setRc(int rc) {
        this.rc = rc;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

}

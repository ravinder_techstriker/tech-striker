package com.daivikapp.social.activities;

        import android.app.Activity;
        import android.app.Dialog;
        import android.app.Fragment;
        import android.content.Context;
        import android.content.Intent;
        import android.content.res.Resources;
        import android.content.res.TypedArray;
        import android.graphics.Paint;
        import android.graphics.PorterDuff;
        import android.net.Uri;
        import android.os.Build;
        import android.os.Bundle;
        import android.os.Environment;
        import android.support.annotation.NonNull;
        import android.support.annotation.Nullable;
        import android.support.annotation.RequiresApi;
        import android.support.design.widget.BottomSheetBehavior;
        import android.support.v4.widget.NestedScrollView;
        import android.support.v7.widget.CardView;
        import android.support.v7.widget.GridLayoutManager;
        import android.support.v7.widget.LinearLayoutManager;
        import android.support.v7.widget.RecyclerView;
        import android.util.DisplayMetrics;
        import android.view.Gravity;
        import android.view.LayoutInflater;
        import android.view.View;
        import android.view.ViewGroup;
        import android.view.inputmethod.InputMethodManager;
        import android.widget.Button;
        import android.widget.EditText;
        import android.widget.ImageView;
        import android.widget.LinearLayout;
        import android.widget.RelativeLayout;
        import android.widget.TextView;
        import android.widget.Toast;

        import com.borjabravo.readmoretextview.ReadMoreTextView;
        import com.bumptech.glide.Glide;
        import com.bumptech.glide.request.target.Target;
        import com.davidecirillo.multichoicerecyclerview.MultiChoiceAdapter;
        import com.facebook.drawee.backends.pipeline.Fresco;
        import com.facebook.drawee.backends.pipeline.PipelineDraweeController;
        import com.facebook.drawee.view.SimpleDraweeView;
        import com.facebook.imagepipeline.common.ResizeOptions;
        import com.facebook.imagepipeline.request.ImageRequest;
        import com.facebook.imagepipeline.request.ImageRequestBuilder;
        import com.github.lzyzsd.circleprogress.CircleProgress;
        import com.google.gson.Gson;
        import com.google.gson.JsonElement;
        import com.jsibbold.zoomage.ZoomageView;
        import com.krishna.fileloader.FileLoader;
        import com.krishna.fileloader.listener.MultiFileDownloadListener;
        import com.krishna.fileloader.request.MultiFileLoadRequest;
        import com.pierfrancescosoffritti.androidyoutubeplayer.player.YouTubePlayer;
        import com.pierfrancescosoffritti.androidyoutubeplayer.player.YouTubePlayerView;
        import com.pierfrancescosoffritti.androidyoutubeplayer.player.listeners.AbstractYouTubePlayerListener;
        import com.pierfrancescosoffritti.androidyoutubeplayer.player.listeners.YouTubePlayerInitListener;
        import com.plattysoft.leonids.ParticleSystem;
        import com.rygelouv.audiosensei.player.AudioSenseiPlayerView;
        import com.daivikapp.social.R;
        import com.daivikapp.social.activities.ContentDetail;
        import com.daivikapp.social.activities.MainActivity;
        import com.daivikapp.social.fragment.AllFeeds;
        import com.daivikapp.social.models.Comment;
        import com.daivikapp.social.models.ContentResponce;
        import com.daivikapp.social.models.Contentlist;
        import com.daivikapp.social.models.Flist;
        import com.daivikapp.social.models.FollowerListResponce;
        import com.daivikapp.social.models.FollowingListResponce;
        import com.daivikapp.social.models.GetCommentResponce;
        import com.daivikapp.social.models.Like;
        import com.daivikapp.social.models.Llist;
        import com.daivikapp.social.models.Option;
        import com.daivikapp.social.models.User;
        import com.daivikapp.social.models.commented_by_user.PostsCommentedByUserResponce;
        import com.daivikapp.social.models.profile.Content;
        import com.daivikapp.social.models.profile.GetProfileResponse;
        import com.daivikapp.social.utilities.EndlessRecyclerViewScrollListener;
        import com.daivikapp.social.utilities.MainApplication;
        import com.daivikapp.social.utilities.NetworkChecking;
        import com.daivikapp.social.utilities.ProgDialog;
        import com.daivikapp.social.utilities.SnakeBaar;
        import com.daivikapp.social.utilities.TinyDB;
        import com.daivikapp.social.utilities.WrapContentDraweeView;

        import org.json.JSONArray;
        import org.json.JSONObject;

        import java.io.File;
        import java.text.SimpleDateFormat;
        import java.util.ArrayList;
        import java.util.Arrays;
        import java.util.Date;
        import java.util.List;

        import cn.jzvd.JZVideoPlayer;
        import cn.jzvd.JZVideoPlayerStandard;
        import okhttp3.MediaType;
        import okhttp3.RequestBody;
        import retrofit2.Call;
        import retrofit2.Callback;
        import retrofit2.Response;

        import static android.view.View.GONE;
        import static com.daivikapp.social.activities.MainActivity.form_one_title;
        import static com.daivikapp.social.activities.MainActivity.lifecycle;
        import static com.daivikapp.social.activities.MainActivity.myAppBar;
        import static com.daivikapp.social.activities.MainActivity.toggle;
        import static com.daivikapp.social.fragment.AllFeeds.contentlistMain;
        import static com.daivikapp.social.utilities.LeftMenu.drawer;
        import static com.daivikapp.social.utilities.MainApplication.getInstance;

public class UserDetailAc  extends Activity implements View.OnClickListener {


    Activity ac;

    int width;
    RelativeLayout main_lay;
    SimpleDraweeView profile_image;
    int clicked_post_position;

    RecyclerView recycle_post, recycle_comment;

    All_Post_Adapter all_post_adapter;
    CommentedOnAdapter commentedOnAdapter;
    ReatedOnAdapter reatedOnAdapter;
    LinearLayoutManager layoutManager, layoutManager1;

    List<Contentlist> all_postListt = new ArrayList();

    private EndlessRecyclerViewScrollListener scrollListener;
    int last_postion = 0;
    boolean checkloading = false;

    String contentId;

    //    public static  Toolbar reg_toolbar;
    NetworkChecking networkChecking;
    ProgDialog prog = new ProgDialog();

    SnakeBaar snakeBaar = new SnakeBaar();

    CommentAdapter commentAdapter;
    FollowerUsersAdapter followerUsersAdapter;
    FollowingUsersAdapter followingUsersAdapter;
    LikeAdapter likeAdapter;

//    TextView post_heading;

    int post_postion = 0;

    List<Comment> all_commentList = new ArrayList();
    List<Option> all_likesList = new ArrayList();
    TypedArray icons;


    List<Flist> allFlist = new ArrayList();
    List<Llist> allLlist= new ArrayList();


    List<String> allOptionLlist= new ArrayList();


    ArrayList<String> pathsList;

    User userContent;

    String responceCode;
    TinyDB tinyDb;

    //    ImageView back;
    Button add_follower;
//    TextView postss, likess, commentss;

    boolean postCheck = false;


    BottomSheetBehavior sheetBehavior;
    CardView layoutBottomSheet;
    Button  send;
    ImageView cancel;
    SimpleDraweeView user_image;
    EditText comment_edt;
    Dialog show1;
    View likeView;
//    public static Contentlist user_contentDetail;

    RelativeLayout donut_progress_layout;
    CircleProgress donut_progress;

    TextView user_name, place,status, followrs, following,  num_comments, num_likes, num_posts;
    String name_str, id_str, image_str;

    GetProfileResponse getProfileResponse = null;
//    GifView centerViewBottom, centerViewTop;

    LinearLayout following_lay, follower_lay, comment_lay, like_lay, post_lay;
    ImageView following_icon, follower_icon, comment_icon, like_icon, post_icon;
    TextView    hed_following, hed_followr, hed_coment, hed_like, hed_post;
    ImageView back, home, videos;

    int numResult = 10;

    String selected_user;

    RecyclerView.RecycledViewPool mLikesPool;
    RecyclerView.RecycledViewPool mTagsPool;
    boolean youtube_check = false;
    NestedScrollView scrollview;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.fragment_userdetail );
   
        ac = this;

        mLikesPool = new RecyclerView.RecycledViewPool();
        mTagsPool = new RecyclerView.RecycledViewPool();

        tinyDb = new TinyDB( ac );



        // Tracking the screen view
        getInstance().trackScreenView("User Detail", tinyDb.getString("ID"));

//        form_one_title.setText(R.string.user_detail);
//        toolbar.setVisibility(View.GONE);

        myAppBar.setExpanded(true,false);


        networkChecking = new NetworkChecking();
        prog = new ProgDialog();
        snakeBaar = new SnakeBaar();

        selected_user =tinyDb.getString("ClIK_USER_ID");
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ac.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        width = displayMetrics.widthPixels;


        user_name = (TextView)findViewById(R.id.user_name);

//        post_heading = (TextView)findViewById(R.id.post_heading);



        status = (TextView)findViewById(R.id.status);
        place = (TextView)findViewById(R.id.place);
        followrs = (TextView)findViewById(R.id.followrs);
        following = (TextView)findViewById(R.id.following);
        num_comments = (TextView)findViewById(R.id.num_comments);
        num_likes = (TextView)findViewById(R.id.num_likes);
        num_posts = (TextView)findViewById(R.id.num_posts);




        hed_following = (TextView)findViewById(R.id.hed_following);
        hed_followr = (TextView)findViewById(R.id.hed_followr);
        hed_coment = (TextView)findViewById(R.id.hed_coment);
        hed_like = (TextView)findViewById(R.id.hed_like);
        hed_post = (TextView)findViewById(R.id.hed_post);


        back = (ImageView) findViewById(R.id.back);
        home = (ImageView) findViewById(R.id.home);
        videos = (ImageView) findViewById(R.id.videos);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                android.app.FragmentManager notifications ;
                android.app.FragmentTransaction fragmentTransaction2;

                tinyDb.putString("TYPE", "POPULAR");
                Intent mainIntent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(mainIntent);
                overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
            }
        });

        videos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                android.app.FragmentManager notifications ;
                android.app.FragmentTransaction fragmentTransaction2;

                tinyDb.putString("TYPE", "VID");
                Intent mainIntent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(mainIntent);
                overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
            }
        });




//        centerViewBottom = (GifView) findViewById(R.id.centerViewBottom);
//        centerViewTop = (GifView) findViewById(R.id.centerViewTop);




        all_likesList = new ArrayList<>();


        List<String> options  = Arrays.asList(getResources().getStringArray(R.array.reaction_name_list));

        Resources r = ac.getResources();
        icons = r.obtainTypedArray(R.array.reaction_images_list);


        for (int i = 0; i < options.size(); i++) {
            Option option = new Option();

            option.setName(options.get(i));
            all_likesList.add(option);


        }



        layoutBottomSheet = (CardView)findViewById(R.id.bottom_sheet);
        recycle_comment = (RecyclerView) findViewById(R.id.recycle_comment);
        recycle_comment.setNestedScrollingEnabled(false);
//        recycle_comment.addItemDecoration(new DividerItemDecoration(recycle_comment.getContext(), DividerItemDecoration.VERTICAL));

//        postss = (TextView)findViewById(R.id.postss);
//        likess = (TextView)findViewById(R.id.likess);
//        commentss = (TextView)findViewById(R.id.commentss);

//        back = (ImageView) findViewById(R.id.back);
        add_follower = (Button) findViewById(R.id.add_follower);
        profile_image = (SimpleDraweeView) findViewById(R.id.profile_image);
//        reg_toolbar = (Toolbar) findViewById( R.id.reg_toolbar );


        recycle_post = (RecyclerView) findViewById(R.id.recycle_post);
        recycle_post.setNestedScrollingEnabled(false);
        layoutManager = new LinearLayoutManager( ac, LinearLayoutManager.VERTICAL, false );
        recycle_post.setLayoutManager( layoutManager );


//        try {
//            recycle_post.setNestedScrollingEnabled(false);
//            scrollListener = new EndlessRecyclerViewScrollListener(layoutManager) {
//
//                @Override
//                public void onScrolled(RecyclerView view, int dx, int dy) {
//                    super.onScrolled(view, dx, dy);
//
//                }
//
//                @Override
//                public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
//
//                    if (postCheck) {
//                        last_postion = layoutManager.findLastCompletelyVisibleItemPosition();
//                        //System.out.println("scrollinggggggggg    "+post_postion);
//                        if (!checkloading) {
//                            checkloading = true;
//                            if (post_postion == 1) {
//                                getContentLikedByUser();
//                            }else if (post_postion == 2){
//                                getContentCommentedByUser();
//                            }else if (post_postion == 3){
//                                getFollowerList();
//                            }else if (post_postion == 4){
//                                getFollowingList();
//                            }
//                        }
//
//                    }
//
//                }
//            };
//        } catch (Exception e1) {
//            //System.out.println("Exceptionnnnnnnnnnnnnn   "+e);
//        }
//
//        // Adds the scroll listener to RecyclerView
//        recycle_post.addOnScrollListener(scrollListener);




        try {
            scrollview = (NestedScrollView) findViewById(R.id.scrollview);
            // Adds the scroll listener to RecyclerView
            scrollview.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
                @Override
                public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {

                    if (postCheck) {
                        last_postion = layoutManager.findLastCompletelyVisibleItemPosition();
                        if (!checkloading) {
                            checkloading = true;
                            if (post_postion == 1) {
                                getContentLikedByUser();
                            }else if (post_postion == 2){
                                getContentCommentedByUser();
                            }else if (post_postion == 3){
                                getFollowerList();
                            }else if (post_postion == 4){
                                getFollowingList();
                            }
                        }

                    }
                }

            });
        } catch (NoClassDefFoundError e){ }

        profile_image.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                show1 = new Dialog(ac, R.style.AlertDialogCustom);
                show1.setContentView(R.layout.dialog_user_image);


                if (show1 != null) {
                    int width = ViewGroup.LayoutParams.MATCH_PARENT;
                    int height = ViewGroup.LayoutParams.MATCH_PARENT;
                    show1.getWindow().setLayout(width, height);
                }
                ImageView cancel = (ImageView) show1.findViewById(R.id.cancel);
                ZoomageView myZoomageView = (ZoomageView) show1.findViewById(R.id.myZoomageView);

                Glide.with(ac)
                        .load(getProfileResponse.getUserContent().getUser().getUimage())
                        .override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL)
                        .into(myZoomageView);





                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        show1.dismiss();

                    }
                });

                show1.show();
            }
        } );


        main_lay = (RelativeLayout) findViewById(R.id.main_lay);

        donut_progress_layout = (RelativeLayout) findViewById(R.id.donut_progress_layout);
        donut_progress = (CircleProgress) findViewById(R.id.donut_progress);


        comment_edt = (EditText) findViewById(R.id.comment_edt);
        send = (Button) findViewById(R.id.send);
        cancel = (ImageView) findViewById(R.id.cancel);
        user_image = (SimpleDraweeView) findViewById(R.id.user_image);

        user_image.setImageURI( Uri.parse(tinyDb.getString("IMAGE")) );



        LinearLayoutManager layoutManager1 = new LinearLayoutManager(ac, LinearLayoutManager.VERTICAL, false);

        commentAdapter = new CommentAdapter(ac);
        recycle_comment.setLayoutManager(layoutManager1);


        send.setOnClickListener(this);
        cancel.setOnClickListener(this);

        like_lay = (LinearLayout) findViewById(R.id.like_lay);
        post_lay = (LinearLayout) findViewById(R.id.post_lay);
        comment_lay = (LinearLayout) findViewById(R.id.comment_lay);
        follower_lay = (LinearLayout) findViewById(R.id.follower_lay);
        following_lay = (LinearLayout) findViewById(R.id.following_lay);


        following_icon= (ImageView) findViewById(R.id.following_icon);
        follower_icon= (ImageView) findViewById(R.id.follower_icon);
        comment_icon= (ImageView) findViewById(R.id.comment_icon);
        like_icon= (ImageView) findViewById(R.id.like_icon);
        post_icon= (ImageView) findViewById(R.id.post_icon);



        like_lay.setOnClickListener(this);
        post_lay.setOnClickListener(this);
        comment_lay.setOnClickListener(this);
        follower_lay.setOnClickListener(this);
        following_lay.setOnClickListener(this);



        sheetBehavior = BottomSheetBehavior.from(layoutBottomSheet);

        sheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                switch (newState) {
                    case BottomSheetBehavior.STATE_HIDDEN:
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED: {
//                        btnBottomSheet.setText("Close Sheet");
                    }
                    break;
                    case BottomSheetBehavior.STATE_COLLAPSED: {
//                        btnBottomSheet.setText("Expand Sheet");
                    }
                    break;
                    case BottomSheetBehavior.STATE_DRAGGING:
                        break;
                    case BottomSheetBehavior.STATE_SETTLING:
                        break;
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });



//        back.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//
//
//                    for (int i = 0; i < all_postListt.size(); i++) {
//                        if (all_postListt.get(i).getType().equals("aud") ) {
//                            try {
//                            AudioSenseiPlayerView player=  layoutManager.findViewByPosition(i).findViewById(R.id.audio_player);
//                            player.stop();
//                            } catch (Exception e) {}
//                        }
//                    }
//
//
//                toolbar.setVisibility(View.VISIBLE);
//                getFragmentManager().popBackStack();
//            }
//        });
        add_follower.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    if (getProfileResponse.getUserContent().getUser().isFollow()) {
                        postFollow(id_str, "0");
                    }else{
                        postFollow(id_str, "1");
                    }
                } catch (Exception e) {
                    postFollow(id_str, "1");
                }

            }
        });


        allOptionLlist.add(getResources().getString(R.string.posts));
        allOptionLlist.add(getResources().getString(R.string.like));
        allOptionLlist.add(getResources().getString(R.string.share));
        allOptionLlist.add(getResources().getString(R.string.comment));
        allOptionLlist.add(getResources().getString(R.string.followers));
        allOptionLlist.add(getResources().getString(R.string.following));


        getProfile();


    }

    @Override
    public void onBackPressed() {

                JZVideoPlayer.releaseAllVideos();


        for (int i = 0; i < all_postListt.size(); i++) {
            if (all_postListt.get(i).getType().equals("aud") ) {
                try {
                    AudioSenseiPlayerView player=  layoutManager.findViewByPosition(i).findViewById(R.id.audio_player);
                    player.stop();
                } catch (Exception e) {}
            }
        }

//        toolbar.setVisibility(View.VISIBLE);
        if(prog != null)
            prog.destroy();

        super.onBackPressed();
    }

//    @Override
//    public void onDetach() {
//        JZVideoPlayer.releaseAllVideos();
//
//
//        for (int i = 0; i < all_postListt.size(); i++) {
//            if (all_postListt.get(i).getType().equals("aud") ) {
//                try {
//                    AudioSenseiPlayerView player=  layoutManager.findViewByPosition(i).findViewById(R.id.audio_player);
//                    player.stop();
//                } catch (Exception e) {}
//            }
//        }
//
////        toolbar.setVisibility(View.VISIBLE);
//        if(prog != null)
//            prog.destroy();
//
//        super.onDetach();
//
//
//    }


    @Override
    public void onClick(View view) {
        if (view == cancel) {
            if (sheetBehavior.getState() != BottomSheetBehavior.STATE_EXPANDED) {
                sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            } else {
                sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            }
        }else if(view == post_lay){
            post_postion = 0;
            postCheck = false;
//            post_heading.setText(ac.getResources().getString(R.string.posts));



            all_postListt.clear();
            all_post_adapter.notifyDataSetChanged();
            checkloading = false;
            getProfile();


        }else if(view == like_lay){




            post_postion = 1;
            postCheck = true;
            checkloading = false;
//            post_heading.setText(ac.getResources().getString(R.string.like));
            all_postListt.clear();
//            reatedOnAdapter.notifyDataSetChanged();
            getContentLikedByUser();


        }else if(view == comment_lay){




            post_postion = 2;

            postCheck = true;
//            post_heading.setText(ac.getResources().getString(R.string.comment));
            checkloading = false;
            all_postListt.clear();
            recycle_post.invalidate();
            getContentCommentedByUser();


        }else if(view == follower_lay) {



            post_postion = 3;

            postCheck = true;
//            post_heading.setText(ac.getResources().getString(R.string.followers));
            all_postListt.clear();
            all_post_adapter.notifyDataSetChanged();
            checkloading = false;
            getFollowerList();


        }else if(view == following_lay) {



            post_postion = 4;

            postCheck = true;
//            post_heading.setText(ac.getResources().getString(R.string.following));
            all_postListt.clear();
            all_post_adapter.notifyDataSetChanged();
            checkloading = false;
            getFollowingList();


        }else if (view == send){
            if (!comment_edt.getText().toString().isEmpty()) {

                Date today = new Date();
                SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss a");
                String dateToStr = format.format(today);
//                //System.out.println(dateToStr);

                String[] array = dateToStr.split(" ");
                postCommentToServer(comment_edt.getText().toString(),contentId, clicked_post_position);


                comment_edt.setText("");


                if (sheetBehavior.getState() != BottomSheetBehavior.STATE_EXPANDED) {
                    sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                } else {
                    sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                }

                InputMethodManager imm = (InputMethodManager) ac.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

                new ParticleSystem(ac, 20, R.drawable.comment01, 5000)
                        .setSpeedRange(0.1f, 0.25f)
                        .setRotationSpeedRange(10, 20)
                        .setInitialRotationRange(0, 360)
                        .oneShot(likeView, 10);




            }else {
                Toast.makeText(ac, "Please add comment", Toast.LENGTH_SHORT).show();
            }
        }

    }





    private void getProfile() {



        if (networkChecking.isNetworkAvailable( ac )) {

            prog.progDialog(ac);

            RequestBody user_id_part = RequestBody.create(MediaType.parse("multipart/form-data"), tinyDb.getString("ID"));
            RequestBody id_part = RequestBody.create(MediaType.parse("multipart/form-data"), selected_user);

            MainApplication.getApiService().getUserProfile("user/profile/"+tinyDb.getString("ID"), id_part, user_id_part).enqueue(new Callback<JsonElement>() {
                @Override
                public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                    prog.hideProg();
                    if (response.isSuccessful()) {


                        Gson gson = new Gson();

                        Content content = null;


                        try {
                            JSONObject jsonObj = new JSONObject(String.valueOf(response.body()));
                            responceCode =  jsonObj.getString("rc");

                        }catch (Exception e1){}

                        if (responceCode.equals("1")) {

                            like_icon.setColorFilter(getResources().getColor(R.color.colorBlack), PorterDuff.Mode.SRC_IN);
                            following_icon.setColorFilter(getResources().getColor(R.color.colorBlack), PorterDuff.Mode.SRC_IN);
                            follower_icon.setColorFilter(getResources().getColor(R.color.colorBlack), PorterDuff.Mode.SRC_IN);
                            comment_icon.setColorFilter(getResources().getColor(R.color.colorBlack), PorterDuff.Mode.SRC_IN);
                            post_icon.setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_IN);

                            followrs.setTextColor(getResources().getColor(R.color.colorBlack));
                            following.setTextColor(getResources().getColor(R.color.colorBlack));
                            num_comments.setTextColor(getResources().getColor(R.color.colorBlack));
                            num_likes.setTextColor(getResources().getColor(R.color.colorBlack));
                            num_posts.setTextColor(getResources().getColor(R.color.colorPrimary));

                            hed_following.setTextColor(getResources().getColor(R.color.colorBlack));
                            hed_followr.setTextColor(getResources().getColor(R.color.colorBlack));
                            hed_coment.setTextColor(getResources().getColor(R.color.colorBlack));
                            hed_like.setTextColor(getResources().getColor(R.color.colorBlack));
                            hed_post.setTextColor(getResources().getColor(R.color.colorPrimary));


                            try {
//                                profileResponse= gson.fromJson(String.valueOf(response.body()), GetProfileResponse.class);

                                JSONObject jsonObj = new JSONObject(String.valueOf(response.body()));
                                JSONObject jsonObj1 = jsonObj.getJSONObject("user_content");
                                JSONArray jsonAry = jsonObj1.getJSONArray("content");
                                getProfileResponse= gson.fromJson(String.valueOf(jsonObj), GetProfileResponse.class);
                                for (int i = 0; i < jsonAry.length(); i++) {

                                    content= gson.fromJson(String.valueOf(jsonAry.get(i)), Content.class);
                                    userContent= gson.fromJson(String.valueOf(jsonObj1.getJSONObject("user")), User.class);

                                    Contentlist contentlist = new Contentlist();
                                    contentlist.setAudios(content.getAudios());
                                    contentlist.setComments(null);
                                    contentlist.setDesc(content.getDesc());
                                    contentlist.setDisplayTime(content.getDisplayTime());
                                    contentlist.setGifs(null);
                                    contentlist.setId(content.getId());
                                    contentlist.setImages(content.getImages());
                                    contentlist.setLang(content.getLang());
                                    contentlist.setLikes(null);
                                    contentlist.setNumcomm(content.getNumcomm());
                                    contentlist.setNumlikes(content.getNumlikes());
                                    contentlist.setNumshare(content.getNumshare());
                                    contentlist.setNumviews(content.getNumviews());
                                    contentlist.setPostdate(content.getPostdate());
                                    contentlist.setScore(content.getScore());
                                    contentlist.setStyle(content.getStyle());
                                    contentlist.setTags(content.getTags());
                                    contentlist.setTitle(content.getTitle());
                                    contentlist.setType(content.getType());

                                    contentlist.setUser(userContent);

                                    contentlist.setV(content.getV());
                                    contentlist.setVideoimages(content.getVideoimages());
                                    contentlist.setVideos(content.getVideos());


                                    all_postListt.add(contentlist);
                                }


//                                //System.out.println("all_postListt     "+all_postListt.size());

                                all_post_adapter = new All_Post_Adapter( ac, all_postListt);
                                recycle_post.setAdapter( all_post_adapter );


                                id_str = getProfileResponse.getUserContent().getUser().getId();
                                name_str = getProfileResponse.getUserContent().getUser().getName();
                                image_str = getProfileResponse.getUserContent().getUser().getUimage();

                                user_name.setText(getProfileResponse.getUserContent().getUser().getName());

                                try {
                                    place.setText(getProfileResponse.getUserContent().getUser().getUplace());
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                try {
                                    status.setText(getProfileResponse.getUserContent().getUser().getUstatus());
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

//                                profile_image.setImageURI(getProfileResponse.getUserContent().getUser().getUimage());


                                ImageRequest request = ImageRequestBuilder.newBuilderWithSource(Uri.parse(getProfileResponse.getUserContent().getUser().getUimage()))
                                        .setResizeOptions(new ResizeOptions(50, 50))
                                        .build();
                                profile_image.setController(
                                        Fresco.newDraweeControllerBuilder()
                                                .setOldController(profile_image.getController())
                                                .setImageRequest(request)
                                                .build());


                                followrs.setText(String.valueOf(getProfileResponse.getUserContent().getUser().getNumfollowers()));
                                following.setText(String.valueOf(getProfileResponse.getUserContent().getUser().getNumfollowing()));
                                num_comments.setText(String.valueOf(getProfileResponse.getUserContent().getUser().getNumcomm()));
                                num_likes.setText(String.valueOf(getProfileResponse.getUserContent().getUser().getNumlikes()));
                                num_posts.setText(String.valueOf(getProfileResponse.getUserContent().getUser().getNumcontent()));


                                if (getProfileResponse.getUserContent().getUser().getId().equals(tinyDb.getString("ID"))){
                                    add_follower.setVisibility(View.GONE);
                                }else{
                                    add_follower.setVisibility(View.VISIBLE);
                                }




                                if (getProfileResponse.getUserContent().getUser().isFollow()){
                                    add_follower.setText(getResources().getString(R.string.unfollow));
                                    add_follower.setBackgroundResource(R.drawable.button_active_shape);
                                    add_follower.setTextColor(getResources().getColor(R.color.colorWhite));
                                }else{

                                    add_follower.setText(getResources().getString(R.string.follow));
                                    add_follower.setBackgroundResource(R.drawable.button_in_active_shape);
                                    add_follower.setTextColor(getResources().getColor(R.color.colorPrimary));
                                }





                            } catch (Exception e) {
                                e.printStackTrace();
                            }


                        } else {
                            snakeBaar.showSnackBar( ac, getString(R.string.no_user), main_lay );


                        }
                    }
                }

                @Override
                public void onFailure(Call<JsonElement> call, Throwable t) {
                    prog.hideProg();
                    t.printStackTrace();
                }
            } );


        } else {
            snakeBaar.showSnackBar( ac, getString(R.string.no_internet), main_lay );
        }
    }





    private void getContentLikedByUser() {
        if (networkChecking.isNetworkAvailable( ac )) {
            if (!checkloading) {
                prog.progDialog(ac);
            }

            RequestBody id_part = RequestBody.create(MediaType.parse("multipart/form-data"), selected_user);
            RequestBody lang_part = RequestBody.create(MediaType.parse("multipart/form-data"), tinyDb.getString("LANG"));
            RequestBody num_res_part = RequestBody.create(MediaType.parse("multipart/form-data"), String.valueOf(numResult));
            RequestBody offset_part = RequestBody.create(MediaType.parse("multipart/form-data"), String.valueOf(all_postListt.size()));


            MainApplication.getApiService().getLikedByUser("content/getbyliked/"+tinyDb.getString("ID"), lang_part, offset_part, num_res_part, id_part).enqueue(new Callback<ContentResponce>() {
                @Override
                public void onResponse(Call<ContentResponce> call, Response<ContentResponce> response) {
                    if (!checkloading) {
                        prog.hideProg();
                    }
                    if (response.isSuccessful()) {
                        if (response.body().getRc() == 1) {

                            like_icon.setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_IN);
                            following_icon.setColorFilter(getResources().getColor(R.color.colorBlack), PorterDuff.Mode.SRC_IN);
                            follower_icon.setColorFilter(getResources().getColor(R.color.colorBlack), PorterDuff.Mode.SRC_IN);
                            comment_icon.setColorFilter(getResources().getColor(R.color.colorBlack), PorterDuff.Mode.SRC_IN);
                            post_icon.setColorFilter(getResources().getColor(R.color.colorBlack), PorterDuff.Mode.SRC_IN);

                            followrs.setTextColor(getResources().getColor(R.color.colorBlack));
                            following.setTextColor(getResources().getColor(R.color.colorBlack));
                            num_comments.setTextColor(getResources().getColor(R.color.colorBlack));
                            num_likes.setTextColor(getResources().getColor(R.color.colorPrimary));
                            num_posts.setTextColor(getResources().getColor(R.color.colorBlack));

                            hed_following.setTextColor(getResources().getColor(R.color.colorBlack));
                            hed_followr.setTextColor(getResources().getColor(R.color.colorBlack));
                            hed_coment.setTextColor(getResources().getColor(R.color.colorBlack));
                            hed_like.setTextColor(getResources().getColor(R.color.colorPrimary));
                            hed_post.setTextColor(getResources().getColor(R.color.colorBlack));


                            if (response.body().getContentlist().size() != 0) {

                                if (checkloading){
                                    all_postListt.addAll(response.body().getContentlist());
//                                    //System.out.println("post sizeeeeeeeee   "+all_postListt.size());
                                    reatedOnAdapter.notifyDataSetChanged();
                                    checkloading = false;

                                }else {
                                    all_postListt = response.body().getContentlist();
                                    reatedOnAdapter = new ReatedOnAdapter();
                                    reatedOnAdapter.setHasStableIds(true);
                                    recycle_post.setAdapter(reatedOnAdapter);
                                }

                            }else{
                                snakeBaar.showSnackBar( ac, getString(R.string.no_more_result), main_lay );
                            }

                        } else {
                            snakeBaar.showSnackBar( ac, getString(R.string.no_user), main_lay );
                        }
                    }
                }

                @Override
                public void onFailure(Call<ContentResponce> call, Throwable t) {
                    if (!checkloading) {
                        prog.hideProg();
                    }
                    t.printStackTrace();
                }
            } );


        } else {
            snakeBaar.showSnackBar( ac, getString(R.string.no_internet), main_lay );
        }
    }


    private void getContentCommentedByUser() {
        if (networkChecking.isNetworkAvailable( ac )) {
            if (!checkloading) {
                prog.progDialog(ac);
            }

            RequestBody id_part = RequestBody.create(MediaType.parse("multipart/form-data"), selected_user);
            RequestBody lang_part = RequestBody.create(MediaType.parse("multipart/form-data"), tinyDb.getString("LANG"));
            RequestBody num_res_part = RequestBody.create(MediaType.parse("multipart/form-data"), String.valueOf(numResult));
            RequestBody offset_part = RequestBody.create(MediaType.parse("multipart/form-data"), String.valueOf(all_postListt.size()));


            MainApplication.getApiService().getContentCommentedByUser("content/getbycommented/"+tinyDb.getString("ID"), lang_part, offset_part, num_res_part, id_part).enqueue(new Callback<PostsCommentedByUserResponce>() {
                @Override
                public void onResponse(Call<PostsCommentedByUserResponce> call, Response<PostsCommentedByUserResponce> response) {
                    if (!checkloading) {
                        prog.hideProg();
                    }
                    if (response.isSuccessful()) {
                        if (response.body().getRc() == 1) {

                            like_icon.setColorFilter(getResources().getColor(R.color.colorBlack), PorterDuff.Mode.SRC_IN);
                            following_icon.setColorFilter(getResources().getColor(R.color.colorBlack), PorterDuff.Mode.SRC_IN);
                            follower_icon.setColorFilter(getResources().getColor(R.color.colorBlack), PorterDuff.Mode.SRC_IN);
                            comment_icon.setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_IN);
                            post_icon.setColorFilter(getResources().getColor(R.color.colorBlack), PorterDuff.Mode.SRC_IN);

                            followrs.setTextColor(getResources().getColor(R.color.colorBlack));
                            following.setTextColor(getResources().getColor(R.color.colorBlack));
                            num_comments.setTextColor(getResources().getColor(R.color.colorPrimary));
                            num_likes.setTextColor(getResources().getColor(R.color.colorBlack));
                            num_posts.setTextColor(getResources().getColor(R.color.colorBlack));

                            hed_following.setTextColor(getResources().getColor(R.color.colorBlack));
                            hed_followr.setTextColor(getResources().getColor(R.color.colorBlack));
                            hed_coment.setTextColor(getResources().getColor(R.color.colorPrimary));
                            hed_like.setTextColor(getResources().getColor(R.color.colorBlack));
                            hed_post.setTextColor(getResources().getColor(R.color.colorBlack));


                            if (response.body().getContentlist().size() != 0) {


                                List<Contentlist> contentlists = new ArrayList<>();

                                for (int i = 0; i < response.body().getContentlist().size(); i++) {


                                    Contentlist contentlist = new Contentlist();
                                    contentlist.setVideoimages(response.body().getContentlist().get(i).getVideoimages());
                                    contentlist.setImages(response.body().getContentlist().get(i).getImages());
                                    contentlist.setAudios(response.body().getContentlist().get(i).getAudios());
                                    contentlist.setTitle(response.body().getContentlist().get(i).getTitle());
                                    contentlist.setDisplayTime(response.body().getContentlist().get(i).getDisplayTime());

                                    contentlist.setId(response.body().getContentlist().get(i).getId());
                                    contentlist.setDesc(response.body().getContentlist().get(i).getDesc());
                                    contentlist.setVideos(response.body().getContentlist().get(i).getVideos());
                                    contentlist.setType(response.body().getContentlist().get(i).getType());
                                    contentlist.setPostdate(response.body().getContentlist().get(i).getPostdate());
                                    contentlist.setNumcomm(response.body().getContentlist().get(i).getNumcomm());
                                    contentlist.setNumlikes(response.body().getContentlist().get(i).getNumlikes());
                                    contentlist.setNumshare(response.body().getContentlist().get(i).getNumshare());
                                    User user =new User();
                                    user.setName(name_str);
                                    user.setUimage(image_str);
                                    user.setId(id_str);

                                    contentlist.setUser(user);


                                    List<Comment> commentList = new ArrayList<>();

                                    for (int j = 0; j < response.body().getContentlist().get(i).getComments().size(); j++) {
                                        Comment comment = new Comment();
                                        comment.setComment(response.body().getContentlist().get(i).getComments().get(j).getComment());
                                        comment.setDisplayTime(response.body().getContentlist().get(i).getComments().get(j).getDisplayTime());
                                        User user1 =new User();
                                        user.setId(response.body().getContentlist().get(i).getComments().get(j).getUser());
                                        user1.setName(name_str);
                                        user1.setUimage(image_str);

                                        comment.setUser(user);
                                        commentList.add(comment);
                                    }
                                    contentlist.setComments(commentList);
                                    contentlists.add(contentlist);
                                }


                                if (checkloading){
                                    all_postListt.addAll(contentlists);
                                    commentedOnAdapter.notifyDataSetChanged();
                                    checkloading = false;

                                }else {
                                    all_postListt = contentlists;
                                    commentedOnAdapter = new CommentedOnAdapter();
                                    commentedOnAdapter.setHasStableIds(true);
                                    recycle_post.setAdapter(commentedOnAdapter);
                                }

                            }else{
                                snakeBaar.showSnackBar( ac, getString(R.string.no_more_result), main_lay );
                            }

                        } else {
                            snakeBaar.showSnackBar( ac, getString(R.string.no_user), main_lay );
                        }
                    }
                }

                @Override
                public void onFailure(Call<PostsCommentedByUserResponce> call, Throwable t) {
                    if (!checkloading) {
                        prog.hideProg();
                    }
                    t.printStackTrace();
                }
            } );


        } else {
            snakeBaar.showSnackBar( ac, getString(R.string.no_internet), main_lay );
        }
    }



    private void getFollowerList() {
        if (networkChecking.isNetworkAvailable( ac )) {
            if (!checkloading) {
                prog.progDialog(ac);
            }

            RequestBody login_user_id_part = RequestBody.create(MediaType.parse("multipart/form-data"), tinyDb.getString("ID"));
            RequestBody clicked_user_id_part = RequestBody.create(MediaType.parse("multipart/form-data"), selected_user);
            RequestBody num_res_part = RequestBody.create(MediaType.parse("multipart/form-data"), String.valueOf(numResult));
            RequestBody offset_part = RequestBody.create(MediaType.parse("multipart/form-data"), String.valueOf(all_postListt.size()));


            MainApplication.getApiService().getFollowerList("user/followlist/"+tinyDb.getString("ID"), clicked_user_id_part, offset_part, num_res_part, login_user_id_part).enqueue(new Callback<JsonElement>() {
                @Override
                public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                    if (!checkloading) {
                        prog.hideProg();
                    }
                    if (response.isSuccessful()) {

                        JSONObject jsonObj;
                        try {
                            jsonObj = new JSONObject(String.valueOf(response.body()));
                            responceCode = jsonObj.getString("rc");

                        } catch (Exception e1) {
                            jsonObj = null;
                        }


                        if (responceCode.equals("1")) {


                            like_icon.setColorFilter(getResources().getColor(R.color.colorBlack), PorterDuff.Mode.SRC_IN);
                            following_icon.setColorFilter(getResources().getColor(R.color.colorBlack), PorterDuff.Mode.SRC_IN);
                            follower_icon.setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_IN);
                            comment_icon.setColorFilter(getResources().getColor(R.color.colorBlack), PorterDuff.Mode.SRC_IN);
                            post_icon.setColorFilter(getResources().getColor(R.color.colorBlack), PorterDuff.Mode.SRC_IN);

                            followrs.setTextColor(getResources().getColor(R.color.colorPrimary));
                            following.setTextColor(getResources().getColor(R.color.colorBlack));
                            num_comments.setTextColor(getResources().getColor(R.color.colorBlack));
                            num_likes.setTextColor(getResources().getColor(R.color.colorBlack));
                            num_posts.setTextColor(getResources().getColor(R.color.colorBlack));

                            hed_following.setTextColor(getResources().getColor(R.color.colorBlack));
                            hed_followr.setTextColor(getResources().getColor(R.color.colorPrimary));
                            hed_coment.setTextColor(getResources().getColor(R.color.colorBlack));
                            hed_like.setTextColor(getResources().getColor(R.color.colorBlack));
                            hed_post.setTextColor(getResources().getColor(R.color.colorBlack));


                            Gson gson = new Gson();
                            FollowerListResponce responcee = null;

                            responcee = gson.fromJson(String.valueOf(response.body()), FollowerListResponce.class);
                            allFlist = responcee.getFlist();

                            followerUsersAdapter = new FollowerUsersAdapter();
                            recycle_post.setAdapter(followerUsersAdapter);
                            recycle_post.invalidate();
                            followerUsersAdapter.notifyDataSetChanged();


                        } else {
                            snakeBaar.showSnackBar(ac, "No comment found.", main_lay);
                        }
                    }
                }

                @Override
                public void onFailure(Call<JsonElement> call, Throwable t) {
                    if (!checkloading) {
                        prog.hideProg();
                    }
                    t.printStackTrace();
                }
            } );


        } else {
            snakeBaar.showSnackBar( ac, getString(R.string.no_internet), main_lay );
        }
    }


    private void getFollowingList() {
        if (networkChecking.isNetworkAvailable( ac )) {
            if (!checkloading) {
                prog.progDialog(ac);
            }

            RequestBody login_user_id_part = RequestBody.create(MediaType.parse("multipart/form-data"), tinyDb.getString("ID"));
            RequestBody clicked_user_id_part = RequestBody.create(MediaType.parse("multipart/form-data"), selected_user);
            RequestBody num_res_part = RequestBody.create(MediaType.parse("multipart/form-data"), String.valueOf(numResult));
            RequestBody offset_part = RequestBody.create(MediaType.parse("multipart/form-data"), String.valueOf(all_postListt.size()));


            MainApplication.getApiService().getFollowingList("user/followinglist/"+tinyDb.getString("ID"), clicked_user_id_part, offset_part, num_res_part, login_user_id_part).enqueue(new Callback<JsonElement>() {
                @Override
                public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                    if (!checkloading) {
                        prog.hideProg();
                    }
                    if (response.isSuccessful()) {

                        JSONObject jsonObj;
                        try {
                            jsonObj = new JSONObject(String.valueOf(response.body()));
                            responceCode = jsonObj.getString("rc");

                        } catch (Exception e1) {
                            jsonObj = null;
                        }


                        if (responceCode.equals("1")) {

                            like_icon.setColorFilter(getResources().getColor(R.color.colorBlack), PorterDuff.Mode.SRC_IN);
                            following_icon.setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_IN);
                            follower_icon.setColorFilter(getResources().getColor(R.color.colorBlack), PorterDuff.Mode.SRC_IN);
                            comment_icon.setColorFilter(getResources().getColor(R.color.colorBlack), PorterDuff.Mode.SRC_IN);
                            post_icon.setColorFilter(getResources().getColor(R.color.colorBlack), PorterDuff.Mode.SRC_IN);

                            followrs.setTextColor(getResources().getColor(R.color.colorBlack));
                            following.setTextColor(getResources().getColor(R.color.colorPrimary));
                            num_comments.setTextColor(getResources().getColor(R.color.colorBlack));
                            num_likes.setTextColor(getResources().getColor(R.color.colorBlack));
                            num_posts.setTextColor(getResources().getColor(R.color.colorBlack));

                            hed_following.setTextColor(getResources().getColor(R.color.colorPrimary));
                            hed_followr.setTextColor(getResources().getColor(R.color.colorBlack));
                            hed_coment.setTextColor(getResources().getColor(R.color.colorBlack));
                            hed_like.setTextColor(getResources().getColor(R.color.colorBlack));
                            hed_post.setTextColor(getResources().getColor(R.color.colorBlack));




                            Gson gson = new Gson();
                            FollowingListResponce responcee = null;


                            responcee = gson.fromJson(String.valueOf(response.body()), FollowingListResponce.class);
                            allLlist = responcee.getLlist();

                            followingUsersAdapter = new FollowingUsersAdapter();
                            recycle_post.setAdapter(followingUsersAdapter);
                            recycle_post.invalidate();
                            followingUsersAdapter.notifyDataSetChanged();

                        } else {
                            snakeBaar.showSnackBar(ac, "No comment found.", main_lay);
                        }
                    }
                }

                @Override
                public void onFailure(Call<JsonElement> call, Throwable t) {
                    if (!checkloading) {
                        prog.hideProg();
                    }
                    t.printStackTrace();
                }
            } );


        } else {
            snakeBaar.showSnackBar( ac, getString(R.string.no_internet), main_lay );
        }
    }




    public class MyAdapter extends MultiChoiceAdapter<MyAdapter.MyViewHolder> {

        Context mContext;
        List<Like> mList = new ArrayList<>();
        public MyAdapter(List<Like> stringList, Context context) {
            this.mList = stringList;
            this.mContext = context;
        }
        public void updateData(List<Like> data) {
            this.mList = data;
            this.notifyDataSetChanged();
        }

        @NonNull
        @Override
        public MyAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view;
            MyAdapter.MyViewHolder vh ;

            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_likes, parent, false);

            vh = new MyAdapter.MyViewHolder( view );
            return vh;
        }
        @Override
        public void onBindViewHolder(MyAdapter.MyViewHolder holder, int position) {
            super.onBindViewHolder(holder, position);

            holder.imageView.setImageResource(icons.getResourceId(position, 0));

        }

        @Override
        public int getItemCount() {
            if(mList != null)
                return mList.size();
            return 0;
        }



        public class MyViewHolder extends RecyclerView.ViewHolder {
            ImageView imageView;


            public MyViewHolder(View itemView) {
                super( itemView );

                imageView = (ImageView) itemView.findViewById(R.id.imageView);

            }
        }


    }




    public class MyAdapterTags extends MultiChoiceAdapter<MyAdapterTags.MyViewHolder> {

        Context mContext;
        List<String> mList;
        public MyAdapterTags(List<String> stringList, Context context) {
            this.mList = stringList;
            this.mContext = context;
        }
        public void updateData(List<String> data) {
            this.mList = data;
            this.notifyDataSetChanged();
        }

        @NonNull
        @Override
        public MyAdapterTags.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view;
            MyAdapterTags.MyViewHolder vh ;

            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_tag, parent, false);

            vh = new MyAdapterTags.MyViewHolder( view );
            return vh;
        }
        @Override
        public void onBindViewHolder(final MyAdapterTags.MyViewHolder holder, int position) {
            super.onBindViewHolder(holder, position);

            holder.textView.setText(mList.get(position));

            holder.main_card.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    toolbar.setVisibility(View.VISIBLE);
                    tinyDb.putBoolean("Tag_Selected", true);
                    tinyDb.putString("Selected_Tag", holder.textView.getText().toString());

                    android.app.FragmentManager notifications ;
                    android.app.FragmentTransaction fragmentTransaction2;
                    notifications = ac.getFragmentManager();
                    fragmentTransaction2 = notifications.beginTransaction();
                    fragmentTransaction2.setCustomAnimations(R.animator.slide_up, 0, 0, R.animator.slide_down);
                    AllFeeds center = new AllFeeds();
                    fragmentTransaction2.replace(R.id.fragment_container, android.app.Fragment.instantiate(ac, center.getClass().getName()));
                    fragmentTransaction2.addToBackStack(null);
                    fragmentTransaction2.commit();

                }
            });


        }

        @Override
        public int getItemCount() {
            if(mList != null)
                return mList.size();
            return 0;
        }



        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView textView;
            RelativeLayout main_card;
            RelativeLayout bg_lay;


            public MyViewHolder(View itemView) {
                super( itemView );

                textView = (TextView) itemView.findViewById(R.id.textView);
                main_card = (RelativeLayout) itemView.findViewById(R.id.main_card);
                bg_lay = (RelativeLayout) itemView.findViewById(R.id.bg_lay);

                textView.setTextColor(getResources().getColor(R.color.colorTags));
                textView.setPaintFlags(textView.getPaintFlags()| Paint.UNDERLINE_TEXT_FLAG);

            }
        }


    }






    public class All_Post_Adapter extends RecyclerView.Adapter<All_Post_Adapter.MyViewHolder> {

        Context con;
        List<Contentlist> all_postList = new ArrayList<>();
        private final static int IMAGE_VIEW = 0;
        private final static int TEXT_VIEW = 1;
        private final static int VIDEO_VIEW = 2;
        private final static int YOUTUBE_VIEW = 3;
        private final static int MULTI_IMAGES_VIEW = 4;
        private final static int AUDIO_VIEW = 5;


        All_Post_Adapter(Context c, List<Contentlist> all_postListt) {
            con = c;
            all_postList = all_postListt;
        }



        @Override
        public long getItemId(int position) {
            return position;
        }


        @Override
        public All_Post_Adapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            int layoutRes = 0;
            switch (viewType) {
                case IMAGE_VIEW:
                    layoutRes = R.layout.row_feed_image_view;
                    break;
                case TEXT_VIEW:
                    layoutRes = R.layout.row_feed_text_view;
                    break;
                case VIDEO_VIEW:
                    layoutRes = R.layout.row_feed_video_view;
                    break;
                case YOUTUBE_VIEW:
                    layoutRes = R.layout.row_youtube_view;
                    break;
                case AUDIO_VIEW:
                    layoutRes = R.layout.row_feed_audio_view;
                    break;
            }

            View view = LayoutInflater.from(parent.getContext()).inflate(layoutRes, parent, false);
            return new All_Post_Adapter.MyViewHolder(view, viewType);
        }
        @Override
        public int getItemViewType(int position) {
            if(all_postList.get(position).getType().equals("uvid")){
                return YOUTUBE_VIEW;
            }else if(all_postList.get(position).getType().equals("vid")){
                return VIDEO_VIEW;
            }else if(all_postList.get(position).getType().equals("img")){
                return IMAGE_VIEW;
            }else if(all_postList.get(position).getType().equals("txt")){
                return TEXT_VIEW;
            }else if(all_postList.get(position).getType().equals("aud")){
                return AUDIO_VIEW;
            }else{
                return TEXT_VIEW;
            }
        }


        @Override
        public void onBindViewHolder(final All_Post_Adapter.MyViewHolder holder, final int position) {

            All_Post_Adapter.MyViewHolder viewHolder = (All_Post_Adapter.MyViewHolder) holder;
            holder.mDataPosition = position;

            try {
                String userImage = all_postList.get(position).getUser().getUimage();
                if(userImage != null) {
                    ImageRequest requestU = ImageRequestBuilder.newBuilderWithSource(Uri.parse(all_postList.get(position).getUser().getUimage()))
                            .setResizeOptions(new ResizeOptions(60, 60))
                            .build();
                    PipelineDraweeController controllerU = (PipelineDraweeController) Fresco.newDraweeControllerBuilder()
                            .setOldController(holder.imageView.getController())
                            .setImageRequest(requestU)
                            .build();

                    holder.imageView.setController(controllerU);
                }
                else {
                    holder.imageView.setActualImageResource(R.drawable.usericon);
                }
            } catch (Exception e) {}


            holder.name.setText(all_postList.get(position).getUser().getName());

            if (all_postList.get(position).getUser().isFollow()) {
                holder.add_follower.setVisibility(GONE);
            } else {
                holder.add_follower.setVisibility(View.VISIBLE);
            }

            try {
                holder.heading.setText(all_postList.get(position).getTitle());

                if(holder.description != null)
                    holder.description.setText(all_postList.get(position).getDesc());

                holder.date.setText(all_postList.get(position).getDisplayTime());
                holder.comment.setText(all_postList.get(position).getNumcomm() + " " + ac.getResources().getString(R.string.comment));
                holder.like.setText(all_postList.get(position).getNumlikes() + " " + ac.getResources().getString(R.string.like));
                holder.share.setText(all_postList.get(position).getNumshare() + " " + ac.getResources().getString(R.string.share));
            } catch (Exception e) {
            }

            ((MyAdapter)holder.recycle_rections.getAdapter()).updateData(all_postList.get(position).getLikes());
            ((MyAdapterTags)holder.recycle_tags.getAdapter()).updateData(all_postList.get(position).getTags());

            if (holder.mViewType == YOUTUBE_VIEW) {

                holder.youtube_player_view.getPlayerUIController().showFullscreenButton(false);
                holder.youtube_player_view.getPlayerUIController().showYouTubeButton(false);
                lifecycle.addObserver(holder.youtube_player_view);


                holder.youtube_player_view.initialize(new YouTubePlayerInitListener() {
                    @Override
                    public void onInitSuccess(@NonNull final YouTubePlayer initializedYouTubePlayer) {
                        initializedYouTubePlayer.addListener(new AbstractYouTubePlayerListener() {
                            @Override
                            public void onReady() {

                                try {
                                    holder.youTubePlayer = initializedYouTubePlayer;
                                    holder.youTubePlayer.cueVideo(all_postList.get(holder.mDataPosition).getVideoimages().get(0).getV(), 0);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                            }
                        });
                    }
                }, true);


            } else if (holder.mViewType == AUDIO_VIEW) {

                holder.audio_player.setAudioTarget(all_postList.get(position).getAudios().get(0));


//                AudioSenseiListObserver.getInstance().registerLifecycle(lifecycle);

            } else if (holder.mViewType == IMAGE_VIEW) {

                if (all_postList.get(position).getImages().size() > 3) {

                    holder.image2_lay.setVisibility(View.VISIBLE);
                    holder.image3_lay.setVisibility(View.VISIBLE);
                    holder.plus_icon.setVisibility(View.VISIBLE);
                    holder.play_gif.setVisibility(View.GONE);
                    holder.image1.setClickable(false);

                    holder.image1.setImageURI(all_postList.get(position).getImages().get(0),ac);
                    holder.image2.setImageURI(all_postList.get(position).getImages().get(1),ac);
                    holder.image3.setImageURI(all_postList.get(position).getImages().get(2),ac);

                } else if (all_postList.get(position).getImages().size() == 3) {
                    holder.image2_lay.setVisibility(View.VISIBLE);
                    holder.image3_lay.setVisibility(View.VISIBLE);
                    holder.plus_icon.setVisibility(GONE);
                    holder.play_gif.setVisibility(View.GONE);
                    holder.image1.setClickable(false);

                    holder.image1.setImageURI(all_postList.get(position).getImages().get(0),ac);

                    holder.image2.setImageURI(all_postList.get(position).getImages().get(1),ac);

                    holder.image3.setImageURI(all_postList.get(position).getImages().get(2),ac);

                } else if (all_postList.get(position).getImages().size() == 2) {

                    holder.image2_lay.setVisibility(View.VISIBLE);
                    holder.image3_lay.setVisibility(GONE);
                    holder.plus_icon.setVisibility(GONE);
                    holder.play_gif.setVisibility(View.GONE);
                    holder.image1.setClickable(false);


                    holder.image1.setImageURI(all_postList.get(position).getImages().get(0),ac);

                    holder.image2.setImageURI(all_postList.get(position).getImages().get(1),ac);

                } else if (all_postList.get(position).getImages().size() == 1) {

                    holder.image2_lay.setVisibility(GONE);
                    holder.image3_lay.setVisibility(GONE);
                    holder.plus_icon.setVisibility(GONE);

                    holder.play_gif.setVisibility(View.GONE);


//                    String uri = all_postList.get(position).getImages().get(0);
//                    String extension = uri.substring(uri.lastIndexOf("."));

                    holder.image1.setClickable(false);
                    holder.image1.setImageURI(all_postList.get(position).getImages().get(0), ac,(View)holder.play_gif);

                    /**
                     if (extension.equals(".gif")) {
                     holder.play_gif.setVisibility(View.VISIBLE);
                     holder.image1.setClickable(true);
                     }
                     else {
                     holder.image1.setClickable(false);
                     holder.play_gif.setVisibility(View.GONE);
                     }
                     **/
                }

            } else if (holder.mViewType == VIDEO_VIEW) {

                if (all_postList.get(position).getVideos().size() > 3) {

                    holder.video2_lay.setVisibility(View.VISIBLE);
                    holder.video3_lay.setVisibility(View.VISIBLE);
                    holder.videoplus_icon.setVisibility(View.VISIBLE);

                    holder.video_view3.setUp(all_postList.get(position).getVideos().get(2),
                            JZVideoPlayerStandard.SCREEN_WINDOW_NORMAL,
                            "");
                    Glide.with(ac)
                            .load(all_postList.get(position).getVideoimages().get(2).getI())
                            .into(holder.video_view3.thumbImageView);

                    holder.video_view2.setUp(all_postList.get(position).getVideos().get(1),
                            JZVideoPlayerStandard.SCREEN_WINDOW_NORMAL,
                            "");
                    Glide.with(ac)
                            .load(all_postList.get(position).getVideoimages().get(1).getI())
                            .into(holder.video_view2.thumbImageView);

                    holder.video_view1.setUp(all_postList.get(position).getVideos().get(0),
                            JZVideoPlayerStandard.SCREEN_WINDOW_NORMAL,
                            "");
                    try {
                        Glide.with(ac)
                                .load(all_postList.get(position).getVideoimages().get(0).getI())
                                .into(holder.video_view1.thumbImageView);
                    } catch (Exception e) {}

                    ((All_Post_Adapter.MyViewHolder) holder).videoplus_icon.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            MainActivity.previousfragment = getFragmentManager().findFragmentById(R.id.fragment_container);
                            // Tracking Event
                            MainApplication.getInstance().trackEvent("AllFeeds", "Post Clicked", "Post Clicked to view detail", tinyDb.getString("ID"));

                            clicked_post_position = position;

                            contentlistMain = all_postList.get(position);
                            tinyDb.putInt("CLICKED_POS", clicked_post_position);

                            Intent intent = new Intent( ac, ContentDetail.class );
                            
                            intent.addFlags( Intent.FLAG_ACTIVITY_CLEAR_TOP );
                            startActivity( intent );
                            ac.overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);

                        }
                    });


                } else if (all_postList.get(position).getVideos().size() == 3) {
                    holder.video2_lay.setVisibility(View.VISIBLE);
                    holder.video3_lay.setVisibility(View.VISIBLE);
                    holder.videoplus_icon.setVisibility(View.GONE);

                    holder.video_view3.setUp(all_postList.get(position).getVideos().get(2),
                            JZVideoPlayerStandard.SCREEN_WINDOW_NORMAL,
                            "");
                    Glide.with(ac)
                            .load(all_postList.get(position).getVideoimages().get(2).getI())
                            .into(holder.video_view3.thumbImageView);

                    holder.video_view2.setUp(all_postList.get(position).getVideos().get(1),
                            JZVideoPlayerStandard.SCREEN_WINDOW_NORMAL,
                            "");
                    Glide.with(ac)
                            .load(all_postList.get(position).getVideoimages().get(1).getI())
                            .into(holder.video_view2.thumbImageView);

                    holder.video_view1.setUp(all_postList.get(position).getVideos().get(0),
                            JZVideoPlayerStandard.SCREEN_WINDOW_NORMAL,
                            "");
                    try {
                        Glide.with(ac)
                                .load(all_postList.get(position).getVideoimages().get(0).getI())
                                .into(holder.video_view1.thumbImageView);
                    } catch (Exception e) {
//                        e.printStackTrace();
                    }

                } else if (all_postList.get(position).getVideos().size() == 2) {
                    holder.video2_lay.setVisibility(View.VISIBLE);
                    holder.video3_lay.setVisibility(GONE);
                    holder.videoplus_icon.setVisibility(View.GONE);

                    holder.video_view2.setUp(all_postList.get(position).getVideos().get(1),
                            JZVideoPlayerStandard.SCREEN_WINDOW_NORMAL,
                            "");
                    Glide.with(ac)
                            .load(all_postList.get(position).getVideoimages().get(1).getI())
                            .into(holder.video_view2.thumbImageView);

                    holder.video_view1.setUp(all_postList.get(position).getVideos().get(0),
                            JZVideoPlayerStandard.SCREEN_WINDOW_NORMAL,
                            "");
                    try {
                        Glide.with(ac)
                                .load(all_postList.get(position).getVideoimages().get(0).getI())
                                .into(holder.video_view1.thumbImageView);
                    } catch (Exception e) {
//                        e.printStackTrace();
                    }

                } else if (all_postList.get(position).getVideos().size() == 1) {

                    holder.video2_lay.setVisibility(GONE);
                    holder.videoplus_icon.setVisibility(GONE);


                    holder.video_view1.setUp(all_postList.get(position).getVideos().get(0),
                            JZVideoPlayerStandard.SCREEN_WINDOW_NORMAL,
                            "");

                    try {
                        Glide.with(ac)
                                .load(all_postList.get(position).getVideoimages().get(0).getI())
                                .into(holder.video_view1.thumbImageView);
                    } catch (Exception e) {}
                }

            }
        }


        @Override
        public int getItemCount() {

            return all_postList.size();
        }


        public class MyViewHolder extends RecyclerView.ViewHolder {
            YouTubePlayerView youtube_player_view;
            YouTubePlayer youTubePlayer;

            ReadMoreTextView description;
            TextView name, heading, comment, like, share, date; // init the item view's
            SimpleDraweeView imageView;

            WrapContentDraweeView image1, image2, image3;
            ImageView play_gif;

            ImageView like_img, comment_img;
            RelativeLayout media_lay, top_lay;
            LinearLayout comment_lay, like_lay, share_lay;

            JZVideoPlayerStandard video_view1, video_view2, video_view3;
            RecyclerView recycle_rections, recycle_tags;

            Button add_follower;

            RelativeLayout plus_icon, videoplus_icon;
            RelativeLayout video3_lay, image3_lay;
            LinearLayout video2_lay, image2_lay;


            LinearLayout images_lay;
            RelativeLayout video_lay;

            AudioSenseiPlayerView audio_player;
//            ImageView button_stop;

            int mDataPosition;
            int mViewType;
            boolean mIsSingleGif;


            public MyViewHolder(View itemView, int viewType) {
                super(itemView);

                mIsSingleGif = false;

                mViewType = viewType;



                add_follower = (Button) itemView.findViewById(R.id.add_follower);


                youtube_player_view = (YouTubePlayerView) itemView.findViewById(R.id.youtube_player_view);


                plus_icon = (RelativeLayout) itemView.findViewById(R.id.plus_icon);
                videoplus_icon = (RelativeLayout) itemView.findViewById(R.id.videoplus_icon);

                like_img = (ImageView) itemView.findViewById(R.id.like_img);
                comment_img = (ImageView) itemView.findViewById(R.id.comment_img);

                audio_player = (AudioSenseiPlayerView) itemView.findViewById(R.id.audio_player);

                comment_lay = (LinearLayout) itemView.findViewById(R.id.comment_lay);
                share_lay = (LinearLayout) itemView.findViewById(R.id.share_lay);
                like_lay = (LinearLayout) itemView.findViewById(R.id.like_lay);
                media_lay = (RelativeLayout) itemView.findViewById(R.id.media_lay);
                top_lay = (RelativeLayout) itemView.findViewById(R.id.top_lay);

                recycle_rections = (RecyclerView) itemView.findViewById(R.id.recycle_rections);
                recycle_tags = (RecyclerView) itemView.findViewById(R.id.recycle_tags);

                name = (TextView) itemView.findViewById(R.id.name);
                description = (ReadMoreTextView) itemView.findViewById(R.id.description);
                heading = (TextView) itemView.findViewById(R.id.heading);
                comment = (TextView) itemView.findViewById(R.id.comment);
                like = (TextView) itemView.findViewById(R.id.like);
                share = (TextView) itemView.findViewById(R.id.share);
                date = (TextView) itemView.findViewById(R.id.date);

                imageView = (SimpleDraweeView) itemView.findViewById(R.id.profile_image);


                layoutManager1 = new LinearLayoutManager(ac, LinearLayoutManager.HORIZONTAL, false);
                this.recycle_rections.setLayoutManager(layoutManager1);
                this.recycle_rections.setRecycledViewPool(mLikesPool);
                MyAdapter myAdapter = new MyAdapter(null, ac);
                this.recycle_rections.setAdapter(myAdapter);

                layoutManager1 = new LinearLayoutManager(ac, LinearLayoutManager.HORIZONTAL, false);
                this.recycle_tags.setLayoutManager(layoutManager1);
                this.recycle_tags.setRecycledViewPool(mTagsPool);
                MyAdapterTags myAdapter2 = new MyAdapterTags(null, ac);
                this.recycle_tags.setAdapter(myAdapter2);

//                if(this.mViewType == AUDIO_VIEW) {
//
//                    button_stop = (ImageView) itemView.findViewById(R.id.button_stop);
//                    this.button_stop.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            audio_player.stop();
//                        }
//                    });
//
//
//                }else
                if(this.mViewType == VIDEO_VIEW) {

                    video_view1 = (JZVideoPlayerStandard) itemView.findViewById(R.id.video_view1);
                    video_view2 = (JZVideoPlayerStandard) itemView.findViewById(R.id.video_view2);
                    video_view3 = (JZVideoPlayerStandard) itemView.findViewById(R.id.video_view3);

                    video_lay = (RelativeLayout) itemView.findViewById(R.id.video_lay);
                    video3_lay = (RelativeLayout) itemView.findViewById(R.id.video3_lay);
                    video2_lay = (LinearLayout) itemView.findViewById(R.id.video2_lay);
                }
                else if(this.mViewType == IMAGE_VIEW) {
                    images_lay = (LinearLayout) itemView.findViewById(R.id.images_lay);
                    image3_lay = (RelativeLayout) itemView.findViewById(R.id.image3_lay);
                    image2_lay = (LinearLayout) itemView.findViewById(R.id.image2_lay);
                    image1 = (WrapContentDraweeView) itemView.findViewById(R.id.image1);
                    image2 = (WrapContentDraweeView) itemView.findViewById(R.id.image2);
                    image3 = (WrapContentDraweeView) itemView.findViewById(R.id.image3);
                    play_gif = (ImageView) itemView.findViewById(R.id.play_gif);

                    this.plus_icon.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            MainActivity.previousfragment = getFragmentManager().findFragmentById(R.id.fragment_container);
                            // Tracking Event
                            MainApplication.getInstance().trackEvent("AllFeeds", "Post Clicked", "Post Clicked to view detail", tinyDb.getString("ID"));

                            clicked_post_position = mDataPosition;

                            contentlistMain = all_postList.get(mDataPosition);

                            tinyDb.putInt("CLICKED_POS", clicked_post_position);
                            Intent intent = new Intent( ac, ContentDetail.class );
                            
                            intent.addFlags( Intent.FLAG_ACTIVITY_CLEAR_TOP );
                            startActivity( intent );
                            ac.overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);

                        }
                    });

                    image1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (image1.onClick()) {
                                play_gif.setVisibility(View.GONE);
                            } else {
                                play_gif.setVisibility(View.VISIBLE);
                            }
                        }
                    });
                }

                this.description.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        MainActivity.previousfragment = getFragmentManager().findFragmentById(R.id.fragment_container);

                        // Tracking Event
                        MainApplication.getInstance().trackEvent("AllFeeds", "Post Clicked", "Post Clicked to view detail", tinyDb.getString("ID"));

                        try {
                            if(audio_player != null)
                                audio_player.removeAllViews();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        clicked_post_position = mDataPosition;

                        contentlistMain = all_postList.get(mDataPosition);
                        tinyDb.putInt("CLICKED_POS", clicked_post_position);


                        Intent intent = new Intent( ac, ContentDetail.class );

                        intent.addFlags( Intent.FLAG_ACTIVITY_CLEAR_TOP );
                        startActivity( intent );
                        ac.overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);

                    }
                });


                this.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        MainActivity.previousfragment = getFragmentManager().findFragmentById(R.id.fragment_container);
                        // Tracking Event
                        MainApplication.getInstance().trackEvent("AllFeeds", "Post Clicked", "Post Clicked to view detail", tinyDb.getString("ID"));


                        clicked_post_position = mDataPosition;

                        contentlistMain = all_postList.get(mDataPosition);
                        tinyDb.putInt("CLICKED_POS", clicked_post_position);


                        Intent intent = new Intent( ac, ContentDetail.class );
                        
                        intent.addFlags( Intent.FLAG_ACTIVITY_CLEAR_TOP );
                        startActivity( intent );
                        ac.overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);

                    }
                });




                this.like_lay.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        youtube_check = true;

                        // Tracking Event
                        MainApplication.getInstance().trackEvent("AllFeeds", "Post Liked", "Post liked", tinyDb.getString("ID"));

                        clicked_post_position = mDataPosition;
                        likeView = like_img;
                        // custom dialog
                        show1 = new Dialog(con, R.style.AlertDialogCustom);
                        show1.setContentView(R.layout.dialog_like);
                        show1.setTitle(R.string.select_reaction);


                        RecyclerView recycle_options = (RecyclerView) show1.findViewById(R.id.recycle_options);


                        contentId = all_postList.get(mDataPosition).getId();

                        likeAdapter = new LikeAdapter(ac);
                        recycle_options.setLayoutManager(new GridLayoutManager(ac, 3));
                        recycle_options.setAdapter(likeAdapter);
                        show1.show();

                    }
                });

                this.comment_lay.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        youtube_check = true;
                        // Tracking Event
                        MainApplication.getInstance().trackEvent("AllFeeds", "Post Comment", "Post comment clicked", tinyDb.getString("ID"));


                        clicked_post_position = mDataPosition;
                        likeView = comment_img;
                        contentId = all_postList.get(mDataPosition).getId();
                        getComments(contentId, "0", "500000");


                        if (sheetBehavior.getState() != BottomSheetBehavior.STATE_EXPANDED) {
                            sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                        } else {
                            sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                        }
                        LinearLayoutManager layoutManager = new LinearLayoutManager(ac, LinearLayoutManager.VERTICAL, false);
                        commentAdapter = new CommentAdapter(ac);
                        recycle_comment.setLayoutManager(layoutManager);
                    }
                });

                this.share_lay.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        // Tracking Event
                        MainApplication.getInstance().trackEvent("AllFeeds", "Post Shared", "Post Shared", tinyDb.getString("ID"));


                        clicked_post_position = mDataPosition;

                        final List<MultiFileLoadRequest> uris = new ArrayList<>();

                        if (all_postList.get(mDataPosition).getType().equals("vid")) {
                            donut_progress_layout.setVisibility(View.VISIBLE);
                            donut_progress.setProgress(0);

                            for (int i = 0; i < all_postList.get(mDataPosition).getVideos().size(); i++) {
                                MultiFileLoadRequest multiFileLoadRequest = new MultiFileLoadRequest(all_postList.get(mDataPosition).getVideos().get(i), Environment.DIRECTORY_DOWNLOADS, FileLoader.DIR_EXTERNAL_PUBLIC, true);
                                uris.add(multiFileLoadRequest);
                            }

                            whatsApp(all_postList, uris, mDataPosition);
                        } else if (all_postList.get(mDataPosition).getType().equals("img")) {
                            donut_progress_layout.setVisibility(View.VISIBLE);
                            donut_progress.setProgress(0);
                            for (int i = 0; i < all_postList.get(mDataPosition).getImages().size(); i++) {
                                MultiFileLoadRequest multiFileLoadRequest = new MultiFileLoadRequest(all_postList.get(mDataPosition).getImages().get(i), Environment.DIRECTORY_DOWNLOADS, FileLoader.DIR_EXTERNAL_PUBLIC, true);
                                uris.add(multiFileLoadRequest);
                            }

                            whatsApp(all_postList, uris, mDataPosition);
                        } else if (all_postList.get(mDataPosition).getType().equals("aud")) {
                            donut_progress_layout.setVisibility(View.VISIBLE);
                            donut_progress.setProgress(0);
                            for (int i = 0; i < all_postList.get(mDataPosition).getAudios().size(); i++) {
                                MultiFileLoadRequest multiFileLoadRequest = new MultiFileLoadRequest(all_postList.get(mDataPosition).getAudios().get(i), Environment.DIRECTORY_DOWNLOADS, FileLoader.DIR_EXTERNAL_PUBLIC, true);
                                uris.add(multiFileLoadRequest);
                            }

                            whatsApp(all_postList, uris, mDataPosition);
                        } else if (all_postList.get(mDataPosition).getType().equals("uvid")) {


                            Intent i = new Intent(Intent.ACTION_SEND);
                            i.setType("text/plain");
                            i.setPackage("com.whatsapp");

                            i.putExtra(Intent.EXTRA_TEXT, all_postList.get(mDataPosition).getTags() + "\n" + all_postList.get(mDataPosition).getTitle()  + "\nhttps://www.youtube.com/watch?v=" + all_postList.get(mDataPosition).getVideoimages().get(0).getV() + "\n\n" + getString(R.string.share_link) + " https://play.google.com/store/apps/details?id=com.panagola.app.om");
                            startActivityForResult(i, 4);
                        } else {

                            Intent i = new Intent(Intent.ACTION_SEND);
                            i.setType("text/plain");
                            i.setPackage("com.whatsapp");

                            i.putExtra(Intent.EXTRA_TEXT,  all_postList.get(mDataPosition).getTags() + "\n" + all_postList.get(mDataPosition).getTitle()+ "\n\n" + getString(R.string.share_link) + " https://play.google.com/store/apps/details?id=com.panagola.app.om");
                            startActivityForResult(i, 4);
                        }
                    }
                });

//                this.top_lay.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//
//                        // Tracking Event
//                        MainApplication.getInstance().trackEvent("AllFeeds", "Post User clicked", "Post User clicked to view detail", tinyDb.getString("ID"));
//                        try {
//                            audio_player.stop();
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
//                        tinyDb.putString("ClIK_USER_ID", all_postList.get(mDataPosition).getUser().getId());
//
//                        android.app.FragmentManager notifications;
//                        android.app.FragmentTransaction fragmentTransaction2;
//                        notifications = ac.getFragmentManager();
//                        fragmentTransaction2 = notifications.beginTransaction();
//                        UserDetail center = new UserDetail();
//                        fragmentTransaction2.add(R.id.fragment_container, android.app.Fragment.instantiate(ac, center.getClass().getName()));
//                        fragmentTransaction2.addToBackStack(null);
//                        fragmentTransaction2.commit();
//                        Bungee.swipeLeft(ac);
//                    }
//                });


//                this.add_follower.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        // Tracking Event
//                        MainApplication.getInstance().trackEvent("AllFeeds", "User added to follow", "User added to follow", tinyDb.getString("ID"));
//                        clicked_post_position = mDataPosition;
//                        postFollow(all_postList.get(mDataPosition).getUser().getId(), "1");
//                    }
//                });

            }
        }
    }



    public class LikeAdapter extends RecyclerView.Adapter<LikeAdapter.MyViewHolder> {



        public LikeAdapter(Context c) {}

        @Override
        public LikeAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            // infalte the item Layout
            View v = LayoutInflater.from( parent.getContext() ).inflate( R.layout.row_reaction, parent, false );
            // set the view's size, margins, paddings and layout parameters
            LikeAdapter.MyViewHolder vh = new LikeAdapter.MyViewHolder( v ); // pass the view to View Holder
            return vh;
        }

        @Override
        public void onBindViewHolder(final LikeAdapter.MyViewHolder holder, final int position) {


            LikeAdapter.MyViewHolder viewHolder = (LikeAdapter.MyViewHolder) holder;

            try {

                holder.image.setImageResource(icons.getResourceId(position, 0));
                holder.name.setText(all_likesList.get(position).getName());

            } catch (Exception e) {
                e.printStackTrace();
            }

            ((LikeAdapter.MyViewHolder) holder).itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    show1.dismiss();



                    postLikeToServer(String.valueOf(position), contentId, clicked_post_position);
                    int numLike = all_postListt.get(clicked_post_position).getNumlikes();
                    numLike = numLike+1;
                    all_postListt.get(clicked_post_position).setNumlikes(numLike);

                    Like like = new Like();
                    like.setK(position);
                    like.setId("0");

                    try {
                        all_postListt.get(clicked_post_position).getLikes().add(like);

                        all_post_adapter.notifyItemChanged(clicked_post_position);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


//                    gifView1.pause();
//                    if (position == 0 || position == 1) {
//                        centerViewBottom.setVisibility(View.VISIBLE);
//                        centerViewBottom.play();
//                        centerViewBottom.setGifResource(R.drawable.smoke);
//
//                        final Handler handler=new Handler();
//
//                        final Runnable updateTask=new Runnable() {
//                            @Override
//                            public void run() {
//
//                                centerViewBottom.setVisibility(View.GONE);
//                                centerViewBottom.pause();
//
//                                handler.postDelayed(this,5000);
//                            }
//                        };
//
//                        handler.postDelayed(updateTask,5000);
//
//
//                    } else if (position == 5) {
//                        centerViewBottom.setVisibility(View.VISIBLE);
//                        centerViewBottom.play();
//                        centerViewBottom.setGifResource(R.drawable.deepam);
//
//                        final Handler handler=new Handler();
//
//                        final Runnable updateTask=new Runnable() {
//                            @Override
//                            public void run() {
//
//                                centerViewBottom.setVisibility(View.GONE);
//                                centerViewBottom.pause();
//
//                                handler.postDelayed(this,5000);
//                            }
//                        };
//
//                        handler.postDelayed(updateTask,5000);
//
//
//                    }else if (position == 2) {
//                        centerViewTop.setVisibility(View.VISIBLE);
//                        centerViewTop.play();
//                        centerViewTop.setGifResource(R.drawable.temple_bell);
//
//                        final Handler handler=new Handler();
//
//                        final Runnable updateTask=new Runnable() {
//                            @Override
//                            public void run() {
//
//                                centerViewTop.setVisibility(View.GONE);
//                                centerViewTop.pause();
//
//                                handler.postDelayed(this,5000);
//                            }
//                        };
//
//                        handler.postDelayed(updateTask,5000);
//
//
//                        MediaPlayer mPlayer2;
//                        mPlayer2= MediaPlayer.create(ac, R.raw.bell_test);
//                        mPlayer2.start();
//
//
//                    }else if(position == 3 || position == 4){
//                        centerViewTop.setVisibility(View.VISIBLE);
//                        centerViewTop.play();
//                        centerViewTop.setGifResource(R.drawable.flawers_fall);
//
//                        final Handler handler=new Handler();
//
//                        final Runnable updateTask=new Runnable() {
//                            @Override
//                            public void run() {
//
//                                centerViewTop.setVisibility(View.GONE);
//                                centerViewTop.pause();
//
//                                handler.postDelayed(this,5000);
//                            }
//                        };
//
//                        handler.postDelayed(updateTask,5000);
//
//                    }else if( position == 10 || position == 11){
//                        centerViewTop.setVisibility(View.VISIBLE);
//                        centerViewTop.play();
//                        centerViewTop.setGifResource(R.drawable.rain);
//
//                        final Handler handler=new Handler();
//
//                        final Runnable updateTask=new Runnable() {
//                            @Override
//                            public void run() {
//
//                                centerViewTop.setVisibility(View.GONE);
//                                centerViewTop.pause();
//
//                                handler.postDelayed(this,5000);
//                            }
//                        };
//
//                        handler.postDelayed(updateTask,5000);
//
//                    }else {
                    new ParticleSystem(ac, 20, icons.getResourceId(position, 0), 5000)
                            .setSpeedRange(0.1f, 0.25f)
                            .setRotationSpeedRange(0, 0)
                            .setInitialRotationRange(0, 0)
                            .oneShot(likeView, 10);
//                    }




//                    centerView.getGifResource();
//                    gifView1.setMovieTime(time);
//                    gifView1.getMovie();





//                    new ParticleSystem(ac, 20, all_likesList.get(position).getImage(), 5000)
//                            .setSpeedRange(0.1f, 0.25f)
//                            .setRotationSpeedRange(10, 20)
//                            .setInitialRotationRange(0, 360)
//                            .oneShot(likeView, 10);

//                    mContainer.clearEmojis();
//
//
//
//                    // add emoji sources
//                    mContainer.addEmoji(icons.getResourceId(position, 0));
//
//                    // set emojis per flow, default 6
//                    mContainer.setPer(10);
//
//                    // set total duration in milliseconds, default 8000
//                    mContainer.setDuration(2000);
//
//                    // set average drop duration in milliseconds, default 2400
//                    mContainer.setDropDuration(2400);
//
//                    // set drop frequency in milliseconds, default 500
//                    mContainer.setDropFrequency(500);
//
//                    mContainer.startDropping();
//
//                    ParticleSystem ps = new ParticleSystem(ac, 100, icons.getResourceId(position, 0), 1000);
//                    ps.setScaleRange(0.7f, 1.3f);
//                    ps.setSpeedModuleAndAngleRange(0.07f, 0.16f, 0, 180);
//                    ps.setRotationSpeedRange(0, 0);
//                    ps.setAcceleration(0.00013f, 90);
//                    ps.setFadeOut(500, new AccelerateInterpolator());
//                    ps.emit(centerView, 10, 2000);

                }
            });

        }

        @Override
        public int getItemCount() {
            return all_likesList.size();
        }


        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView name;
            SimpleDraweeView image;

            public MyViewHolder(View itemView) {
                super( itemView );
                // get the reference of item view's
                name = (TextView) itemView.findViewById( R.id.name );;
                image = (SimpleDraweeView) itemView.findViewById( R.id.image );

            }
        }
    }




    public class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.MyViewHolder> {



        public CommentAdapter(Context c) {}

        @Override
        public CommentAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            // infalte the item Layout
            View v = LayoutInflater.from( parent.getContext() ).inflate( R.layout.row_comment, parent, false );
            // set the view's size, margins, paddings and layout parameters
            CommentAdapter.MyViewHolder vh = new CommentAdapter.MyViewHolder( v ); // pass the view to View Holder
            return vh;
        }

        @Override
        public void onBindViewHolder(final CommentAdapter.MyViewHolder holder, final int position) {


            CommentAdapter.MyViewHolder viewHolder = (CommentAdapter.MyViewHolder) holder;

            try {
                holder.name.setText( all_commentList.get( position ).getUser().getName());
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                holder.comment.setText( all_commentList.get( position ).getComment() );
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                holder.date.setText( all_commentList.get( position ).getDisplayTime());
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                holder.profile_image.setImageURI(Uri.parse(all_commentList.get(position).getUser().getUimage()));
            } catch (Exception e) {
                e.printStackTrace();
            }
            ((CommentAdapter.MyViewHolder) holder).itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    tinyDb.putString("ClIK_USER_ID", all_commentList.get( position ).getUser().getId());


                    Intent mainIntent = new Intent(getApplicationContext(), UserDetailAc.class);
                    startActivity(mainIntent);
                    finish();
                    overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);


                }
            });
        }

        @Override
        public int getItemCount() {
            return all_commentList.size();
        }


        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView name, comment, date;
            SimpleDraweeView profile_image;

            public MyViewHolder(View itemView) {
                super( itemView );
                // get the reference of item view's
                name = (TextView) itemView.findViewById( R.id.name );
                comment = (TextView) itemView.findViewById( R.id.comment );
                date = (TextView) itemView.findViewById( R.id.date );
                profile_image = (SimpleDraweeView) itemView.findViewById( R.id.profile_image );

            }
        }
    }




    private void postLikeToServer(final String likePostion, String content_id, final int clicked_post_position) {



        if (networkChecking.isNetworkAvailable( ac )) {
//            prog.progDialog( ac );


            RequestBody user_id_part = RequestBody.create(MediaType.parse("multipart/form-data"), tinyDb.getString("ID"));
            RequestBody password_part = RequestBody.create(MediaType.parse("multipart/form-data"), tinyDb.getString("PASSWORD"));
            RequestBody content_id_part = RequestBody.create(MediaType.parse("multipart/form-data"), content_id);
            RequestBody comment_part = RequestBody.create(MediaType.parse("multipart/form-data"), likePostion);



            MainApplication.getApiService().addLike("content/like/"+tinyDb.getString("ID"),content_id_part, user_id_part, password_part, comment_part).enqueue(new Callback<JsonElement>() {
                @Override
                public void onResponse(Call<JsonElement> call, Response<JsonElement > response) {
//                    prog.hideProg();
                    if (response.isSuccessful()) {

                        try {
                            JSONObject jsonObj = new JSONObject(String.valueOf(response.body()));
                            String responceCode =  jsonObj.getString("rc");
                            if (responceCode.equals("1")){
                                snakeBaar.showSnackBar( ac, getString(R.string.reaction_posted), main_lay );
                                int num_of_comment = all_postListt.get(clicked_post_position).getNumshare();
                                num_of_comment = num_of_comment+1;
                                all_postListt.get(clicked_post_position).setNumshare(num_of_comment);
                                all_post_adapter.notifyItemChanged(clicked_post_position);
                            }

                        }catch (Exception e1){}

                    }
                }

                @Override
                public void onFailure(Call<JsonElement > call, Throwable t) {
                    prog.hideProg();
                    t.printStackTrace();
                }
            });

        } else {
            snakeBaar.showSnackBar( ac, getString(R.string.no_internet), main_lay );
        }
    }



    private void postShareToServer( String content_id, final int clicked_post_position) {



        if (networkChecking.isNetworkAvailable( ac )) {
//            prog.progDialog( ac );


            RequestBody user_id_part = RequestBody.create(MediaType.parse("multipart/form-data"), tinyDb.getString("ID"));
            RequestBody password_part = RequestBody.create(MediaType.parse("multipart/form-data"), tinyDb.getString("PASSWORD"));
            RequestBody content_id_part = RequestBody.create(MediaType.parse("multipart/form-data"), content_id);



            MainApplication.getApiService().addShare("content/share/"+tinyDb.getString("ID"),content_id_part, user_id_part, password_part).enqueue(new Callback<JsonElement>() {
                @Override
                public void onResponse(Call<JsonElement> call, Response<JsonElement > response) {
//                    prog.hideProg();
                    if (response.isSuccessful()) {

                        try {
                            JSONObject jsonObj = new JSONObject(String.valueOf(response.body()));
                            String responceCode =  jsonObj.getString("rc");
                            if (responceCode.equals("1")){
                                snakeBaar.showSnackBar( ac, getString(R.string.share_posted), main_lay );
                                int num_of_comment = all_postListt.get(clicked_post_position).getNumshare();
                                num_of_comment = num_of_comment+1;
                                all_postListt.get(clicked_post_position).setNumshare(num_of_comment);

                                all_post_adapter.notifyItemChanged(clicked_post_position);
                            }

                        }catch (Exception e1){}

                    }
                }

                @Override
                public void onFailure(Call<JsonElement > call, Throwable t) {
                    prog.hideProg();
                    t.printStackTrace();
                }
            });

        } else {
            snakeBaar.showSnackBar( ac, getString(R.string.no_internet), main_lay );
        }
    }


    private void postFollow(String leaderId,  final String follow) {



        if (networkChecking.isNetworkAvailable( ac )) {
            prog.progDialog( ac );


            RequestBody user_id_part = RequestBody.create(MediaType.parse("multipart/form-data"), tinyDb.getString("ID"));
            RequestBody password_part = RequestBody.create(MediaType.parse("multipart/form-data"), tinyDb.getString("PASSWORD"));
            RequestBody leaderId_part = RequestBody.create(MediaType.parse("multipart/form-data"), leaderId);
            RequestBody follow_part = RequestBody.create(MediaType.parse("multipart/form-data"), follow);



            MainApplication.getApiService().addFollow("/api/user/follow/"+tinyDb.getString("ID"),leaderId_part,  user_id_part, follow_part, password_part).enqueue(new Callback<JsonElement>() {
                @Override
                public void onResponse(Call<JsonElement> call, Response<JsonElement > response) {
                    prog.hideProg();
                    if (response.isSuccessful()) {

                        try {
                            JSONObject jsonObj = new JSONObject(String.valueOf(response.body()));
                            String responceCode =  jsonObj.getString("rc");
                            if (responceCode.equals("1")){

                                if (follow.equals("0")){
                                    add_follower.setText(getResources().getString(R.string.follow));
                                    add_follower.setBackgroundResource(R.drawable.button_in_active_shape);
                                    add_follower.setTextColor(getResources().getColor(R.color.colorPrimary));
                                    snakeBaar.showSnackBar(ac, getString(R.string.un_follow), main_lay);
                                }else{
                                    add_follower.setText(getResources().getString(R.string.unfollow));
                                    add_follower.setBackgroundResource(R.drawable.button_active_shape);
                                    add_follower.setTextColor(getResources().getColor(R.color.colorWhite));
                                    snakeBaar.showSnackBar(ac, getString(R.string.add_follow), main_lay);
                                }

                            }

                        }catch (Exception e1){}

                    }
                }

                @Override
                public void onFailure(Call<JsonElement > call, Throwable t) {
                    prog.hideProg();
                    t.printStackTrace();
                }
            });

        } else {
            snakeBaar.showSnackBar( ac, getString(R.string.no_internet), main_lay );
        }
    }


    private void getComments(String content_id, String offset, String num_reslt) {

        if (networkChecking.isNetworkAvailable( ac )) {
            prog.progDialog( ac );


            all_commentList = new ArrayList<>();
            RequestBody lang_part = RequestBody.create(MediaType.parse("multipart/form-data"), tinyDb.getString("LANG"));
            RequestBody offset_part = RequestBody.create(MediaType.parse("multipart/form-data"), offset);
            RequestBody content_id_part = RequestBody.create(MediaType.parse("multipart/form-data"), content_id);
            RequestBody numreslt_part = RequestBody.create(MediaType.parse("multipart/form-data"), num_reslt);



            MainApplication.getApiService().getComment("content/comments/get/"+tinyDb.getString("ID"), content_id_part, offset_part, numreslt_part, lang_part).enqueue(new Callback<JsonElement>() {
                @Override
                public void onResponse(Call<JsonElement> call, Response<JsonElement > response) {
                    prog.hideProg();
                    if (response.isSuccessful()) {

                        try {

                            try {
                                JSONObject jsonObj = new JSONObject(String.valueOf(response.body()));
                                responceCode =  jsonObj.getString("rc");

                            }catch (Exception e1){}

                            if (responceCode.equals("1")) {
                                Gson gson = new Gson();
                                GetCommentResponce responcee = null;


//                                //System.out.println("responceeeeeeeeeeeeee   " + response.body());
                                responcee = gson.fromJson(String.valueOf(response.body()), GetCommentResponce.class);

                                all_commentList = responcee.getContent().getComments();
                                recycle_comment.setAdapter(commentAdapter);

                            }else{
                                snakeBaar.showSnackBar( ac, "No comment found.", main_lay );
                            }


                        }catch (Exception e1){}

                    }
                }

                @Override
                public void onFailure(Call<JsonElement > call, Throwable t) {
                    prog.hideProg();
                    t.printStackTrace();
                }
            });








        } else {
            snakeBaar.showSnackBar( ac, getString(R.string.no_internet), main_lay );
        }

    }


    private void postCommentToServer(final String comment_str, String content_id, final int clicked_post_position) {



        if (networkChecking.isNetworkAvailable( ac )) {
            prog.progDialog( ac );


            RequestBody user_id_part = RequestBody.create(MediaType.parse("multipart/form-data"), tinyDb.getString("ID"));
            RequestBody password_part = RequestBody.create(MediaType.parse("multipart/form-data"), tinyDb.getString("PASSWORD"));
            RequestBody content_id_part = RequestBody.create(MediaType.parse("multipart/form-data"), content_id);
            RequestBody comment_part = RequestBody.create(MediaType.parse("multipart/form-data"), comment_str);



            MainApplication.getApiService().addComment("content/comment/add/"+tinyDb.getString("ID"),content_id_part, user_id_part, password_part, comment_part).enqueue(new Callback<JsonElement>() {
                @Override
                public void onResponse(Call<JsonElement> call, Response<JsonElement > response) {
                    prog.hideProg();
                    if (response.isSuccessful()) {

                        try {
                            JSONObject jsonObj = new JSONObject(String.valueOf(response.body()));
                            String responceCode =  jsonObj.getString("rc");
                            if (responceCode.equals("1")){
                                snakeBaar.showSnackBar( ac, getString(R.string.comment_added), main_lay );
                                int num_of_comment = all_postListt.get(clicked_post_position).getNumcomm();
                                num_of_comment = num_of_comment+1;
                                all_postListt.get(clicked_post_position).setNumcomm(num_of_comment);
                                all_post_adapter.notifyItemChanged(clicked_post_position);
                            }

                        }catch (Exception e1){}

                    }
                }

                @Override
                public void onFailure(Call<JsonElement > call, Throwable t) {
                    prog.hideProg();
                    t.printStackTrace();
                }
            });

        } else {
            snakeBaar.showSnackBar( ac, getString(R.string.no_internet), main_lay );
        }
    }



    public  void whatsApp(final  List<Contentlist> all_postList, final List<MultiFileLoadRequest> uris, final  int position){
//        //System.out.println("URI list  "+uris);

        pathsList = new ArrayList<>();

        FileLoader.multiFileDownload(ac)
                .fromDirectory(Environment.DIRECTORY_PICTURES, FileLoader.DIR_EXTERNAL_PUBLIC)
                .progressListener(new MultiFileDownloadListener() {
                    @Override
                    public void onProgress(File downloadedFile, int progress, int totalFiles) {

                        donut_progress.setMax(totalFiles);
                        donut_progress.setProgress(progress);
//                        //System.out.println("Path listt-  "+downloadedFile.getAbsolutePath());
                        pathsList.add(downloadedFile.getAbsolutePath());

//                        //System.out.println(uris.size()+"   Path list size  "+pathsList.size());
                        if (pathsList.size() == uris.size()){

                            donut_progress_layout.setVisibility( View.GONE );
                            donut_progress.setProgress( 0 );

                            ArrayList<Uri> uri_lis = new ArrayList<>();
                            for (int i = 0; i < pathsList.size(); i++) {
                                uri_lis.add(Uri.parse(pathsList.get(i)));
                            }

                            if (all_postList.get(position).getType().equals("aud")) {
//                                videoshare.setType("audio/*");

                                Intent share = new Intent(Intent.ACTION_SEND);
                                share.putExtra(Intent.EXTRA_STREAM, uri_lis.get(0));
                                share.setType("audio/*");
                                share.setPackage("com.whatsapp");
                                share.putExtra(Intent.EXTRA_TEXT, all_postList.get(position).getTags()+"\n"+all_postList.get(position).getTitle()+"\n"+"\n\n"+getString(R.string.share_link)+" https://play.google.com/store/apps/details?id=com.panagola.app.om");
                                share.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                                startActivityForResult(share, 4);
                            }else{
                                Intent videoshare = new Intent(Intent.ACTION_SEND);
                                videoshare.setType("*/*");
                                videoshare.setPackage("com.whatsapp");
                                videoshare.putExtra(Intent.EXTRA_TEXT, all_postList.get(position).getTags()+"\n"+all_postList.get(position).getTitle()+"\n"+"\n\n"+getString(R.string.share_link)+" https://play.google.com/store/apps/details?id=com.panagola.app.om");
                                videoshare.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                                videoshare.putParcelableArrayListExtra(Intent.EXTRA_STREAM,uri_lis);

                                startActivityForResult(videoshare, 4);
                            }

//                            Intent videoshare = new Intent(Intent.ACTION_SEND);
//                            if (all_postList.get(position).getType().equals("aud")) {
//                                videoshare.setType("audio/*");
//                            }else{
//                                videoshare.setType("*/*");
//                            }
//                            videoshare.setPackage("com.whatsapp");
//                            videoshare.putExtra(Intent.EXTRA_TEXT, "Tags:-"+all_postList.get(position).getTags()+"\n"+all_postList.get(position).getTitle()+"\n"+all_postList.get(position).getDesc()+"\n\n"+getString(R.string.share_link)+" https://play.google.com/store/apps/details?id=com.panagola.app.om"); videoshare.putParcelableArrayListExtra(Intent.EXTRA_STREAM,uri_lis);
//
//                            startActivityForResult(videoshare, 4);
                        }

                    }

                    @Override
                    public void onError(Exception e, int progress) {
                        super.onError(e, progress);
                    }
                }).loadMultiple(true, uris);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 4) {
            contentId = all_postListt.get(clicked_post_position).getId();
            postShareToServer(contentId, clicked_post_position);

        }
    }

    public class FollowerUsersAdapter extends RecyclerView.Adapter<FollowerUsersAdapter.MyViewHolder> {

        FollowerUsersAdapter() {}

        @Override
        public int getItemViewType(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }



        @Override
        public FollowerUsersAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_followers, parent, false);
            FollowerUsersAdapter.MyViewHolder vh = new FollowerUsersAdapter.MyViewHolder( view );
            return vh;

        }


        @Override
        public void onBindViewHolder(final FollowerUsersAdapter.MyViewHolder holder, final int position) {


            FollowerUsersAdapter.MyViewHolder viewHolder = (FollowerUsersAdapter.MyViewHolder) holder;
            holder.viewPostion = position;


            try {
                ImageRequest requestU = ImageRequestBuilder.newBuilderWithSource(Uri.parse(allFlist.get(position).getUimage()))
                        .setResizeOptions(new ResizeOptions(60, 60))
                        .build();
                PipelineDraweeController controllerU = (PipelineDraweeController) Fresco.newDraweeControllerBuilder()
                        .setOldController(holder.profile_image.getController())
                        .setImageRequest(requestU)
                        .build();

                holder.profile_image.setController(controllerU);
            } catch (Exception e) {}


            try {
                holder.description.setVisibility(View.GONE);
            } catch (Exception e) {}


            try {
                holder.name.setText(allFlist.get(holder.viewPostion).getName());
            } catch (Exception e) {}



            holder.add_follower.setText(getResources().getString(R.string.followers));

            ((FollowerUsersAdapter.MyViewHolder) holder).top_lay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    tinyDb.putString("ClIK_USER_ID", allFlist.get(holder.viewPostion).getId());

                    Intent mainIntent = new Intent(ac, UserDetailAc.class);
                    startActivity(mainIntent);
                    ac.overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);

                }
            } );




        }


        @Override
        public int getItemCount() {

            return allFlist.size();
        }


        public class MyViewHolder extends RecyclerView.ViewHolder {

            int viewPostion;


            TextView name, description;
            SimpleDraweeView profile_image;


            RelativeLayout top_lay;
            Button add_follower;

            public MyViewHolder(View itemView) {
                super( itemView );


                add_follower = (Button) itemView.findViewById( R.id.add_follower );
                top_lay = (RelativeLayout) itemView.findViewById( R.id.top_lay);
                name = (TextView) itemView.findViewById( R.id.name );
                description = (TextView) itemView.findViewById( R.id.description );
                profile_image = (SimpleDraweeView) itemView.findViewById(R.id.profile_image);



            }
        }


    }

    public class FollowingUsersAdapter extends RecyclerView.Adapter<FollowingUsersAdapter.MyViewHolder> {

        FollowingUsersAdapter() {}

        @Override
        public int getItemViewType(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }



        @Override
        public FollowingUsersAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_followers, parent, false);
            FollowingUsersAdapter.MyViewHolder vh = new FollowingUsersAdapter.MyViewHolder( view );
            return vh;

        }


        @Override
        public void onBindViewHolder(final FollowingUsersAdapter.MyViewHolder holder, final int position) {


            FollowingUsersAdapter.MyViewHolder viewHolder = (FollowingUsersAdapter.MyViewHolder) holder;
            holder.viewPostion = position;


            try {
                ImageRequest requestU = ImageRequestBuilder.newBuilderWithSource(Uri.parse(allLlist.get(position).getUimage()))
                        .setResizeOptions(new ResizeOptions(60, 60))
                        .build();
                PipelineDraweeController controllerU = (PipelineDraweeController) Fresco.newDraweeControllerBuilder()
                        .setOldController(holder.profile_image.getController())
                        .setImageRequest(requestU)
                        .build();

                holder.profile_image.setController(controllerU);
            } catch (Exception e) {}


            try {
                holder.description.setVisibility(View.GONE);
            } catch (Exception e) {}


            try {
                holder.name.setText(allLlist.get(holder.viewPostion).getName());
            } catch (Exception e) {}


            holder.add_follower.setText(getResources().getString(R.string.following));
//           if (allFlist.get(holder.viewPostion).isFollow()){
//                holder.add_follower.setText(getResources().getString(R.string.follow));
//           }else{
//               holder.add_follower.setText(getResources().getString(R.string.un_follow));
//           }


            ((FollowingUsersAdapter.MyViewHolder) holder).top_lay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    tinyDb.putString("ClIK_USER_ID", allLlist.get(position).getId());

                    Intent mainIntent = new Intent(ac, UserDetailAc.class);
                    startActivity(mainIntent);
                    ac.overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);

                }
            } );


//            ((MyViewHolder) holder).add_follower.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    clicked_post_position = position;
//
//                        followrRemoveAdd(allLlist.get(position).getId(), "0", position);
//
//
//                }
//            });

        }


        @Override
        public int getItemCount() {

            return allLlist.size();
        }


        public class MyViewHolder extends RecyclerView.ViewHolder {

            int viewPostion;


            TextView name, description;
            SimpleDraweeView profile_image;


            RelativeLayout top_lay;
            Button add_follower;

            public MyViewHolder(View itemView) {
                super( itemView );


                add_follower = (Button) itemView.findViewById( R.id.add_follower );
                top_lay = (RelativeLayout) itemView.findViewById( R.id.top_lay);
                name = (TextView) itemView.findViewById( R.id.name );
                description = (TextView) itemView.findViewById( R.id.description );
                profile_image = (SimpleDraweeView) itemView.findViewById(R.id.profile_image);

            }
        }


    }

    public class CommentedOnAdapter extends RecyclerView.Adapter<CommentedOnAdapter.MyViewHolder> {

        CommentedOnAdapter() {}

        @Override
        public int getItemViewType(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }



        @Override
        public CommentedOnAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_commented_on, parent, false);
            CommentedOnAdapter.MyViewHolder vh = new CommentedOnAdapter.MyViewHolder( view );
            return vh;

        }


        @Override
        public void onBindViewHolder(final CommentedOnAdapter.MyViewHolder holder, final int position) {


            CommentedOnAdapter.MyViewHolder viewHolder = (CommentedOnAdapter.MyViewHolder) holder;
            holder.viewPostion = position;

            if (all_postListt.get(position).getType().equals("img")) {

                try {
                    ImageRequest requestU = ImageRequestBuilder.newBuilderWithSource(Uri.parse(all_postListt.get(position).getImages().get(0)))
                            .setResizeOptions(new ResizeOptions(100, 100))
                            .build();
                    PipelineDraweeController controllerU = (PipelineDraweeController) Fresco.newDraweeControllerBuilder()
                            .setOldController(holder.postimage.getController())
                            .setImageRequest(requestU)
                            .build();

                    holder.postimage.setController(controllerU);
                } catch (Exception e) {}


            }else if (all_postListt.get(position).getType().equals("vid") || all_postListt.get(position).getType().equals("uvid")) {
                holder.play_icon.setVisibility(View.VISIBLE);
                try {
                    ImageRequest requestU = ImageRequestBuilder.newBuilderWithSource(Uri.parse(all_postListt.get(position).getVideoimages().get(0).getI()))
                            .setResizeOptions(new ResizeOptions(100, 100))
                            .build();
                    PipelineDraweeController controllerU = (PipelineDraweeController) Fresco.newDraweeControllerBuilder()
                            .setOldController(holder.postimage.getController())
                            .setImageRequest(requestU)
                            .build();

                    holder.postimage.setController(controllerU);
                } catch (Exception e) {}


            }else if (all_postListt.get(position).getType().equals("aud")){
//                holder.play_icon.setVisibility(View.VISIBLE);
                try {

                    holder.postimage.setImageResource(R.drawable.audio_post);
                } catch (Exception e) {}
            }else{
                holder.postimage.setImageResource(R.drawable.text_post);

            }

            int pos = 0;
            for (int i = 0; i < all_postListt.get(holder.viewPostion).getComments().size(); i++) {
//                //System.out.println(all_postListt.get(holder.viewPostion).getComments().get(i).getUser().getId()+"      hsdhshshh   "+selected_user);
                if (all_postListt.get(holder.viewPostion).getComments().get(i).getUser().getId().equals(selected_user)){
                    pos = i;
                }
            }

            try {
                holder.post_date.setText(all_postListt.get(holder.viewPostion).getComments().get(pos).getDisplayTime());
            } catch (Exception e) {}
            try {
                holder.comment.setText(all_postListt.get(holder.viewPostion).getComments().get(pos).getComment());
            } catch (Exception e) {}


            try {
                holder.title.setText(all_postListt.get(holder.viewPostion).getTitle());
            } catch (Exception e) {}



            try {
                holder.description.setText(all_postListt.get(holder.viewPostion).getDesc());
            } catch (Exception e) {}






            ((CommentedOnAdapter.MyViewHolder) holder).itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    MainActivity.previousfragment = getFragmentManager().findFragmentById(R.id.fragment_container);
                    // Tracking Event
                    MainApplication.getInstance().trackEvent("UserDetail", "Commented on  Clicked", "Commented on Clicked to view detail", tinyDb.getString("ID"));


                    clicked_post_position = holder.viewPostion;

                    contentlistMain = all_postListt.get(holder.viewPostion);
                    tinyDb.putInt("CLICKED_POS", clicked_post_position);


                    Intent intent = new Intent( ac, ContentDetail.class );
                    
                    intent.addFlags( Intent.FLAG_ACTIVITY_CLEAR_TOP );
                    startActivity( intent );
                    ac.overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                }
            });


        }


        @Override
        public int getItemCount() {

            return all_postListt.size();
        }


        public class MyViewHolder extends RecyclerView.ViewHolder {

            int viewPostion;


            TextView title, description, comment, post_date;
            SimpleDraweeView postimage;
            ImageView play_icon;


            RelativeLayout top_lay;

            public MyViewHolder(View itemView) {
                super( itemView );


                top_lay = (RelativeLayout) itemView.findViewById( R.id.top_lay);

                title= (TextView) itemView.findViewById( R.id.title );
                description= (TextView) itemView.findViewById( R.id.description );
                comment= (TextView) itemView.findViewById( R.id.comment );
                post_date= (TextView) itemView.findViewById( R.id.post_date );

                postimage = (SimpleDraweeView) itemView.findViewById(R.id.postimage);
                play_icon = (ImageView) itemView.findViewById(R.id.play_icon);

            }
        }


    }


    public class ReatedOnAdapter extends RecyclerView.Adapter<ReatedOnAdapter.MyViewHolder> {

        ReatedOnAdapter() {}

        @Override
        public int getItemViewType(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }



        @Override
        public ReatedOnAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_reacted_on, parent, false);
            ReatedOnAdapter.MyViewHolder vh = new ReatedOnAdapter.MyViewHolder( view );
            return vh;

        }


        @Override
        public void onBindViewHolder(final ReatedOnAdapter.MyViewHolder holder, final int position) {


            ReatedOnAdapter.MyViewHolder viewHolder = (ReatedOnAdapter.MyViewHolder) holder;
            holder.viewPostion = position;

            if (all_postListt.get(position).getType().equals("img")) {

                try {
                    ImageRequest requestU = ImageRequestBuilder.newBuilderWithSource(Uri.parse(all_postListt.get(position).getImages().get(0)))
                            .setResizeOptions(new ResizeOptions(100, 100))
                            .build();
                    PipelineDraweeController controllerU = (PipelineDraweeController) Fresco.newDraweeControllerBuilder()
                            .setOldController(holder.postimage.getController())
                            .setImageRequest(requestU)
                            .build();

                    holder.postimage.setController(controllerU);
                } catch (Exception e) {}


            }else if (all_postListt.get(position).getType().equals("vid") || all_postListt.get(position).getType().equals("uvid")) {
                holder.play_icon.setVisibility(View.VISIBLE);
                try {
                    ImageRequest requestU = ImageRequestBuilder.newBuilderWithSource(Uri.parse(all_postListt.get(position).getVideoimages().get(0).getI()))
                            .setResizeOptions(new ResizeOptions(100, 100))
                            .build();
                    PipelineDraweeController controllerU = (PipelineDraweeController) Fresco.newDraweeControllerBuilder()
                            .setOldController(holder.postimage.getController())
                            .setImageRequest(requestU)
                            .build();

                    holder.postimage.setController(controllerU);
                } catch (Exception e) {}


            }else if (all_postListt.get(position).getType().equals("aud")){
//                holder.play_icon.setVisibility(View.VISIBLE);
                try {

                    holder.postimage.setImageResource(R.drawable.audio_post);
                } catch (Exception e) {}
            }else{
                holder.postimage.setImageResource(R.drawable.text_post);
            }


            for (int i = 0; i < all_postListt.get(position).getLikes().size(); i++) {




//                if (all_postListt.get(holder.viewPostion).getLikes().get(i).getUser().getId().equals(selected_user)){


                ImageView imageview = new ImageView(ac);

                LinearLayout.LayoutParams params = new LinearLayout
                        .LayoutParams(60, 60);

                // Add image path from drawable folder.
                imageview.setImageResource(icons.getResourceId(all_postListt.get(position).getLikes().get(i).getK(), 0));
                imageview.setLayoutParams(params);
                holder.reactions.addView(imageview);
//                    }

            }

            try {
                holder.displaytime.setText(all_postListt.get(position).getDisplayTime());
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                holder.description.setText(all_postListt.get(position).getDesc());
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                holder.title.setText(all_postListt.get(position).getTitle());
            } catch (Exception e) {
                e.printStackTrace();
            }


            ((ReatedOnAdapter.MyViewHolder) holder).itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    MainActivity.previousfragment = getFragmentManager().findFragmentById(R.id.fragment_container);
                    // Tracking Event
                    MainApplication.getInstance().trackEvent("UserDetail", "Reaction Clicked", "Reaction Clicked to view detail", tinyDb.getString("ID"));


                    clicked_post_position = holder.viewPostion;

                    contentlistMain = all_postListt.get(holder.viewPostion);
                    tinyDb.putInt("CLICKED_POS", clicked_post_position);


                    Intent intent = new Intent( ac, ContentDetail.class );
                    
                    intent.addFlags( Intent.FLAG_ACTIVITY_CLEAR_TOP );
                    startActivity( intent );
                    ac.overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                }
            });



        }


        @Override
        public int getItemCount() {

            return all_postListt.size();
        }


        public class MyViewHolder extends RecyclerView.ViewHolder {

            int viewPostion;
            TextView displaytime, description, title;

            SimpleDraweeView postimage;
            ImageView play_icon;


            RelativeLayout top_lay;
            LinearLayout reactions;

            public MyViewHolder(View itemView) {
                super( itemView );


                top_lay = (RelativeLayout) itemView.findViewById( R.id.top_lay);
                reactions = (LinearLayout) itemView.findViewById( R.id.reactions);
                postimage = (SimpleDraweeView) itemView.findViewById(R.id.postimage);
                displaytime = (TextView) itemView.findViewById(R.id.displaytime);
                description = (TextView) itemView.findViewById(R.id.description);
                title = (TextView) itemView.findViewById(R.id.title);
                play_icon = (ImageView) itemView.findViewById(R.id.play_icon);

            }
        }


    }






}


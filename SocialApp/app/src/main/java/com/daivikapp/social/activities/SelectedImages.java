package com.daivikapp.social.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Paint;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.rygelouv.audiosensei.player.AudioSenseiPlayerView;
import com.daivikapp.social.R;
import com.daivikapp.social.models.ContentUploadResponce;
import com.daivikapp.social.models.GetTagResponce;
import com.daivikapp.social.utilities.MainApplication;
import com.daivikapp.social.utilities.NetworkChecking;
import com.daivikapp.social.utilities.ProgDialog;
import com.daivikapp.social.utilities.SnakeBaar;
import com.daivikapp.social.utilities.TinyDB;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import cn.jzvd.JZVideoPlayerStandard;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.daivikapp.social.utilities.MainApplication.getInstance;


public class SelectedImages extends Activity implements View.OnClickListener, LocationListener {


    Activity ac;

    SnakeBaar snakeBaar;
    ProgDialog prog;
    NetworkChecking networkChecking;

    Dialog show1;
    RecyclerView recycle_media, recycle_tag;




    ImagesAdapter imagesAdapter;
    TagsAdapter tagsAdapter;
    RelativeLayout main_lay;
    LinearLayoutManager layoutManager, layoutManager1;
    LinearLayout description;

    ArrayList<String> all_mediaList = new ArrayList<>();
    ArrayList<String> selectedTagList = new ArrayList<>();


    ArrayList<Boolean> boolTagList = new ArrayList<>();

    EditText title_edt, description_edt;

    LocationManager mLocationManager;
    String place_str = "";

    TinyDB tinyDb;
    ImageView profile_image;

    TextView btnsubmit;
    View line;
    TextView page_title;
    ImageView back;
  public static   RelativeLayout upload_toolbar;
    String mediaType = "";
    GetTagResponce getTagResponce = null;
    AudioSenseiPlayerView audio_player;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.fregment_upload_images );
        ac = this;
        tinyDb = new TinyDB( ac );

//        toolbar.setVisibility(View.GONE);

        // Tracking the screen view
        getInstance().trackScreenView("Post Content", tinyDb.getString("ID"));


        page_title = (TextView)findViewById(R.id.page_title);
        back = (ImageView) findViewById(R.id.back);
        upload_toolbar = (RelativeLayout) findViewById(R.id.reg_toolbar);
        profile_image = (ImageView) findViewById(R.id.profile_image);

        profile_image.setImageURI( Uri.parse(tinyDb.getString("IMAGE")) );


        page_title.setText(R.string.share_post);


        networkChecking = new NetworkChecking();
        snakeBaar = new SnakeBaar();
        prog = new ProgDialog();





        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ac.onBackPressed();
                try {
                    audio_player.stop();
                    audio_player.setVisibility(View.GONE);
                } catch (Exception e) {
                }

            }
        });



        title_edt = (EditText) findViewById(R.id.title_edt);
        description_edt = (EditText) findViewById(R.id.description_edt);
        btnsubmit = (TextView) findViewById(R.id.btnsubmit);
        btnsubmit.setOnClickListener(this);

        line = (View) findViewById(R.id.line);



        main_lay = (RelativeLayout) findViewById(R.id.main_lay);
        description = (LinearLayout) findViewById(R.id.description);
        recycle_media = (RecyclerView)  findViewById(R.id.recycle_post);
        recycle_media.setNestedScrollingEnabled(false);
        recycle_tag = (RecyclerView)  findViewById(R.id.recycle_tag);
        audio_player = (AudioSenseiPlayerView) findViewById( R.id.audio_player );


        layoutManager1 = new LinearLayoutManager( ac, LinearLayoutManager.HORIZONTAL, false );

        imagesAdapter = new ImagesAdapter( ac );
        recycle_tag.setHasFixedSize( true );
        recycle_tag.setItemViewCacheSize( 10 );
        recycle_tag.setDrawingCacheEnabled( true );
        recycle_tag.setLayoutManager( layoutManager1 );


        recycle_media.setVisibility(View.VISIBLE);
        mediaType = tinyDb.getString("MideaType");
        all_mediaList = tinyDb.getListString("Media_Path");

        if (mediaType.equals("Image")) {
            audio_player.setVisibility(View.GONE);

            layoutManager = new LinearLayoutManager( ac, LinearLayoutManager.VERTICAL, false );

            imagesAdapter = new ImagesAdapter( ac );
            recycle_media.setHasFixedSize( true );
            recycle_media.setItemViewCacheSize( 10 );
            recycle_media.setDrawingCacheEnabled( true );
            recycle_media.setLayoutManager( layoutManager );

            recycle_media.setAdapter( imagesAdapter );
        }else if (mediaType.equals("Video")) {
            audio_player.setVisibility(View.GONE);
            layoutManager = new LinearLayoutManager( ac, LinearLayoutManager.VERTICAL, false );

            imagesAdapter = new ImagesAdapter( ac );
            recycle_media.setHasFixedSize( true );
            recycle_media.setItemViewCacheSize( 10 );
            recycle_media.setDrawingCacheEnabled( true );
            recycle_media.setLayoutManager( layoutManager );
            recycle_media.setAdapter( imagesAdapter );




        }else if (mediaType.equals("Audio")) {



            audio_player.setVisibility(View.VISIBLE);
            audio_player.setAudioTarget(all_mediaList.get(0));







        }else {
            audio_player.setVisibility(View.GONE);
        }





//        //System.out.println("gsgsdgsg   "+all_mediaList);

        if (mediaType.equals("Text")) {

            RelativeLayout.LayoutParams rel_btn = new RelativeLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

            rel_btn.addRule(RelativeLayout.BELOW, R.id.profile_image);
//            rel_btn.addRule(RelativeLayout.RIGHT_OF, R.id.profile_image);
            rel_btn.setMargins(5, 5, 5, 5);

            description.setLayoutParams(rel_btn);

//            LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(
//                    LinearLayout.LayoutParams.MATCH_PARENT,
//                    LinearLayout.LayoutParams.MATCH_PARENT
//            );
//            description.setLayoutParams(param);
            recycle_media.setVisibility(View.GONE);
        }




        title_edt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    description_edt.requestFocus();
                    InputMethodManager imm = (InputMethodManager) ac.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.showSoftInput(description_edt, InputMethodManager.SHOW_IMPLICIT);

//                    InputMethodManager imm = (InputMethodManager) ac.getSystemService(Context.INPUT_METHOD_SERVICE);
//                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    return true;
                }
                return false;
            }
        });






        //Permission check and ask for required permission
        Dexter.withActivity(ac)
                .withPermissions(
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.ACCESS_FINE_LOCATION
                ).withListener(new MultiplePermissionsListener() {
            @Override
            public void onPermissionsChecked(MultiplePermissionsReport report) {

                mLocationManager = (LocationManager) ac.getSystemService(Context.LOCATION_SERVICE);

                @SuppressLint("MissingPermission")
                Location location = mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                if(location != null) {
                    // Do something with the recent location fix
                    //  otherwise wait for the update below
                    place_str =  getAddressFromLatLng(ac, location);
//                    //System.out.println("Placeeeeeeeee   "+place_str);
                }
                else {

//                    mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
                }



            }

            @Override
            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
            }

        }).check();


        Gson gson = new Gson();
        getTagResponce= gson.fromJson(tinyDb.getString("Tags"), GetTagResponce.class);


        for (int i = 0; i < getTagResponce.getTags().size(); i++) {
            boolTagList.add(false);
        }

        layoutManager1 = new LinearLayoutManager( ac, LinearLayoutManager.HORIZONTAL, false );

        tagsAdapter = new TagsAdapter( ac );
        recycle_tag.setHasFixedSize( true );
        recycle_tag.setItemViewCacheSize( 10 );
        recycle_tag.setDrawingCacheEnabled( true );
        recycle_tag.setLayoutManager( layoutManager1 );
        recycle_tag.setAdapter(tagsAdapter);




    }



    @Override
    public void onClick(View view) {

   if (view == btnsubmit){

       selectedTagList = new ArrayList<>();

       for (int i = 0; i < boolTagList.size(); i++) {
           if (boolTagList.get(i)) {
               selectedTagList.add(getTagResponce.getTags().get(i));
           }
       }


       if (title_edt.getText().toString().length()<10){


           new AlertDialog.Builder(ac)
                   .setTitle(R.string.oops)
                   .setMessage(R.string.post_title)
                   .setCancelable(false)
                   .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                       @Override
                       public void onClick(DialogInterface dialog, int which) {
                           // Whatever...
                       }
                   }).show();

       }else if (selectedTagList.size()<=0){


           new AlertDialog.Builder(ac)
                   .setTitle(R.string.oops)
                   .setMessage(R.string.select_tag)
                   .setCancelable(false)
                   .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                       @Override
                       public void onClick(DialogInterface dialog, int which) {
                           // Whatever...
                       }
                   }).show();

       }else {


           show1 = new Dialog(ac, R.style.AlertDialogCustom);
           show1.setContentView(R.layout.dialog_confirmetion);


           Button cancel = (Button) show1.findViewById(R.id.cancel);
           Button done = (Button) show1.findViewById(R.id.done);

           done.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View v) {
                   if (!mediaType.equals("Text")) {
                       // Tracking Event
                       MainApplication.getInstance().trackEvent("Post", "Status Added", "Media post added", tinyDb.getString("ID"));
                       postDataToServer();
                   } else {
                       // Tracking Event
                       MainApplication.getInstance().trackEvent("Post", "Status Added", "Text post added", tinyDb.getString("ID"));
                       postTextDataToServer();
                   }
                   show1.dismiss();
               }
           });

           cancel.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View v) {
                  show1.dismiss();

               }
           });

           show1.show();



       }
   }

    }


    private void postTextDataToServer() {

        if (networkChecking.isNetworkAvailable( ac )) {
            prog.progDialog( ac );

            ArrayList<RequestBody> chanalListPart = new ArrayList<>();

            RequestBody loc_part;
            RequestBody user_id_part = RequestBody.create(MediaType.parse("multipart/form-data"), tinyDb.getString("ID"));
            RequestBody password_part = RequestBody.create(MediaType.parse("multipart/form-data"), tinyDb.getString("PASSWORD"));
            RequestBody title_part = RequestBody.create(MediaType.parse("multipart/form-data"), title_edt.getText().toString());
            RequestBody desc_part = RequestBody.create(MediaType.parse("multipart/form-data"), description_edt.getText().toString());
            RequestBody lang_part = RequestBody.create(MediaType.parse("multipart/form-data"), tinyDb.getString("LANG"));
            RequestBody type_part = RequestBody.create(MediaType.parse("multipart/form-data"), "txt");
            RequestBody style_part = RequestBody.create(MediaType.parse("multipart/form-data"), "0");
//            RequestBody tags_part = RequestBody.create(MediaType.parse("multipart/form-data"), selectedTagList);


            for (int i = 0; i < selectedTagList.size(); i++) {
                RequestBody tag_part = RequestBody.create(MediaType.parse("multipart/form-data"), selectedTagList.get(i));
                chanalListPart.add(tag_part);
            }
//            //System.out.println( "sdsgsgsgdsdgs      "+chanalListPart);

            if (!place_str.isEmpty()) {
                loc_part = RequestBody.create(MediaType.parse("multipart/form-data"), place_str);
            }else{
                loc_part = RequestBody.create(MediaType.parse("multipart/form-data"), "");
            }


            MainApplication.getApiService().uploadStatus("content/add/"+tinyDb.getString("ID"), user_id_part, password_part, title_part, desc_part, lang_part, type_part, style_part, chanalListPart, loc_part).enqueue(new Callback<JsonElement>() {
                @Override
                public void onResponse(Call<JsonElement> call, Response<JsonElement > response) {
                    prog.hideProg();
                    if (response.isSuccessful()) {

                        try {
                            audio_player.stop();
                            audio_player.setVisibility(View.GONE);
                        } catch (Exception e) {
//                            e.printStackTrace();
                        }
                        Gson gson = new Gson();
                        ContentUploadResponce responcee = null;


//                        //System.out.println("responceeeeeeeeeeeeee   "+response.body());
                        responcee= gson.fromJson(String.valueOf(response.body()), ContentUploadResponce.class);

                        if (responcee.getRc() == 1){
                            snakeBaar.showSnackBar( ac, "Status uploaded successfully", main_lay );
//                            toolbar.setVisibility(View.VISIBLE);
                            try {
                                upload_toolbar.setVisibility(View.GONE);
                            } catch (Exception e) {}
                            Intent mainIntent = new Intent(getApplicationContext(), MainActivity.class);
                            mainIntent.putExtra("Screen", "AllFeeds");
                            startActivity(mainIntent);
                            overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                            finish();

                        }

                    }
                }

                @Override
                public void onFailure(Call<JsonElement > call, Throwable t) {
                    prog.hideProg();
                    t.printStackTrace();
                }
            });

        } else {
            snakeBaar.showSnackBar( ac, getString(R.string.no_internet), main_lay );
        }
    }


    private void postDataToServer() {



        if (networkChecking.isNetworkAvailable( ac )) {
            prog.progDialog(ac);

            ArrayList<MultipartBody.Part> images_partList = new ArrayList<>();
            for (int i = 0; i < all_mediaList.size(); i++) {
                Uri uri = Uri.fromFile(new File(all_mediaList.get(i)));
//                File file = new File(getRealPathFromURI(uri));

                File file = new File(all_mediaList.get(i));

                MultipartBody.Part imagenPerfil = null;
                // create RequestBody instance from file
                RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);


                if (mediaType.equals("Image")){
                    // MultipartBody.Part is used to send also the actual file name
                    imagenPerfil = MultipartBody.Part.createFormData("images", file.getName(), requestFile);
            }else  if (mediaType.equals("Video")){
                    imagenPerfil = MultipartBody.Part.createFormData("videos", file.getName(), requestFile);
                }else if (mediaType.equals("Audio")) {
                    imagenPerfil = MultipartBody.Part.createFormData("audios", file.getName(), requestFile);
                }

//                images_partList.add(prepareFilePart("images", Uri.fromFile(new File(all_mediaList.get(1)))));
                images_partList.add(imagenPerfil);
            }
//

            ArrayList<RequestBody> chanalListPart = new ArrayList<>();
            RequestBody type_part;
            RequestBody loc_part;
            RequestBody user_id_part = RequestBody.create(MediaType.parse("multipart/form-data"), tinyDb.getString("ID"));
            RequestBody password_part = RequestBody.create(MediaType.parse("multipart/form-data"), tinyDb.getString("PASSWORD"));
            RequestBody title_part = RequestBody.create(MediaType.parse("multipart/form-data"), title_edt.getText().toString());
            RequestBody desc_part = RequestBody.create(MediaType.parse("multipart/form-data"), description_edt.getText().toString());
            RequestBody lang_part = RequestBody.create(MediaType.parse("multipart/form-data"), tinyDb.getString("LANG"));

            for (int i = 0; i < selectedTagList.size(); i++) {
                RequestBody tag_part = RequestBody.create(MediaType.parse("multipart/form-data"), selectedTagList.get(i));
                chanalListPart.add(tag_part);
            }
//            //System.out.println( "sdsgsgsgdsdgs      "+chanalListPart);


            if (mediaType.equals("Image")) {
                 type_part = RequestBody.create(MediaType.parse("multipart/form-data"), "img");
            }else if (mediaType.equals("Audio")) {
                type_part = RequestBody.create(MediaType.parse("multipart/form-data"), "aud");
            }else{
                 type_part = RequestBody.create(MediaType.parse("multipart/form-data"), "vid");
            }
            RequestBody style_part = RequestBody.create(MediaType.parse("multipart/form-data"), "0");
//            RequestBody tags_part = RequestBody.create(MediaType.parse("multipart/form-data"), "");
            if (!place_str.isEmpty()) {
                loc_part = RequestBody.create(MediaType.parse("multipart/form-data"), place_str);
            }else{
                loc_part = RequestBody.create(MediaType.parse("multipart/form-data"), "");
            }

            MainApplication.getApiService().uploadFiles("content/add/"+tinyDb.getString("ID"),user_id_part, password_part, title_part, desc_part, lang_part, type_part, style_part, chanalListPart, loc_part, images_partList).enqueue(new Callback<JsonElement>() {
                @Override
                public void onResponse(Call<JsonElement> call, Response<JsonElement > response) {
                    prog.hideProg();
                    if (response.isSuccessful()) {

                        try {
                            audio_player.stop();
                            audio_player.setVisibility(View.GONE);
                        } catch (Exception e) {}

                        Gson gson = new Gson();
                        ContentUploadResponce responcee = null;

                        responcee= gson.fromJson(String.valueOf(response.body()), ContentUploadResponce.class);

                        if (responcee.getRc() == 1){

                            try {
                                snakeBaar.showSnackBar( ac, getString(R.string.content_uploaded), main_lay );
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            try {
                                upload_toolbar.setVisibility(View.GONE);
                            } catch (Exception e) {}

                            android.app.FragmentManager notifications ;
                            android.app.FragmentTransaction fragmentTransaction2;

                            tinyDb.putString("TYPE", "POPULAR");
                            Intent mainIntent = new Intent(getApplicationContext(), MainActivity.class);
                            mainIntent.putExtra("Screen", "AllFeeds");
                            startActivity(mainIntent);

                            overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                            finish();

                        }else{
                            snakeBaar.showSnackBar( ac, getString(R.string.content), main_lay );
                        }

                    }
                }

                @Override
                public void onFailure(Call<JsonElement > call, Throwable t) {

                    try {
                        prog.hideProg();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    t.printStackTrace();
                }
            });








        } else {
            snakeBaar.showSnackBar( ac, getString(R.string.no_internet), main_lay );
        }
    }






    public static String getAddressFromLatLng(Context context, Location latLng) {
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(context, Locale.getDefault());
        try {
            addresses = geocoder.getFromLocation(latLng.getLatitude(), latLng.getLongitude(), 1);
            return addresses.get(0).getAddressLine(0);
        } catch (Exception e) {
//            e.printStackTrace();
            return "";
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        if (location != null) {
            Log.v("Location Changed", location.getLatitude() + " and " + location.getLongitude());
            mLocationManager.removeUpdates(this);
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {}

    @Override
    public void onProviderEnabled(String provider) { }

    @Override
    public void onProviderDisabled(String provider) {}



//    public class MyAdapter extends MultiChoiceAdapter<MyAdapter.MyViewHolder> {
//
//        Context mContext;
//        List<String> mList;
//        public MyAdapter(List<String> stringList, Context context) {
//            this.mList = stringList;
//            this.mContext = context;
//        }
//
//        @NonNull
//        @Override
//        public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
//            View view;
//            MyViewHolder vh ;
//
//            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_tag, parent, false);
//
//            vh = new MyViewHolder( view );
//            return vh;
//        }
//        @Override
//        public void onBindViewHolder(final MyAdapter.MyViewHolder holder, final int position) {
//            super.onBindViewHolder(holder, position);
//            holder.textView.setText(mList.get(position));
//
////            holder.textView.setOnClickListener(new View.OnClickListener() {
////                @Override
////                public void onClick(View v) {
////
////
////                        if (myAdapter.getSelectedItemList().size() >3) {
////
////                            Toast.makeText(ac, R.string.select_tag_limit, Toast.LENGTH_SHORT).show();
////                        }else{
////                            boolean check_item = false;
////                            for (int i = 0; i < myAdapter.getSelectedItemList().size(); i++) {
////                              if (position == myAdapter.getSelectedItemList().get(i)){
////                                  check_item = true;
////                                  break;
////                              }
////                            }
////
////                    if (!check_item) {
////
////                        myAdapter.select(position);
////                        holder.textView.setTextColor(Color.WHITE);
////                        holder.main_card.setBackgroundColor(ac.getResources().getColor(R.color.colorPrimary));
////
////
////                    } else {
////                        //Reset your changes
////                        myAdapter.deselect(position);
////                        holder.main_card.setBackgroundColor(ac.getResources().getColor(R.color.colorWhite));
////                        holder.textView.setTextColor(Color.BLACK);
////
////
////                    }
////                    }
////
////                    //System.out.println(myAdapter.getSelectedItemCount()+"    selected item size   "+myAdapter.getSelectedItemList() );
////                }
////            });
//
//
//        }
//
//        @Override
//        public int getItemCount() {
//            return mList.size();
//        }
//
//
//
//
//
//
//        @Override
//        public void setActive(View rootView, boolean state) {
//
////            //System.out.println("shdshdshh     "+rootView.getId());
////
////            if (myAdapter.getSelectedItemList().size()>3){
////
//////                myAdapter.deselect(position);
////            }
//            TextView  textView = (TextView) rootView.findViewById(R.id.textView);
//            CardView main_card = (CardView) rootView.findViewById(R.id.main_card);
//
//            if (state) {
//                textView.setTextColor(Color.WHITE);
//                main_card.setBackgroundColor(ac.getResources().getColor(R.color.colorPrimary));
//
//
//            } else {
//                main_card.setBackgroundColor(ac.getResources().getColor(R.color.colorWhite));
//                textView.setTextColor(Color.BLACK);
//
//
//            }
//
//
//
//        }
//
//
//        public class MyViewHolder extends RecyclerView.ViewHolder {
//            TextView textView;
//            CardView main_card;
//
//
//            public MyViewHolder(View itemView) {
//                super( itemView );
//
//                textView = (TextView) itemView.findViewById(R.id.textView);
//                main_card = (CardView) itemView.findViewById(R.id.main_card);
//
//
//
//
//            }
//        }
//
//
//    }

    public class ImagesAdapter extends RecyclerView.Adapter<ImagesAdapter.MyViewHolder> {


        Context con;



        ImagesAdapter(Context c) {
            con = c;
        }



        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


            View view;
            MyViewHolder vh ;

                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_selected_images, parent, false);

                vh = new MyViewHolder( view );
                return vh;



        }

        @Override
        public void onBindViewHolder(final MyViewHolder holder, final int position) {


            MyViewHolder viewHolder = (MyViewHolder) holder;

            if (mediaType.equals("Image")) {

                holder.videoView.setVisibility(View.GONE);

                Uri imageUri= Uri.fromFile(new File(all_mediaList.get(position)));

                DraweeController controller =
                        Fresco.newDraweeControllerBuilder()
                                .setUri(imageUri)
                                .setAutoPlayAnimations(true)
                                .build();

                holder.imageView.setController(controller);


            }else{
                try {

                    holder.imageView.setVisibility(View.GONE);



                    holder.videoView.setUp(all_mediaList.get(position),
                            JZVideoPlayerStandard.SCREEN_WINDOW_NORMAL,
                            "");

                    MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();

                    mediaMetadataRetriever.setDataSource(all_mediaList.get(position));
                    Bitmap bmFrame = mediaMetadataRetriever.getFrameAtTime(5000000); //unit in microsecond
                    holder.videoView.thumbImageView.setImageBitmap(bmFrame);

                } catch (Exception e) {
//                    e.printStackTrace();
                }
            }


        }


        @Override
        public int getItemCount() {

            return all_mediaList.size();
        }



        public class MyViewHolder extends RecyclerView.ViewHolder {
            SimpleDraweeView imageView;
            JZVideoPlayerStandard videoView;



            public MyViewHolder(View itemView) {
                super( itemView );

                videoView = (JZVideoPlayerStandard) itemView.findViewById(R.id.video_view);
                imageView = (SimpleDraweeView) itemView.findViewById(R.id.image1);


            }
        }


    }



    public class TagsAdapter extends RecyclerView.Adapter<TagsAdapter.MyViewHolder> {


        Context con;



        TagsAdapter(Context c) {
            con = c;
        }



        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


            View view;
            MyViewHolder vh ;

            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_tag, parent, false);


            vh = new MyViewHolder( view );
            return vh;



        }

        @Override
        public void onBindViewHolder(final MyViewHolder holder, final int position) {


            MyViewHolder viewHolder = (MyViewHolder) holder;
            holder.textView.setText(getTagResponce.getTags().get(position));

            if (boolTagList.get(position)) {
                holder.textView.setTextColor(ac.getResources().getColor(R.color.colorTags));
                holder.textView.setPaintFlags(holder.textView.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);


            } else {
                holder.textView.setTextColor(Color.BLACK);
                holder.textView.setPaintFlags(0);
            }



            ((MyViewHolder) holder).itemView.setOnClickListener( new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    int size = 0;
                    for (int i = 0; i < boolTagList.size(); i++) {
                        if (boolTagList.get(i)) {
                            size++;
                        }
                    }


                    if (size < 3) {
                        if (boolTagList.get(position)) {
                            boolTagList.set(position, false);
                        } else {
                            boolTagList.set(position, true);
                        }
                    }else{

                        if (boolTagList.get(position)) {
                            boolTagList.set(position, false);
                        } else {
                            Toast.makeText(ac, ac.getResources().getString(R.string.select_tag_limit), Toast.LENGTH_SHORT).show();
                        }
                    }
                    notifyDataSetChanged();
                }
            } );


        }


        @Override
        public int getItemCount() {

            return getTagResponce.getTags().size();
        }



        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView textView;
            RelativeLayout main_card;



            public MyViewHolder(View itemView) {
                super( itemView );

                textView = (TextView) itemView.findViewById(R.id.textView);
                main_card = (RelativeLayout) itemView.findViewById(R.id.main_card);


            }
        }


    }

    @Override
    public void onBackPressed(){
        if(prog != null)
            prog.destroy();

        show1 = new Dialog(ac, R.style.AlertDialogCustom);
        show1.setContentView(R.layout.dialog_post_cancel_confirmation);

        Button cancel = (Button) show1.findViewById(R.id.cancel);
        Button done = (Button) show1.findViewById(R.id.done);

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                show1.dismiss();
               finish();
//                toolbar.setVisibility(View.VISIBLE);
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                show1.dismiss();
            }
        });

        show1.show();



    }

}

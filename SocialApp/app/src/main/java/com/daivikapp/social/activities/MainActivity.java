package com.daivikapp.social.activities;

import android.app.Activity;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.arch.lifecycle.Lifecycle;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.AppBarLayout;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.view.SimpleDraweeView;
import com.google.gson.JsonElement;
import com.rygelouv.audiosensei.player.AudioSenseiPlayerView;
import com.daivikapp.social.R;
import com.daivikapp.social.fragment.AllFeeds;
import com.daivikapp.social.fragment.UserProfile;
import com.daivikapp.social.utilities.LeftMenu;
import com.daivikapp.social.utilities.MainApplication;
import com.daivikapp.social.utilities.NetworkChecking;
import com.daivikapp.social.utilities.ProgDialog;
import com.daivikapp.social.utilities.SnakeBaar;
import com.daivikapp.social.utilities.TinyDB;
import com.daivikapp.social.utilities.TrackInstall;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import cn.jzvd.JZVideoPlayer;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.daivikapp.social.utilities.LeftMenu.drawer;


public class MainActivity extends AppCompatActivity {

    DrawerLayout drawer_layout_home;
     Toolbar toolbar;

    private ListView contactussLeftDrawer;
    public  static  ImageView toggle;
   public static TextView form_one_title;
   RelativeLayout linearLayout;

   static Activity ac;

    public static SimpleDraweeView profileImage;
    public static TextView name;
    TinyDB tinyDb;
    String responceCode;

    SnakeBaar snakeBaar;
    ProgDialog prog;
    NetworkChecking networkChecking;

   public static Lifecycle lifecycle;

    AudioSenseiPlayerView audio_player;
    public static Fragment previousfragment;
    ImageView home, videos;
    LinearLayout profile_lay;
    public static  AppBarLayout myAppBar;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_main );



        ac = this;

        tinyDb = new TinyDB( getApplicationContext() );
        networkChecking = new NetworkChecking();
        snakeBaar = new SnakeBaar();
        prog = new ProgDialog();

        getChannels();


        audio_player = (AudioSenseiPlayerView) findViewById(R.id.audio_player);
        audio_player.setAudioTarget("");
        audio_player.removeAllViews();


         lifecycle = getLifecycle();


         myAppBar =  (AppBarLayout)findViewById(R.id.app_bar);

        Date current_date_time = new Date();
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss a");
        String dateToStr = format.format(current_date_time);
        //System.out.println(dateToStr);


        toolbar = (Toolbar) findViewById( R.id.reg_toolbar );
        name = (TextView) findViewById( R.id.name );
        profileImage = (SimpleDraweeView)findViewById( R.id.profile_image );
        profile_lay = (LinearLayout) findViewById( R.id.profile_lay );

        profile_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                android.app.FragmentManager notifications ;
                android.app.FragmentTransaction fragmentTransaction2;
                notifications = ac.getFragmentManager();
                fragmentTransaction2 = notifications.beginTransaction();
                fragmentTransaction2.setCustomAnimations(R.animator.slide_up, 0, 0, R.animator.slide_down);
                UserProfile center1 = new UserProfile();
                fragmentTransaction2.replace(R.id.fragment_container, android.app.Fragment.instantiate(ac, center1.getClass().getName()));
                fragmentTransaction2.addToBackStack(null);
                fragmentTransaction2.commit();

                drawer.closeDrawer(Gravity.LEFT);
            }
        });

        linearLayout = (RelativeLayout) findViewById( R.id.contactushomedrawer_layout );

        form_one_title = (TextView) findViewById( R.id.form_one_title );

        home = (ImageView) findViewById( R.id.home );
        videos = (ImageView) findViewById( R.id.videos );




        drawer_layout_home = (DrawerLayout) findViewById( R.id.drawer_layout_home );
        contactussLeftDrawer = (ListView) findViewById( R.id.contactuss_left_drawer );
        toggle = (ImageView) findViewById( R.id.toggle );

        name.setText(tinyDb.getString("NAME"));

        profileImage.setImageURI( Uri.parse(tinyDb.getString("IMAGE")) );





        LeftMenu leftMenu = new LeftMenu();
        leftMenu.init( this, drawer_layout_home, toolbar, contactussLeftDrawer, toggle );


        previousfragment = getFragmentManager().findFragmentById(R.id.fragment_container);


        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                android.app.FragmentManager notifications ;
                android.app.FragmentTransaction fragmentTransaction2;



                tinyDb.putString("TYPE", "POPULAR");
                try {
                    notifications = ac.getFragmentManager();
                    fragmentTransaction2 = notifications.beginTransaction();
                    fragmentTransaction2.setCustomAnimations(R.animator.slide_up, 0, 0, R.animator.slide_down);
                    AllFeeds center = new AllFeeds();
                    fragmentTransaction2.replace(R.id.fragment_container, Fragment.instantiate(ac, center.getClass().getName()));
                    fragmentTransaction2.addToBackStack(null);
                    fragmentTransaction2.commit();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });


        videos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                android.app.FragmentManager notifications ;
                android.app.FragmentTransaction fragmentTransaction2;

                tinyDb.putString("TYPE", "VID");
                try {
                    notifications = ac.getFragmentManager();
                    fragmentTransaction2 = notifications.beginTransaction();
                    fragmentTransaction2.setCustomAnimations(R.animator.slide_up, 0, 0, R.animator.slide_down);
                    AllFeeds center2 = new AllFeeds();
                    fragmentTransaction2.add(R.id.fragment_container, Fragment.instantiate(ac, center2.getClass().getName()));
                    fragmentTransaction2.addToBackStack(null);
                    fragmentTransaction2.commit();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });


        MainApplication.getInstance().trackEvent("User", tinyDb.getString("ID"), "App Open by this User", tinyDb.getString("ID"));

    }




    public void loadFragment(Fragment fragment) {
        FragmentManager fm = getFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.add( R.id.fragment_container, fragment );
        fragmentTransaction.addToBackStack( null );
        fragmentTransaction.commit();

    }



    @Override
    protected void onPause() {
        super.onPause();
        JZVideoPlayer.releaseAllVideos();
    }


    Dialog show1;
    boolean doubleBackToExitPressedOnce = false;

    @Override
    public void onBackPressed() {

        try {
            if (JZVideoPlayer.backPress()) {
                return;
            }
        } catch (Exception e) {}

        if (getFragmentManager().findFragmentById(R.id.fragment_container) instanceof  AllFeeds) {
            if (doubleBackToExitPressedOnce) {
                overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left);
                finish();
                return;
            }
            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, R.string.please_press, Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce=false;
                }
            }, 2000);

        } else {
            try {
                    getFragmentManager().popBackStack();
                        if (tinyDb.getString("TYPE").equals("VID")) {
                            form_one_title.setText(R.string.videos);
                        }else{
                            form_one_title.setText(R.string.popular_posts);
                        }

            } catch (Exception e) {

                    getFragmentManager().popBackStack();

                        if (tinyDb.getString("TYPE").equals("VID")) {
                            form_one_title.setText(R.string.videos);
                        }else{
                            form_one_title.setText(R.string.popular_posts);

                }

            }



        }

    }

    void getTags() {



        if (networkChecking.isNetworkAvailable( ac )) {
            prog.progDialog(ac);


            RequestBody lang_part = RequestBody.create(MediaType.parse("multipart/form-data"), tinyDb.getString("LANG"));

            MainApplication.getApiService().getTags("/api/content/tags/"+tinyDb.getString("ID"),lang_part).enqueue(new Callback<JsonElement>() {
                @Override
                public void onResponse(Call<JsonElement> call, Response<JsonElement > response) {
                    prog.hideProg();
                    if (response.isSuccessful()) {




                        try {
                            JSONObject jsonObj = new JSONObject(String.valueOf(response.body()));
                            responceCode =  jsonObj.getString("rc");

                            //System.out.println("responceeeeeeeeeeeeee   "+response.body());


                        } catch (Exception e) {
//                            e.printStackTrace();
                        }


                        if (responceCode.equals("1")){

                            tinyDb.putString("Tags", String.valueOf(response.body()));


                        }

                    }
                }

                @Override
                public void onFailure(Call<JsonElement > call, Throwable t) {
                    prog.hideProg();
                    t.printStackTrace();
                }
            });








        } else {
            snakeBaar.showSnackBar( ac, getString(R.string.no_internet), linearLayout );
        }
    }




    void getChannels() {



        if (networkChecking.isNetworkAvailable( ac )) {
            prog.progDialog(ac);


            RequestBody lang_part = RequestBody.create(MediaType.parse("multipart/form-data"), tinyDb.getString("LANG"));




            MainApplication.getApiService().getChannels("/api/content/channelimages/"+tinyDb.getString("ID"), lang_part).enqueue(new Callback<JsonElement>() {
                @Override
                public void onResponse(Call<JsonElement> call, Response<JsonElement > response) {
                    prog.hideProg();
                    if (response.isSuccessful()) {

                        getTags();


                        try {
                            JSONObject jsonObj = new JSONObject(String.valueOf(response.body()));
                            responceCode =  jsonObj.getString("rc");

                            //System.out.println("responceeeeeeeeeeeeee   "+response.body());


                        } catch (Exception e) {
//                            e.printStackTrace();
                        }




                        if (responceCode.equals("1")){

                            tinyDb.putString("Channels", String.valueOf(response.body()));
                            loadFragment(new AllFeeds());

//                            FragmentManager notifications ;
//                            FragmentTransaction fragmentTransaction2;
//
//                            notifications = ac.getFragmentManager();
//                            fragmentTransaction2 = notifications.beginTransaction();
//                            AllFeeds center = new AllFeeds();
//                            fragmentTransaction2.add(R.id.fragment_container, android.app.Fragment.instantiate(ac, center.getClass().getName()));
//                            fragmentTransaction2.addToBackStack(null);
//                            fragmentTransaction2.commit();
                        }

                    }
                }

                @Override
                public void onFailure(Call<JsonElement > call, Throwable t) {
                    prog.hideProg();
                    t.printStackTrace();
                }
            });








        } else {
            snakeBaar.showSnackBar( ac, getString(R.string.no_internet), linearLayout );
        }
    }


    @Override
    protected void onResume() {
        Configuration config = getBaseContext().getResources().getConfiguration();
        String lang = tinyDb.getString("LANG");

        Locale locale = new Locale(lang);
        Locale.setDefault(locale);
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());

        //Handling install referrer
        TrackInstall.handleInstallReferrer(ac, tinyDb);
        //Handling login referrer
        TrackInstall.handleInstallLogin(ac, tinyDb);

        super.onResume();
    }

    @Override
    protected void onStop() {

        super.onStop();
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }


//   public static void toolBarVisibility(){
//        final Handler handler=new Handler();
//
//        final Runnable updateTask=new Runnable() {
//            @Override
//            public void run() {
//
//                try {
//
//                    f = ac.getFragmentManager().findFragmentById(R.id.fragment_container);
//                    //System.out.println("fffffffffffffffffff   "+f);
//                    if(f instanceof UserDetail){
//                        toolbar.setVisibility(View.GONE);
//                    }else {
//                        toolbar.setVisibility(View.VISIBLE);
//                    }
//                } catch (Exception e1) {
//                    //System.out.println("fffffffffffffffffff   "+e1);
//                    toolbar.setVisibility(View.VISIBLE);
//                }
//
////                                handler.postDelayed(this,1000);
//            }
//        };
//
//        handler.postDelayed(updateTask,100);
//    }

    @Override
    public void onDestroy() {
        if(prog != null)
            prog.destroy();
        super.onDestroy();
    }
}


package com.daivikapp.social.models;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetChanelsResponse {

    @SerializedName("rc")
    @Expose
    private int rc;
    @SerializedName("channels")
    @Expose
    private List<Channel> channels = null;

    public int getRc() {
        return rc;
    }

    public void setRc(int rc) {
        this.rc = rc;
    }

    public List<Channel> getChannels() {
        return channels;
    }

    public void setChannels(List<Channel> channels) {
        this.channels = channels;
    }

}
